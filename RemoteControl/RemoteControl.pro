QT += core
QT -= gui

CONFIG += c++11

TARGET = RemoteControl
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp



unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_thread
unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_filesystem
unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_system

INCLUDEPATH += $$PWD/../../../boost_1_63_0
DEPENDPATH += $$PWD/../../../boost_1_63_0

