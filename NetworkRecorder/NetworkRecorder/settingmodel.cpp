#include "settingmodel.h"
#include "NetworkDeviceManager.h"
using namespace NetworkLib;
#include "StorageManager.h"
#include <QProcess>
#include <QProgressBar>

int temp = 0;


SettingModel::SettingModel(QObject *parent)
	: QAbstractTableModel(parent)
{
    drives = StorageManager::HardDiskInformation();
}

SettingModel::~SettingModel()
{
}


std::vector<DiskInfo> SettingModel::currentChanged()
{
    drives = StorageManager::HardDiskInformation();
    return drives;
}


Qt::ItemFlags SettingModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	return flags;
}

int SettingModel::rowCount(const QModelIndex &) const
{
   return (int)drives.size();
}

int SettingModel::columnCount(const QModelIndex &) const
{
    return 9;
}

QVariant SettingModel::data(const QModelIndex &index, int role) const
{

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    //if (role == Qt::DisplayRole)
	{
		int col = index.column();
		int row = index.row();

        if (row >= 0 && row < (int)drives.size())
        {
            auto& drive = drives[row];
			if (col == 0)
			{
				return drive.info.c_str();
			}
            else if (col == 1)
            {
                //return drive.model_name.c_str();
                return drive.display_model_name.c_str();
            }
            else if (col == 2)
            {
                return drive.serial_number.c_str();
            }
            else if (col == 3)
            {
                return QString::number(drive.life_cycle);
            }
            else if (col == 4)
            {
                return QString::number(drive.written);
            }
            else if (col == 5)
			{
                return QString::number(drive.capacity / 1000.0, 'f', 1);
            }
            else if (col == 6)
			{
                return QString::number(drive.available / 1000.0, 'f', 1);
			}
            else if (col == 7)
			{
				return QString::number(drive.free / 1000.0, 'f', 1);
			}
//            else if (col == 4)
//            {
//                 return QString::number(drive.free /10/ 1000.0, 'f', 1);

//            }
		}
	}
	return QVariant();
}

bool SettingModel::setData(const QModelIndex &, const QVariant &, int)
{
   int a = 0;
   return false;
}

QVariant SettingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal)
		{
			if (section == 0)
			{
				return QString("Drive");
			}
            else if (section == 1)
            {
                return QString("Model #");
            }
            else if (section == 2)
            {
                return QString("Serial #");
            }
            else if (section == 3)
            {
                return QString("LifeCycle(PB)");
            }
            else if (section == 4)
            {
                return QString("DataWritten(%)");
            }
            else if (section == 5)
			{
                return QString("Capacity (GB)");
			}
            else if (section == 6)
			{
                return QString("UsedSpace(GB)");
			}
            else if (section == 7)
			{
                return QString("FreeSpace(GB)");
			}
            else if (section == 8)
            {
                return QString("Usage (%)");
            }
		}
	}

	return QVariant();
}
