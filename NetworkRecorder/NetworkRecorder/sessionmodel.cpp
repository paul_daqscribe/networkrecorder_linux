#include "sessionmodel.h"
#include "DeviceRecordSetting.h"
#include "NetworkDeviceManager.h"
#include <QtCore/QTime>
#include <QDateTimeEdit>
#include <QMessageBox>

using namespace NetworkLib;

//QString time_format = "dd-MM-yyyy HH:mm:ss";
QString time_format = "yyyy-MM-dd HH:mm:ss";

SessionModel::SessionModel(QObject *parent)
	: QAbstractTableModel(parent)
{
}

SessionModel::~SessionModel()
{
}

void SessionModel::addRow(QModelIndex &index)
{
    //QModelIndex indexTest;
//    beginInsertRows(QModelIndex(),0,2);
//    endInsertRows();

}


Qt::ItemFlags SessionModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = QAbstractTableModel::flags(index);
	if (index.row() > 1 && index.column() == 1) return flags;
	return flags | Qt::ItemIsEditable;
}

int SessionModel::rowCount(const QModelIndex &) const
{
   return 8;
}

int SessionModel::columnCount(const QModelIndex &) const
{
    //NET-57
    return 2;
}

QVariant SessionModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				return QString(DeviceRecordSetting::GetInstance()->GetRecordName().c_str());
			}
			else if (index.row() == 1)
			{
                return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(0).c_str());
			}
            else if (index.row() == 2)
            {
                return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(1).c_str());
            }
            else if (index.row() == 3)
            {
                return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(2).c_str());
            }
            else if (index.row() == 4)
            {
                return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(3).c_str());
            }

         else if (index.row() == 5)
         {
            return QString::number(DeviceRecordSetting::GetInstance()->GetRecordMaximumSize() / 1000000.0, 'f', 2);
         }
         else if (index.row() == 6)
         {
			 auto t = DeviceRecordSetting::GetInstance()->RecordTriggerTime;
             //return QDateTime(QDate(t.tm_year + 1900, t.tm_mon + 1, t.tm_mday), QTime(t.tm_hour, t.tm_min, t.tm_sec));
             QDateTime qtime = QDateTime(QDate(t.tm_year + 1900, t.tm_mon + 1, t.tm_mday), QTime(t.tm_hour, t.tm_min, t.tm_sec));

             //return qtime;
             //kjk
             return qtime.toString(time_format);

         }
         else if (index.row() == 7)
         {
            return QString(DeviceRecordSetting::GetInstance()->GetReplayPath().c_str());
         }
      }
	}
    else if (role == Qt::ToolTipRole)
    {
        //NET-57
        if(index.row() == 1)
        {
            return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(0).c_str());
        }
        else if(index.row() == 2)
        {
            return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(1).c_str());
        }
        else if(index.row() == 3)
        {
            return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(2).c_str());
        }
        else if(index.row() == 4)
        {
            return QString(DeviceRecordSetting::GetInstance()->GetRecordPath(3).c_str());
        }
        else if(index.row() == 6)
        {
            return "YYYY-MM-DD hh:mm:ss, hh:24hour";
        }
    }
	return QVariant();
}

bool SessionModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	if (role == Qt::EditRole)
	{
		if (index.column() == 0)
		{
			if (index.row() == 0)
			{
				DeviceRecordSetting::GetInstance()->SetRecordName(value.toString().toStdString());
			}
			else if (index.row() == 1)
			{
                DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString(), 0);
			}
            else if (index.row() == 2)
            {
                DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString(), 1);
            }
            else if (index.row() == 3)
            {
                DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString(), 2);
            }
            else if (index.row() == 4)
            {
                DeviceRecordSetting::GetInstance()->SetRecordPath(value.toString().toStdString(), 3);
            }
         else if (index.row() == 5)
         {
            DeviceRecordSetting::GetInstance()->SetRecordMaximumSize((long long)(value.toDouble() * 1000000));
         }
         else if (index.row() == 6)
         {
			 auto& t = DeviceRecordSetting::GetInstance()->RecordTriggerTime;
             //auto ti = value.toDateTime();

             //kjk
             QDateTime b = QDateTime::fromString(value.toString(),time_format);
             if (!(b.isValid()))
             {
                 QMessageBox msgBox;
                 msgBox.setText(tr("It is not valid.\nCheck the format : YYYY-MM-DD hh:mm:ss, hh:24hour" ));
                 msgBox.setStandardButtons(QMessageBox::Ok);
                 msgBox.setIcon(QMessageBox::Critical);
                 int ret = msgBox.exec();

                 return false;
             }
             auto ti = b;
			 t.tm_year = ti.date().year() - 1900;
			 t.tm_mon = ti.date().month()-1;
			 t.tm_mday = ti.date().day();
			 t.tm_hour = ti.time().hour();
			 t.tm_min = ti.time().minute();
			 t.tm_sec = ti.time().second();
         }
         else if (index.row() == 7)
         {
            DeviceRecordSetting::GetInstance()->SetReplayPath(value.toString().toStdString());
         }
         return true;
		}
	}


	return false;
}

QVariant SessionModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Vertical)
		{
			if (section == 0)
			{
            return QString("Record Session");
			}
			else if (section == 1)
			{
                QString temp = "Record Path1 CH : ";
                if(DeviceRecordSetting::GetInstance()->v_Group.size() > 0)
                    //return QString(temp + DeviceRecordSetting::GetInstance()->v_Group[0].m_SetPort.c_str());
                    return QString(temp + DeviceRecordSetting::GetInstance()->m_pathCH[section-1].c_str());
                else
                    return QString("Record Path1");
            }
            else if (section == 2)
            {
                QString temp = "Record Path2 CH : ";
                if(DeviceRecordSetting::GetInstance()->v_Group.size() > 1)
                    //return QString(temp + DeviceRecordSetting::GetInstance()->v_Group[1].m_SetPort.c_str());
                    return QString(temp + DeviceRecordSetting::GetInstance()->m_pathCH[section-1].c_str());
                else
                    return QString("Record Path2");
            }
            else if (section == 3)
            {
                QString temp = "Record Path3 CH : ";
                if(DeviceRecordSetting::GetInstance()->v_Group.size() > 2)
                    //return QString(temp + DeviceRecordSetting::GetInstance()->v_Group[2].m_SetPort.c_str());
                    return QString(temp + DeviceRecordSetting::GetInstance()->m_pathCH[section-1].c_str());
                else
                    return QString("Record Path3");
            }
            else if (section == 4)
            {
                QString temp = "Record Path4 CH : ";
                if(DeviceRecordSetting::GetInstance()->v_Group.size() > 3)
                    //return QString(temp + DeviceRecordSetting::GetInstance()->v_Group[3].m_SetPort.c_str());
                    return QString(temp + DeviceRecordSetting::GetInstance()->m_pathCH[section-1].c_str());
                else
                    return QString("Record Path4");
            }
         else if (section == 5)
         {
            return QString("Maximum Single File Size (Mb)");
         }
         else if (section == 6)
         {
            return QString("Trigger Time");
         }
         else if (section == 7)
         {
            return QString("Replay Path");
         }
		}
	}

	return QVariant();
}
