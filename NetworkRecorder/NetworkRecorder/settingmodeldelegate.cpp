#include "settingmodeldelegate.h"
#include <qcombobox.h>
#include <QProgressBar>
#include <QtGui>
#include <QDialog>
#include <QPushButton>
#include <QApplication>

SettingModelDelegate::SettingModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{
//    QProgressBar *bar = new QProgressBar;
//    //ui.storageTableView->setIndexWidget(settingModel.index(0,4), bar);
//    bar->setValue(50);

//    QProgressBar* bar = new QProgressBar();
//    bar->setValue(50);
}

SettingModelDelegate::~SettingModelDelegate()
{
}

void SettingModelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    QItemDelegate::paint(painter, option, index);

//    //int progress = index.model()->data().toInt();

//    if (index.column() == 4)
//    {
//        //int progress = index.model()->data(index, Qt::EditRole).toInt();
//        int progress = index.model()->data(index, Qt::EditRole).toInt();

//        QStyleOptionProgressBar progressBarOption;
//        progressBarOption.rect = option.rect;
//        progressBarOption.minimum = 0;
//        progressBarOption.maximum = 100;
//        progressBarOption.progress = progress;
//        progressBarOption.text = QString::number(progress) + "%";
//        progressBarOption.textVisible = true;

//        QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);
//    }

}

bool SettingModelDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
 {
//     if( event->type() == QEvent::MouseButtonRelease )
//     {
//         QMouseEvent * e = (QMouseEvent *)event;
//         int clickX = e->x();
//         int clickY = e->y();

//         QRect r = option.rect;//getting the rect of the cell
//         int x,y,w,h;
//         x = r.left() + r.width() - 30;//the X coordinate
//         y = r.top();//the Y coordinate
//         w = 30;//button width
//         h = 30;//button height

//         if( clickX > x && clickX < x + w )
//             if( clickY > y && clickY < y + h )
//             {
//                 QDialog * d = new QDialog();
//                 d->setGeometry(0,0,100,100);
//                 d->show();
//             }
//     }

//     return true;
 }

QWidget* SettingModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if (index.row() == 1)
	{
		QComboBox* comboBox = new QComboBox(parent);
		comboBox->addItem("1 Gb");
		comboBox->addItem("2.1 Gb");
		comboBox->addItem("2.5 Gb");
		comboBox->setCurrentIndex(0);
		return comboBox;
	}
	else
	{
		return QItemDelegate::createEditor(parent, option, index);
	}
}

void SettingModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	if (index.row() == 1)
	{
		QString value = index.model()->data(index, Qt::EditRole).toString();
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		comboBox->setCurrentIndex(comboBox->findText(value));
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void SettingModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if (index.row() == 1)
	{
		QComboBox* comboBox = static_cast<QComboBox*>(editor);
		int value = comboBox->currentIndex();
		model->setData(index, value, Qt::EditRole);
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void SettingModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
    editor->setGeometry(option.rect);
}
