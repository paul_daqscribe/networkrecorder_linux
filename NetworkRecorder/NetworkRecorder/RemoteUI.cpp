#include "RemoteUI.h"
#include "logger.h"
#include "NetworkDeviceManager.h"

RemoteUI::RemoteUI(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(run()));
	connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(close()));
	connect(ui.settingButton, SIGNAL(clicked()), this, SLOT(setting()));
	connect(&firmctr, SIGNAL(on_input_up(int)), this, SLOT(comhandle(int)));
	auto mgr = NetworkLib::NetworkDeviceManager::GetInstance();
	ui.spinBox->setValue(mgr->ServerPort);
	ui.firmataPortSpinBox->setValue(mgr->TriggerPort);
	run();
}

RemoteUI::~RemoteUI()
{
	firmctr.close();
}

void RemoteUI::getSetting()
{
	auto mgr = NetworkLib::NetworkDeviceManager::GetInstance();
	ui.spinBox->setValue(mgr->ServerPort);
	ui.firmataPortSpinBox->setValue(mgr->TriggerPort);
}

void RemoteUI::closeEvent(QCloseEvent *)
{
	done();
}

void RemoteUI::run()
{
	firmctr.close();
	firmctr.open();
	auto mgr = NetworkLib::NetworkDeviceManager::GetInstance();
	mgr->ServerPort = ui.spinBox->value();
	mgr->TriggerPort = ui.firmataPortSpinBox->value();
	hide();
}

void RemoteUI::setting()
{
	firmctr.setIcon(windowIcon());
	firmctr.openui();
}

void RemoteUI::comhandle(int idx)
{
	auto manager = NetworkLib::NetworkDeviceManager::GetInstance();
	if (manager->TriggerPort == idx)
	{
		manager->Record();
		recordStarted();
	}
}
