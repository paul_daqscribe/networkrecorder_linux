#ifndef NETWORKRECORDER_H
#define NETWORKRECORDER_H

#include <QtWidgets/QMainWindow>
#include "ui_networkrecorder.h"
#include "recordchannelmodel.h"
#include "replaychannelmodel.h"
#include "replaydevicemodeldelegate.h"
#include "settingmodel.h"
#include "settingmodeldelegate.h"
#include "sessionmodel.h"
#include "sessionmodeldelegate.h"
#include "usertagmodel.h"
#include <QTimer>
#include <QProgressBar>
#include <qsettings.h>
#include <memory>

class RemoteUI;
class NetworkServer;

class NetworkRecorder : public QMainWindow
{
	Q_OBJECT

public:

	NetworkRecorder(const std::string& path, QWidget *parent = 0);

	~NetworkRecorder();

	public slots :

	void Monitor();

	void StartRecord();

	void StartRecordAll();

	void StopRecord();

	void UpdateStatus();

    void UpdateStatus_1sec();

	void ResetUi();

	void SetUp();

	void SetDown();

	void Enable();

	void Disable();

	void UpdateSessions();

	void InsertTagBefore();

	void InsertTagAfter();

	void RemoveTag();

	void AddTagToSession();

	void AddResetTagToSession();

	void LoadSetting();

	void SaveSetting();

	void SettingServer();

	void InitServer();

    //kjk
    void InitProgressbar();

    void SetProgressbar();

    void SetSessionProgressbar();

    void SetGUI();

    void SetStorage();



private slots:

    void on_tabWidget_currentChanged(int index);


private:

	void LoadUISetting();

	void SaveUISetting();

	Ui::NetworkRecorderClass ui;

	RecordChannelModel recordModel;

	ReplayChannelModel replayModel;

	ReplayDeviceModelDelegate deviceModelDelegate;

	SettingModel settingModel;

	SettingModelDelegate settingModelDelegate;

	SessionModel sessionModel;

	SessionModelDelegate sessionModelDelegate;

	UserTagModel userTagModel;

	std::unique_ptr<QProgressBar> progressbar;

	std::unique_ptr<RemoteUI> remoteUI;

	QTimer *timer;
    QTimer *timer2;

	QTimer *timeTriggerTimer;

	std::string m_path;

	NetworkServer * server;

    QProgressBar *sessionbar[4];
    QProgressBar *bar[8];

    QProgressBar *storagebar[16];
    QProgressBar *storagebar_life[16];

    //QProgressBar *bar[8];

    bool b_TrrigerChecked = true;

};

#endif // NETWORKRECORDER_H
