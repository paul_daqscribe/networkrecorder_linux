
#define _GNU_SOURCE

#include "networkrecorder.h"
#include "NetworkDeviceManager.h"
#include "DeviceRecordSetting.h"
//#include "NetworkDeviceManager.h"
//#include "NTNetworkDevice.h"
using namespace NetworkLib;
#include "qfiledialog.h"
#include "qmessagebox.h"
#include "RemoteUI.h"
#include "SFPDPDevice.h"
#include "GESFPDPDevice.h"
#include "StorageManager.h"
#include "usertagmanager.h"
#include "logger.h"
NetworkLib::FileLog flog;
#include "networkserver.h"
#include "MinidumpHelp.h" 
#include <sched.h>
#include <QLabel>
#include <QMovie>
#include <QtCore>
//#include <thread>
//#include <iostream>


struct Monitor : public IPacketMonitor
{
	Monitor(NetworkRecorder* precorder) : m_recorder(std::shared_ptr<NetworkRecorder>(precorder)) {}
	void Update(int)
	{
		if (m_recorder) m_recorder->UpdateStatus();
	}
	std::shared_ptr<NetworkRecorder> m_recorder;
};

NetworkRecorder::NetworkRecorder(const std::string& path, QWidget *parent)
	: QMainWindow(parent)
	, recordModel(this)
	, replayModel(this)
	, deviceModelDelegate(this)
	, settingModel(this)
	, settingModelDelegate(this)
	, sessionModel(this)
	, sessionModelDelegate(this)
	, userTagModel(this)
	, progressbar(new QProgressBar(this))
    , remoteUI(new RemoteUI())
    , m_path(path)
	, server(new NetworkServer())
{
#ifdef WIN32
	MinidumpHelp test;
	test.install_self_mini_dump();
#endif
    flog.SetFile(m_path + "/log.txt");
	NetworkLib::Logger::GetInstance()->AddLog(&flog);
	ui.setupUi(this);
    StorageManager::CreatePath(m_path);
	auto netmgr = NetworkDeviceManager::GetInstance();
    netmgr->Init(m_path + "/NetworkRecorder.xml");
	timer = new QTimer(this);
    timer2 = new QTimer(this);
	timeTriggerTimer = new QTimer(this);
    ui.recordTableView->setModel(&recordModel);
    //ui.recordTableView->resizeColumnsToContents();
	ui.replayTableView->setModel(&replayModel);
	ui.replayTableView->setItemDelegate(&deviceModelDelegate);
	ui.replayTableView->resizeColumnsToContents();
	ui.storageTableView->setModel(&settingModel);
	ui.storageTableView->setItemDelegate(&settingModelDelegate);
    ui.storageTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	ui.recordingOptionTableView->setModel(&sessionModel);
    ui.recordingOptionTableView->resizeColumnsToContents();
	ui.recordingOptionTableView->setItemDelegate(&sessionModelDelegate);

	ui.userTagTableView->setModel(&userTagModel);

	ui.statusBar->addPermanentWidget(progressbar.get(), 0);
    progressbar->hide();
	connect(timer, SIGNAL(timeout()), this, SLOT(UpdateStatus()));
    connect(timer2, SIGNAL(timeout()), this, SLOT(UpdateStatus_1sec()));
	connect(ui.actionRun, SIGNAL(triggered()), this, SLOT(Monitor()));
	connect(ui.actionInsertTagAfter, SIGNAL(triggered()), this, SLOT(InsertTagBefore()));
	connect(ui.actionInsertTagBefore, SIGNAL(triggered()), this, SLOT(InsertTagAfter()));
	connect(ui.actionRemoveTag, SIGNAL(triggered()), this, SLOT(RemoveTag()));
	connect(ui.actionAddTagToSession, SIGNAL(triggered()), this, SLOT(AddTagToSession()));
	connect(ui.actionAddResetTagToRecord, SIGNAL(triggered()), this, SLOT(AddResetTagToSession()));
	connect(ui.actionStartRecord, SIGNAL(triggered()), this, SLOT(StartRecord()));
	connect(ui.actionStopRecord, SIGNAL(triggered()), this, SLOT(StopRecord()));
	connect(ui.actionLoadSetting, SIGNAL(triggered()), this, SLOT(LoadSetting()));
	connect(ui.actionSaveSetting, SIGNAL(triggered()), this, SLOT(SaveSetting()));
	connect(ui.actionRunServer, SIGNAL(triggered()), this, SLOT(SettingServer()));
	connect(timeTriggerTimer, SIGNAL(timeout()), this, SLOT(UpdateSessions()));
	connect(ui.actionTimeTrigger, SIGNAL(triggered()), this, SLOT(StartRecordAll()));
	connect(remoteUI.get(), SIGNAL(done()), this, SLOT(InitServer()));
	connect(server, SIGNAL(updateui()), this, SLOT(UpdateStatus()));
	connect(remoteUI.get(), SIGNAL(recordStarted()), this, SLOT(UpdateStatus()));

    ui.statusBar->setStyleSheet("QStatusBar{padding-left:16px;background:rgba(0,0,0,0);color:blue;font-weight:bold;}");

	ui.recordTableView->addAction(ui.actionStartRecord);
	ui.recordTableView->addAction(ui.actionStopRecord);
	ui.replayTableView->addAction(ui.actionStartRecord);
	ui.replayTableView->addAction(ui.actionStopRecord);

    //ui.userTagTableView->addAction(ui.actionAddTagToSession);
	ui.userTagTableView->addAction(ui.actionInsertTagAfter);
	ui.userTagTableView->addAction(ui.actionInsertTagBefore);
	ui.userTagTableView->addAction(ui.actionRemoveTag);

	LoadUISetting();
	remoteUI->setWindowIcon(windowIcon());
	server->init(NetworkDeviceManager::GetInstance()->ServerPort);

    InitProgressbar();
    SetGUI();
    SetStorage();

    ResetUi();

}

NetworkRecorder::~NetworkRecorder()
{
	auto mgr = NetworkDeviceManager::GetInstance();
	mgr->Stop();

//    auto nt_mgr = NTNetworkDeviceManager::GetInstance();
//    if (mgr->IsOpen())

    mgr->WriteSystemInfo(m_path + "/NetworkRecorder.xml");
	NetworkDeviceManager::Clear();
	SaveUISetting();
	if (timer) delete timer;
    if (timer2) delete timer2;
	if (timeTriggerTimer) delete timeTriggerTimer;
}

void NetworkRecorder::SetGUI()
{
    //NET-42 change GUI
    ui.mainToolBar->setContextMenuPolicy(Qt::NoContextMenu);
    ui.mainToolBar->setContextMenuPolicy(Qt::PreventContextMenu);

    ui.mainToolBar->setStyleSheet("QToolBar {background: rgb(30,30,30)}");
    QLabel* temp = new QLabel();
    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    temp->setPixmap(QPixmap(":/DaqSuite/Resources/daqscribe_logo.png"));

    ui.mainToolBar->addWidget(spacer);
    ui.mainToolBar->addWidget(temp);
}

void NetworkRecorder::SetStorage()
{
    auto setting = DeviceRecordSetting::GetInstance();
    unsigned long mask = setting->ThreadPriorityProgram;
    if(sched_setaffinity(0, sizeof(mask), (cpu_set_t*)&mask) < 0)
        perror("sched_setaffinity");

    //disable Storage row : hard fix only 1~4
    if (setting->m_StorageNum == 2)
    {
        ui.recordingOptionTableView->setRowHidden(3, true);
        ui.recordingOptionTableView->setRowHidden(4, true);
    }
    else if (setting->m_StorageNum == 3)
    {
        ui.recordingOptionTableView->setRowHidden(4, true);
    }
    else if (setting->m_StorageNum == 4)
    {

    }
    else // 1
    {
        ui.recordingOptionTableView->setRowHidden(2, true);
        ui.recordingOptionTableView->setRowHidden(3, true);
        ui.recordingOptionTableView->setRowHidden(4, true);
    }
}


void NetworkRecorder::InitProgressbar()
{
    //set bar
    auto devices = NetworkDeviceManager::GetInstance()->GetRecordDevices();
    for (int i = 0; i < (int)devices.size(); ++i)
    {
        bar[i] = new QProgressBar();
        bar[i]->setAlignment(Qt::AlignCenter);
        ui.recordTableView->setIndexWidget(recordModel.index(i,4), bar[i]);
        bar[i]->setFormat("0");
        bar[i]->setValue(0);
        //bar[i]->setFormat("0");
        bar[i]->setTextVisible(true);
    }

    std::vector<DiskInfo> drives;
    drives = settingModel.currentChanged();

    //set storagebar
    for(int i = 0; i<(int)drives.size(); i++)
    {
        storagebar_life[i] = new QProgressBar();
        QPalette p = storagebar_life[i]->palette();
        p.setColor(QPalette::Highlight, Qt::blue);
        storagebar_life[i]->setPalette(p);
        storagebar_life[i]->setAlignment(Qt::AlignCenter);
        ui.storageTableView->setIndexWidget(settingModel.index(i,4), storagebar_life[i]);

        storagebar[i] = new QProgressBar();
        storagebar[i]->setAlignment(Qt::AlignCenter);
        ui.storageTableView->setIndexWidget(settingModel.index(i,8), storagebar[i]);
    }

    //NET-57
    //set sessionbar
    ui.recordingOptionTableView->setSpan(0,0,1,2);
    ui.recordingOptionTableView->setSpan(5,0,1,2);
    ui.recordingOptionTableView->setSpan(6,0,1,2);
    ui.recordingOptionTableView->setSpan(7,0,1,2);

    //m_StorageNum
    auto setting = DeviceRecordSetting::GetInstance();
    for(int i = 0; i < setting->m_StorageNum; i++)
    {
        sessionbar[i]= new QProgressBar();
        sessionbar[i]->setAlignment(Qt::AlignCenter);
        ui.recordingOptionTableView->setIndexWidget(sessionModel.index(i+1,1), sessionbar[i]);

        //NET-57
        sessionbar[i]->setValue(StorageManager::GetStorageUsage(setting->m_RecordPath[i]));

    }

}

//NET-57
void NetworkRecorder::SetSessionProgressbar()
{
    auto setting = DeviceRecordSetting::GetInstance();
    for(int i = 0; i < setting->m_StorageNum; i++)
    {
        int value = 0;
        value = StorageManager::GetStorageUsage(setting->m_RecordPath[i]);

        if(value != 0)
        {
            sessionbar[i]->setValue(value);
            setting->m_StorageUsage[i] = value;
        }

    }
}

void NetworkRecorder::SetProgressbar()
{
    auto devices = NetworkDeviceManager::GetInstance()->GetRecordDevices();
    for (int i = 0; i < (int)devices.size(); ++i)
    {
        auto dev = devices[i];
        if (dev->GetActiveFlag())
        {
            QModelIndex recordIndex = recordModel.index(i,3);
            int totalTime = (int)(recordModel.data(recordIndex, Qt::EditRole).toFloat());

            QModelIndex nIndex = recordModel.index(i,4);
            int progress = (int)(recordModel.data(nIndex, Qt::EditRole).toFloat());

            float value = (float)progress / (float)totalTime * 100;

            bar[i]->setValue((int)value);
            bar[i]->setFormat(QVariant(progress).toString());
        }
    }
}

void NetworkRecorder::ResetUi()
{
    bool bChecked = NetworkDeviceManager::GetInstance()->IsRunning();
	ui.actionAddResetTagToRecord->setEnabled(bChecked);
	ui.actionAddTagToSession->setEnabled(bChecked);
    ui.actionRunServer->setEnabled(!bChecked);

    ui.actionStartRecord->setEnabled(bChecked);
    //ui.actionTimeTrigger->setEnabled(bChecked);

	ui.actionStopRecord->setEnabled(bChecked);
    ui.actionSaveSetting->setEnabled(!bChecked);
    ui.actionLoadSetting->setEnabled(!bChecked);

    ui.tabWidget->setTabEnabled(1, !bChecked);

}

void NetworkRecorder::UpdateStatus_1sec()
{
    if (NetworkDeviceManager::GetInstance()->IsRunning())
    {
        SetSessionProgressbar();
        timer2->start(1000);
    }
}

void NetworkRecorder::UpdateStatus()
{
	if (NetworkDeviceManager::GetInstance()->IsRunning())
	{
        timer->start(100);
		auto networkmanager = NetworkDeviceManager::GetInstance();
		const std::vector<std::shared_ptr<INetworkDevice>>& devices = networkmanager->GetRecordDevices();
        //QString status;
		for (size_t i = 0; i < devices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = devices[i];
			if (pDevice && pDevice->IsRunning())
			{
				break;
			}
        }

        SetProgressbar();

        recordModel.dataChanged(QModelIndex(), QModelIndex());
		replayModel.dataChanged(QModelIndex(), QModelIndex());
		if (!ui.actionRun->isChecked()) ui.actionRun->setChecked(true);
	}
	else
	{
		if (ui.actionRun->isChecked()) ui.actionRun->setChecked(false);
        timer->stop();
	}
	ResetUi();
}

void NetworkRecorder::Monitor()
{
    setFocus();

    if (!ui.actionRun->isChecked())
    {
        NetworkDeviceManager::GetInstance()->Stop();

        ResetUi();
        timer->stop();

        recordModel.dataChanged(QModelIndex(), QModelIndex());
        replayModel.dataChanged(QModelIndex(), QModelIndex());

        //about trigger
        b_TrrigerChecked = true;
        ui.actionTimeTrigger->setEnabled(false);
        timeTriggerTimer->stop();
    }
    else
    {
        ui.statusBar->showMessage("Initialing buffers...", 2000);
        ui.actionTimeTrigger->setEnabled(true);

        auto devices = NetworkDeviceManager::GetInstance()->GetRecordDevices();
        bool activeflag = false;
        for (int i = 0; i < (int)devices.size(); ++i)
        {
            auto dev = devices[i];
            if (dev->GetActiveFlag())
            {
                activeflag = true;
                break;
            }
        }

        auto replaydevices = NetworkDeviceManager::GetInstance()->GetReplayDevices();
        for (int i = 0; i < (int)replaydevices.size(); ++i)
        {
            auto dev = replaydevices[i];
            if (dev->GetActiveFlag())
            {
                activeflag = true;
                break;
            }
        }

        if (activeflag)
        {
            if (!NetworkDeviceManager::GetInstance()->IsRunning())
            {
                NetworkDeviceManager::GetInstance()->SetRunMode(MONITORING);
                NetworkDeviceManager::GetInstance()->Monitor();
                ResetUi();
                timer->start(100);
                timer2->start(1000);
            }
        }
    }
}

void NetworkRecorder::StartRecord()
{

    auto setting = DeviceRecordSetting::GetInstance();
    auto networkmanager = NetworkDeviceManager::GetInstance();
    QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
    auto& devices = networkmanager->GetRecordDevices();
    if (indexes.size() > 0)
    {
        for (int i=0; i< indexes.size(); i++)
        {
            QModelIndex index = indexes[i];
            size_t idx = index.row();
            if (devices.size() > idx)
            {
                auto& dev = devices[idx];

                //get group path
                for(int x=0; x < setting->v_Group.size(); x++)
                {
                    for(int y = 0; y < setting->v_Group[x].m_NumPort; y++)
                    {
                         if(idx+1 == setting->v_Group[x].v_Port[y])
                         {
                             string path = setting->v_Group[x].m_Path;

                             if(path=="path1")
                                 dev->SetRecordFile(setting->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber(), 0));
                             else if (path=="path2")
                                 dev->SetRecordFile(setting->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber(), 1));
                             else if (path=="path3")
                                 dev->SetRecordFile(setting->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber(), 2));
                             else if (path=="path4")
                                 dev->SetRecordFile(setting->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber(), 3));
                             else
                                 dev->SetRecordFile(setting->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber(), 0));
                         }
                    }
                }

                dev->RecordBegin(idx+1);
            }
        }
        int nRow = recordModel.rowCount() - 1;
        recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
    }


    QModelIndexList indexes_replay = ui.replayTableView->selectionModel()->selection().indexes();
    auto& replyDevices = networkmanager->GetReplayDevices();
    for (int i = 0; i < indexes_replay.size(); ++i)
    {
        QModelIndex index = indexes_replay.at(i);
        size_t idx = index.row();
        if (replyDevices.size() > idx)
        {
            replyDevices[idx]->RecordBegin(idx);
        }
    }
    int nRow = replayModel.rowCount() - 1;
    replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));


//    auto networkmanager = NetworkDeviceManager::GetInstance();
//    //QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
//    auto& devices = networkmanager->GetRecordDevices();

//    for(int idx = 0; idx < devices.size(); idx++)
//    {
//        auto& dev = devices[idx];
//        if(idx == 0)
//            dev->SetReplayFile(DeviceRecordSetting::GetInstance()->GetNetworkRecordFileName(dev->GetID(), dev->GetRunNumber()));
//        else if(idx == 1)
//            dev->SetReplayFile(DeviceRecordSetting::GetInstance()->GetNetworkRecordFileName2(dev->GetID(), dev->GetRunNumber()));
//        dev->RecordBegin(idx);
//    }

//    auto& replyDevices = networkmanager->GetReplayDevices();
//    for (int idx = 0; idx < replyDevices.size(); ++idx)
//    {
//        //QModelIndex index = indexes.at(i);
//        //size_t idx = index.row();
//        if (replyDevices.size() > idx)
//        {
//            replyDevices[idx]->RecordBegin(idx);
//        }
//    }
//    int nRow = replayModel.rowCount() - 1;
//    replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));

}

void NetworkRecorder::StartRecordAll()
{

    if (b_TrrigerChecked)
    {
        //ui.statusBar->setStyleSheet("color: blue");
        //ui.statusBar->setStyleSheet("QStatusBar{padding-left:12px;background:rgba(0,0,0,0);color:blue;font-weight:bold;}");

        //ui.statusBar->setStyleSheet();

        ui.actionTimeTrigger->setEnabled(!b_TrrigerChecked);
        b_TrrigerChecked = false;
    }

    ui.statusBar->showMessage("Waiting for trigger - Local computer time", 3000);
    timeTriggerTimer->start(100);
}

void NetworkRecorder::StopRecord()
{

    //QWidget * pwidget = focusWidget();
    auto networkmanager = NetworkDeviceManager::GetInstance();

    QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
    auto& devices = networkmanager->GetRecordDevices();
    if (indexes.size() > 0)
    {
        for (int i=0; i< indexes.size(); i++)
        {
            //QModelIndex index = indexes.first();
            QModelIndex index = indexes[i];
            size_t idx = index.row();
            if (devices.size() > idx)
            {
                devices[idx]->RecordEnd();
            }
        }
    }
    int nRow = recordModel.rowCount() - 1;
    recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));


    QModelIndexList indexes_replay = ui.replayTableView->selectionModel()->selection().indexes();
    auto& replyDevices = networkmanager->GetReplayDevices();
    for (int i = 0; i < indexes_replay.size(); ++i)
    {
        QModelIndex index = indexes_replay.at(i);
        size_t idx = index.row();
        if (replyDevices.size() > idx)
        {
            replyDevices[idx]->RecordEnd();
        }
    }
    int nRow_replay = replayModel.rowCount() - 1;
    replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow_replay, 4));



//    auto networkmanager = NetworkDeviceManager::GetInstance();
//    auto& devices = networkmanager->GetRecordDevices();
//    for(int idx = 0; idx < devices.size(); idx++)
//    {
//        devices[idx]->RecordEnd();
//    }

}

void NetworkRecorder::SetUp()
{
	QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	auto& devices = networkmanager->GetRecordDevices();
	for (int i = 0; i < indexes.size(); ++i)
	{
		QModelIndex index = indexes.at(i);
		size_t idx = index.row();
		if (devices.size() > idx)
		{
			devices[idx]->SetRunOption(WRITE_TO_DEVICE);
		}
	}
	int nRow = recordModel.rowCount() - 1;
	recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
}

void NetworkRecorder::SetDown()
{
	QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	auto& devices = networkmanager->GetRecordDevices();
	for (int i = 0; i < indexes.size(); ++i)
	{
		QModelIndex index = indexes.at(i);
		size_t idx = index.row();
		if (devices.size() > idx)
		{
			devices[idx]->SetRunOption(READ_FROM_DEVICE);
		}
	}
	int nRow = recordModel.rowCount() - 1;
	recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
}

void NetworkRecorder::Enable()
{
	QWidget * pwidget = focusWidget();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	if (pwidget == ui.recordTableView)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto& devices = networkmanager->GetRecordDevices();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->SetActiveFlag(true);
			}
		}
		int nRow = recordModel.rowCount() - 1;
		recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
	}
	if (pwidget == ui.replayTableView)
	{
		QModelIndexList indexes = ui.replayTableView->selectionModel()->selection().indexes();
		auto& replyDevices = networkmanager->GetReplayDevices();
		for (int i = 0; i < indexes.size(); ++i)
		{
			QModelIndex index = indexes.at(i);
			size_t idx = index.row();
			if (replyDevices.size() > idx)
			{
				replyDevices[idx]->SetActiveFlag(true);
			}
		}
		int nRow = replayModel.rowCount() - 1;
		replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));
	}
}

void NetworkRecorder::Disable()
{
	QWidget * pwidget = focusWidget();
	auto networkmanager = NetworkDeviceManager::GetInstance();
	if (pwidget == ui.recordTableView)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto& devices = networkmanager->GetRecordDevices();
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->SetActiveFlag(false);
			}
		}
		int nRow = recordModel.rowCount() - 1;
		recordModel.dataChanged(recordModel.index(0, 3), recordModel.index(nRow, 4));
	}
	if (pwidget == ui.replayTableView)
	{
		QModelIndexList indexes = ui.replayTableView->selectionModel()->selection().indexes();
		auto& replyDevices = networkmanager->GetReplayDevices();
		for (int i = 0; i < indexes.size(); ++i)
		{
			QModelIndex index = indexes.at(i);
			size_t idx = index.row();
			if (replyDevices.size() > idx)
			{
				replyDevices[idx]->SetActiveFlag(false);
			}
		}
		int nRow = replayModel.rowCount() - 1;
		replayModel.dataChanged(replayModel.index(0, 3), replayModel.index(nRow, 4));
	}
}

void NetworkRecorder::UpdateSessions()
{
	auto setting = DeviceRecordSetting::GetInstance();
	time_t now;
	time(&now);
	tm n = *localtime(&now);
	double diff = difftime(mktime(&setting->RecordTriggerTime), mktime(&n));
	if (0 >= diff)
	{
        ui.statusBar->showMessage("Record is triggered", 5000);
		timeTriggerTimer->stop();
		ui.actionTimeTrigger->setCheckable(false);
		auto networkmanager = NetworkDeviceManager::GetInstance();
		networkmanager->Record();
		ResetUi();
        timer->start(100);
        timer2->start(1000);

        b_TrrigerChecked = true;
	}
}

void NetworkRecorder::InsertTagBefore()
{
	QModelIndex index = ui.userTagTableView->selectionModel()->currentIndex();
	auto mgr = UserTagManager::GetInstance();
    mgr->Insert(index.row() + 1);
	ui.userTagTableView->setModel(0);
	ui.userTagTableView->setModel(&userTagModel);
}

void NetworkRecorder::InsertTagAfter()
{
	QModelIndex index = ui.userTagTableView->selectionModel()->currentIndex();
    auto mgr = UserTagManager::GetInstance();
    mgr->Insert(index.row());
	ui.userTagTableView->setModel(0);
	ui.userTagTableView->setModel(&userTagModel);
}

void NetworkRecorder::RemoveTag()
{
	QModelIndex index = ui.userTagTableView->selectionModel()->currentIndex();
	auto mgr = UserTagManager::GetInstance();
	mgr->Remove(index.row());
	ui.userTagTableView->setModel(0);
	ui.userTagTableView->setModel(&userTagModel);
}

void NetworkRecorder::AddResetTagToSession()
{
	QModelIndexList tags = ui.userTagTableView->selectionModel()->selection().indexes();
	if (tags.size() > 0)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto networkmanager = NetworkDeviceManager::GetInstance();
		auto& devices = networkmanager->GetRecordDevices();
		QString mss = " Tagged channel : ";
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->AddTag(tags.front().row(), false);
				mss += QString::number(idx) + " " + QString::number(tags.front().row());
			}
		}
		ui.statusBar->showMessage(mss, 1000);
	}
}

void NetworkRecorder::AddTagToSession()
{
	QModelIndexList tags = ui.userTagTableView->selectionModel()->selection().indexes();
	if (tags.size() > 0)
	{
		QModelIndexList indexes = ui.recordTableView->selectionModel()->selection().indexes();
		auto networkmanager = NetworkDeviceManager::GetInstance();
		auto& devices = networkmanager->GetRecordDevices();
		QString mss = " Tagged channel : ";
		if (indexes.size() > 0)
		{
			QModelIndex index = indexes.first();
			size_t idx = index.row();
			if (devices.size() > idx)
			{
				devices[idx]->AddTag(tags.front().row(), true);
				mss += QString::number(idx) + " " + QString::number(tags.front().row());
			}
		}
		ui.statusBar->showMessage(mss, 1000);
	}
}

void NetworkRecorder::SaveUISetting()
{
	QSettings settings("DaqSuite", "NetworkRecorder");
	settings.beginGroup("UI");
	settings.setValue("size", size());
	settings.setValue("pos", pos());
	settings.endGroup();
}

void NetworkRecorder::LoadUISetting()
{
    QSettings settings("DaqSuite", "NetworkRecorder");
	settings.beginGroup("UI");
	resize(settings.value("size", QSize(800, 600)).toSize());
	move(settings.value("pos", QPoint(200, 200)).toPoint());
	settings.endGroup();
}

void NetworkRecorder::LoadSetting()
{
    //std::string path = DeviceRecordSetting::GetInstance()->GetRecordPath(0);
    //NET 53 Changed default setting file location.
    std::string path = m_path;

	QString fileName = QFileDialog::getOpenFileName(this, tr("Setting File"), path.c_str(), tr("Files (*.setting)"));
	if (fileName != "")
	{
		NetworkDeviceManager::GetInstance()->ReadSystemInfo(fileName.toStdString());
		ui.userTagTableView->setModel(0);
		ui.userTagTableView->setModel(&userTagModel);
        ui.recordTableView->setModel(0);
		ui.recordTableView->setModel(&recordModel);
        ui.recordTableView->resizeColumnsToContents();

		ui.replayTableView->setModel(0);
		ui.replayTableView->setModel(&replayModel);
        ui.replayTableView->resizeColumnsToContents();
		ui.recordingOptionTableView->setModel(0);
		ui.recordingOptionTableView->setModel(&sessionModel);
        ui.recordingOptionTableView->resizeColumnsToContents();
	}

    InitProgressbar();
    //LoadUISetting();

}

void NetworkRecorder::SaveSetting()
{
    //std::string path = DeviceRecordSetting::GetInstance()->GetRecordPath(0);
    //NET 53 Changed default setting file location.
    std::string path = m_path;

	QString fileName = QFileDialog::getSaveFileName(this, tr("Setting File"), path.c_str(), tr("Files (*.setting)"));
    fileName += ".setting";
	if (fileName != "")
	{
		NetworkDeviceManager::GetInstance()->WriteSystemInfo(fileName.toStdString());
	}
}

void NetworkRecorder::SettingServer()
{
    if (remoteUI)
	{
		remoteUI->getSetting();
		remoteUI->show();
	}
}

void NetworkRecorder::InitServer()
{
	server->init(NetworkDeviceManager::GetInstance()->ServerPort);
}


void NetworkRecorder::on_tabWidget_currentChanged(int index)
{
    if(index == 1)
    {
        ui.statusBar->showMessage("Loading storage information.", 1000);

        std::vector<DiskInfo> drives;
        drives = settingModel.currentChanged();

        //ui.storageTableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

        //set bar value
        for(int i = 0; i<(int)drives.size(); i++)
        {
            //
            //storagebar
            //
            auto& drive = drives[i];
            //int value = drive.available / drive.capacity * 100;
            int value = drive.available / (drive.free + drive.available) * 100;
            storagebar[i]->setValue(value);

            //
            //storagebar_life
            //

            unsigned long long unit = 0;
            if(drive.unit == "GB")
                unit = 1;
            else if(drive.unit == "TB")
                unit = 1000;
            else if(drive.unit == "PB")
                unit = 1000 * 1000;

            double m_written = (drive.written * unit) / (drive.life_cycle * 1000 * 1000) * 100;
            storagebar_life[i]->setValue(m_written);

            //disable

            //ui.storageTableView->verticalHeader()->setVisible(false);


        }
    }

}

