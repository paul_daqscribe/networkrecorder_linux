#ifndef RECORDCHANNELMODEL_H
#define RECORDCHANNELMODEL_H

#include <QAbstractTableModel>
namespace NetworkLib
{
   class NetworkDeviceManager;
}

class RecordChannelModel : public QAbstractTableModel
{
   Q_OBJECT

public:

   RecordChannelModel(QObject *parent);

   ~RecordChannelModel();

   Qt::ItemFlags flags(const QModelIndex &index) const;

   int rowCount(const QModelIndex &parent = QModelIndex()) const;

   int columnCount(const QModelIndex &parent = QModelIndex()) const;

   QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

   bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

   QVariant headerData(int section, Qt::Orientation orientation, int role) const;

private:

   NetworkLib::NetworkDeviceManager* manager;

};
#endif // RECORDCHANNELMODEL_H
