#ifndef SETTINGMODELDELEGATE_H
#define SETTINGMODELDELEGATE_H

#include <QItemDelegate>

class SettingModelDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	SettingModelDelegate(QObject *parent);

	~SettingModelDelegate();

    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index);

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;


    //const QStyleOptionProgressBar progressBarOption;
};

#endif // SETTINGMODELDELEGATE_H
