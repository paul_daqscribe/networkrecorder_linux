#include "recordchannelmodel.h"
#include "NetworkDeviceManager.h"
using namespace NetworkLib;
#include "pcap_header.h"
#include "SFPDPDevice.h"

RecordChannelModel::RecordChannelModel(QObject *parent)
   : QAbstractTableModel(parent)
{
   manager = NetworkDeviceManager::GetInstance();
}

RecordChannelModel::~RecordChannelModel()
{
}


Qt::ItemFlags RecordChannelModel::flags(const QModelIndex &index) const
{
   Qt::ItemFlags flags = QAbstractTableModel::flags(index);
   if (!NetworkDeviceManager::GetInstance()->IsRunning())
   {
	   if (index.column() == 1)
	   {
		   return flags | Qt::ItemIsUserCheckable;
	   }

       //NET-44 add Filtered stream
       //Fixed channel name to be changeable
       else if (index.column() >= 0 && index.column() < 4)
	   {
		   return flags | Qt::ItemIsEditable;
	   }
   }
   return flags;
}

int RecordChannelModel::rowCount(const QModelIndex &) const
{
   return (int)manager->GetRecordDevices().size();
}

int RecordChannelModel::columnCount(const QModelIndex &) const
{
   return 10;
}

QVariant RecordChannelModel::data(const QModelIndex &index, int role) const
{
   if (role == Qt::DisplayRole || role == Qt::EditRole)
   {
      const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetRecordDevices();
      if (index.row() < (int)devices.size())
      {
         const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
         if (index.column() == 0)
         {
			 return QString(pDevice->Name.c_str());
		 }
         else if (index.column() == 2)
         {
            return QString::number(pDevice->GetRunNumber());
         }
		 else if (index.column() == 3)
		 {
			 return QString::number(pDevice->GetRecordDuration() / 1000.0, 'f', 3);
		 }
		 else if (index.column() == 4)
		 {
             return QString::number(pDevice->RemainRecordTime() / 1000.0, 'f', 3);
		 }
		 else if (index.column() == 5)
         {
             return QString::number(pDevice->GetCurrentSpeed() / 1000000.0, 'f', 2);
         }
         else if (index.column() == 6)
         {
			 return QString::number(pDevice->RecordSpeed() / 1000000.0, 'f', 2);
         }
		 else if (index.column() == 7)
		 {
             return QString::number(pDevice->GetTotalDataSize() / 1000000.0, 'f', 2);
		 }
		 else if (index.column() == 8)
         {
            return pDevice->IsRecording() ? "Recording" : "Stopped";
         }
         else if (index.column() == 9)
         {
            //QString A = QString::number(pDevice->GetBufferLevel);
            //QString B = pDevice->ErrorInfo.c_str();

            //debug mode
//            return QString::number(pDevice->GetMemcpySpeed(), 'f', 0) + "_" +
            //QString::number(pDevice->GetBufferLevel(), 'f', 0);

            //real mode
            return pDevice->ErrorInfo.c_str();
         }
      }
   }
   else if (role == Qt::CheckStateRole)
   {
	   if (index.column() == 1)
	   {
		   const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetRecordDevices();
		   if (index.row() < (int)devices.size())
		   {
			   const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
			   return (pDevice->GetActiveFlag() ? Qt::Checked : Qt::Unchecked);
		   }
	   }
   }
   return QVariant();
}

bool RecordChannelModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
   if (role == Qt::EditRole)
   {
      const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetRecordDevices();
      if (index.row() < (int)devices.size())
      {
         const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
		 if (index.column() == 2)
         {
            pDevice->SetRunNumber(value.toInt());
         }
		 else if (index.column() == 3)
		 {
            //NET-56 Recording time can not be set nagative.
            if(value > 0)
                pDevice->SetRecordDuration(value.toDouble() * 1000);
            else
                pDevice->SetRecordDuration(0);
		 }
         //NET-44 add Filtered stream
         //Fixed channel name to be changeable
         else if (index.column() == 0)
         {
             pDevice->SetName(value.toString().toStdString());
         }
		 return true;
      }
   }
   else if (role == Qt::CheckStateRole)
   {
	   if (index.column() == 1)
	   {
		   const std::vector<std::shared_ptr<INetworkDevice>>& devices = manager->GetRecordDevices();
		   if (index.row() < (int)devices.size())
		   {
			   const std::shared_ptr<INetworkDevice>& pDevice = devices[index.row()];
			   pDevice->SetActiveFlag((value.toInt() == Qt::Checked ? true : false));
		   }
		   return true;
	   }
   }
   return false;
}

QVariant RecordChannelModel::headerData(int section, Qt::Orientation orientation, int role) const
{
   if (role == Qt::DisplayRole)
   {
      if (orientation == Qt::Horizontal)
      {
         if (section == 0)
         {
            return QString("Name");
         }
         else if (section == 1)
         {
            return QString("Active");
         }
         else if (section == 2)
         {
            return QString("Run Number");
         }
		 else if (section == 3)
		 {
			 return QString("Duration (Sec)");
		 }
		 else if (section == 4)
		 {
			 return QString("Remaining Time (Sec)");
		 }
		 else if (section == 5)
         {
            return QString("Data Speed\n(MBytes/sec)");
         }
		 else if (section == 6)
		 {
             return QString("Record Speed\n(MBytes/sec)");
		 }
		 else if (section == 7)
         {
            return QString("Data Size\n(MBytes)");
         }
         else if (section == 8)
         {
            return QString("Status");
         }
         else if (section == 9)
         {
            return QString("Messages");
         }
      }
      else
      {
         return QString::number(section + 1);
      }
   }

   return QVariant();
}
