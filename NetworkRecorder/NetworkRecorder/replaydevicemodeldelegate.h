#ifndef REPLAYDEVICEMODELDELEGATE_H
#define REPLAYDEVICEMODELDELEGATE_H

#include <QItemDelegate>
namespace NetworkLib
{
	class NetworkDeviceManager;
}

class ReplayDeviceModelDelegate : public QItemDelegate
{
	Q_OBJECT

public:

	ReplayDeviceModelDelegate(QObject *parent);

	virtual QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

	virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

	virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

public slots:

	void buttonClickedRecord();

private:

	NetworkLib::NetworkDeviceManager* manager;

};

#endif // REPLAYDEVICEMODELDELEGATE_H
