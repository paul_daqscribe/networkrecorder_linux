TEMPLATE = app
TARGET = NetworkRecorder
DESTDIR = ../build
QT += core widgets gui network serialport

win32:RC_FILE = $${PWD}/NetworkRecorder.rc

win32: DEFINES += _XKEYCHECK_H
DEFINES += _NT_TOOLS \
        _NTAPI_EXTDESCR_7_ \
        HAVE_STRING_H \
        _NT_HOST_64BIT \
         D_FILE_OFFSET_BITS=64
INCLUDEPATH += . \
    ./../../../boost_1_63_0

win32:contains(QMAKE_HOST.arch, x86_64) LIBS += -L"./../../../../boost_1_63_0/lib64-msvc-12.0/"
else:win32: LIBS += -L"./../../../../boost_1_63_0/lib32-msvc-12.0/"

win32: INCLUDEPATH += ./../../../ThirdParty/winpcap/Include \
    ./../../../ThirdParty/SFPDP/include

win32:contains(QMAKE_HOST.arch, x86_64) LIBS += -L"./../../../ThirdParty/winpcap/Lib/x64/"
else:win32: LIBS += -L"./../../../ThirdParty/winpcap/Lib/"

win32: LIBS += -lwpcap \
    -lPacket\
    -lWs2_32 \
    -L./../../../ThirdParty/Napatech/lib -llibntapi64 \
    -L./../../../ThirdParty/Napatech/lib -llibntutil64

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../build/ -lNetworkLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../build/ -lNetworkLib
else:unix: LIBS += -L$$OUT_PWD/../../build/ -lNetworkLib

INCLUDEPATH += $$PWD/../../NetworkLib
DEPENDPATH += $$PWD/../../NetworkLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/libNetworkLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/libNetworkLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/NetworkLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../build/NetworkLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../build/libNetworkLib.a


INCLUDEPATH += $$PWD/../../../../boost_1_63_0
DEPENDPATH += $$PWD/../../../../boost_1_63_0

unix: LIBS += -L$$PWD/../../../../boost_1_63_0/stage/lib/ -lboost_thread
unix: LIBS += -L$$PWD/../../../../boost_1_63_0/stage/lib/ -lboost_filesystem
unix: LIBS += -L$$PWD/../../../../boost_1_63_0/stage/lib/ -lboost_system

unix: LIBS += -L/opt/napatech3/lib -lpcap \
                -L/opt/napatech3/lib -lntapi \
                -L/opt/napatech3/lib -lntutil

include(NetworkRecorder.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/release/ -lrt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/debug/ -lrt
else:unix: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lrt

INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/release/librt.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/debug/librt.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/release/rt.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/debug/rt.lib
#else:unix: PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/librt.a

#unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lnuma

#INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
#DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu

#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/libnuma.a

SOURCES +=


unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib64/ -lnuma

INCLUDEPATH += $$PWD/../../../../../../usr/lib64
DEPENDPATH += $$PWD/../../../../../../usr/lib64
