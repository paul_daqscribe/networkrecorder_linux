#include "networkrecorder.h"
#include <QtWidgets/QApplication>
#include <qdir.h>
#include <QProcess>
#include "DeviceRecordSetting.h"
using namespace NetworkLib;
//#define TEST

//#define RedHat
#define Ubuntu

#ifndef TEST
int main(int argc, char *argv[])
{
#ifdef WIN32
    std::string path = "C:\\ProgramData\\DaqScribe";
#else

    #ifdef Ubuntu
    std::string path = (QDir::homePath() + "/DaqScribe/").toStdString();
    #else
    //std::string path = "/home/ddr7040/DaqScribe/";

    QProcess process;
    QString command = "who -s";
    process.start(command);
    process.waitForFinished();
    QString res = process.readAll().data();
    QStringList parts = res.split(" ");
    QString& part = parts[0];
    std::string path = "/home/" + part.toStdString() + "/DaqScribe/";

    #endif

    DeviceRecordSetting * deviceSetting = DeviceRecordSetting::GetInstance();
    if (deviceSetting->GetRecordPath(0) == "")
    {
        deviceSetting->SetRecordPath((QDir::homePath() + "/Simple Record").toStdString(),0);
    }

    if (deviceSetting->GetRecordPath(1) == "")
    {
        deviceSetting->SetRecordPath((QDir::homePath() + "/Simple Record").toStdString(),1);
    }

    if (deviceSetting->GetRecordPath(2) == "")
    {
        deviceSetting->SetRecordPath((QDir::homePath() + "/Simple Record").toStdString(),2);
    }

    if (deviceSetting->GetRecordPath(3) == "")
    {
        deviceSetting->SetRecordPath((QDir::homePath() + "/Simple Record").toStdString(),3);
    }
#endif
    //QApplication::addLibraryPath("/home/ddr70/lib");

    QApplication a(argc, argv);
    NetworkRecorder w(path);
    w.show();
    return a.exec();
}

#else
#include "RecordController.h"
#include <boost/thread/thread.hpp>

int main()
{
    DeviceRecordSetting * deviceSetting = DeviceRecordSetting::GetInstance();
    if (deviceSetting->GetRecordPath1() == "")
    {
        deviceSetting->SetRecordPath1((QDir::homePath() + "/Simple Record/").toStdString());
    }
    std::shared_ptr<RecordController> recorder(new RecordController());
    recorder->filename = deviceSetting->GetRecordPath1();
    recorder->FileHeader.resize(6);
    recorder->RecordDuration = 2000000;
    memcpy(&recorder->FileHeader[0], "header", 6);
    recorder->Open(false);
    std::vector<std::string> out;
    std::string out1;
    out1.resize(5011, '0');
    std::string out2;
    out2.resize(10011, '1');
    std::string out3;
    out3.resize(5011, '2');
    std::string out4;
    out4.resize(10011, '3');
    out.push_back(out1);
    out.push_back(out2);
    out.push_back(out3);
    out.push_back(out4);
    int niteration = 236;
    for (int i = 0; i < niteration; ++i)
    {
        int idx = (i % (int)out.size());
        recorder->Write((char*)out[idx].c_str(), (int)out[idx].size());
        Sleep(100);
    }
    Sleep(2000);
    recorder->Close();

    recorder->SetReplaySessionBySize(0, 10000000);
    recorder->Open(true);
    int i = 0;
    std::string line;
    unsigned int  len = 0;
    while (recorder->ReadNext(line))
    {
        int idx = (i % (int)out.size());
        if (out[idx] != line)
        {
            std::cout << "Something wrong at " << i + 1 << "\n";
            //std::cout << out[idx];
            //std::cout << "\n";
            //std::cout << line;
            //std::cout << "\n";
        }
        ++i;
        len += line.size();
        Sleep(150);
    }
    std::cout << i << " : " << len << "\n";

    recorder->Close();
    //Sleep(200);

    return 1;
}
#endif
