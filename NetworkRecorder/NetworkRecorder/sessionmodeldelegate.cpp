#include "sessionmodeldelegate.h"
#include "NetworkDeviceManager.h"
#include "SFPDPDevice.h"
using namespace NetworkLib;
#include "DeviceRecordSetting.h"
#include <QSpinBox>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <QMouseEvent>
#include <qfiledialog.h>
#include <qpainter.h>

SessionModelDelegate::SessionModelDelegate(QObject *parent)
	: QItemDelegate(parent)
{

}

SessionModelDelegate::~SessionModelDelegate()
{
}

void SessionModelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QItemDelegate::paint(painter, option, index);
}

QWidget* SessionModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (index.row() == 1 || index.row() == 2 || index.row() == 3 || index.row() == 4 || index.row() == 7)
	{
        QPushButton* button = new QPushButton(parent);
        button->setText("Browse");
        if (index.row() == 1)
            connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord1()));
        if (index.row() == 2)
            connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord2()));
        if (index.row() == 3)
            connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord3()));
        if (index.row() == 4)
            connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedRecord4()));
        if (index.row() == 7)
            connect(button, SIGNAL(clicked()), this, SLOT(buttonClickedReplay()));
        return button;
	}
	return QItemDelegate::createEditor(parent, option, index);
}

void SessionModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    //if (index.row() == 1 || index.row() == 5|| index.row() == 2)
    if (index.row() == 1 || index.row() == 2 || index.row() == 3 || index.row() == 4 || index.row() == 7)
	{
	}
	else
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void SessionModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    //if (index.row() == 1 || index.row() == 5|| index.row() == 2)
    if (index.row() == 1 || index.row() == 2 || index.row() == 3 || index.row() == 4 || index.row() == 7)
	{
	}
	else
	{
		QItemDelegate::setModelData(editor, model, index);
	}
}

void SessionModelDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
	editor->setGeometry(option.rect);
}

void SessionModelDelegate::buttonClickedRecord1()
{
	QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
    if (fileName != "") DeviceRecordSetting::GetInstance()->SetRecordPath(fileName.toStdString(), 0);
}

void SessionModelDelegate::buttonClickedRecord2()
{
    QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
    if (fileName != "") DeviceRecordSetting::GetInstance()->SetRecordPath(fileName.toStdString(), 1);
}

void SessionModelDelegate::buttonClickedRecord3()
{
    QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
    if (fileName != "") DeviceRecordSetting::GetInstance()->SetRecordPath(fileName.toStdString(), 2);
}

void SessionModelDelegate::buttonClickedRecord4()
{
    QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
    if (fileName != "") DeviceRecordSetting::GetInstance()->SetRecordPath(fileName.toStdString(), 3);
}

void SessionModelDelegate::buttonClickedReplay()
{
	QString fileName = QFileDialog::getExistingDirectory(0, tr("Project Directory"), "");
	if (fileName != "") DeviceRecordSetting::GetInstance()->SetReplayPath(fileName.toStdString());
}
