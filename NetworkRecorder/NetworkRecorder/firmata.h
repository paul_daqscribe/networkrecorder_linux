#ifndef FIRMATA_H
#define FIRMATA_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include "settingsdialog.h"

namespace Firmata
{
	struct Pin
	{
		Pin() : mode(255), analog_channel(127), supported_modes(0), value(0) {}
		unsigned char mode;
		unsigned char analog_channel;
		unsigned long long supported_modes;
		unsigned int value;
	};

	enum PinMode
	{
		MODE_INPUT = 0x00,
		MODE_OUTPUT = 0x01,
		MODE_ANALOG = 0x02,
		MODE_PWM = 0x03,
		MODE_SERVO = 0x04,
		MODE_SHIFT = 0x05,
		MODE_I2C = 0x06,
		MODE_ONEWIRE = 0x07,
		MODE_STEPPER = 0x08,
		MODE_ENCODER = 0x09,
		MODE_SERIAL = 0x0A,
		MODE_PULLUP = 0x0B,
		MODE_IGNORE = 0x7F
	};

	enum MessageType
	{
		START_SYSEX = 0xF0,
		END_SYSEX = 0xF7,
		PIN_MODE_QUERY = 0x72,
		PIN_MODE_RESPONSE = 0x73,
		PIN_STATE_QUERY = 0x6D,
		PIN_STATE_RESPONSE = 0x6E,
		CAPABILITY_QUERY = 0x6B,
		CAPABILITY_RESPONSE = 0x6C,
		ANALOG_MAPPING_QUERY = 0x69,
		ANALOG_MAPPING_RESPONSE = 0x6A,
		REPORT_FIRMWARE = 0x79
	};

	class FirmataController : public QObject
	{
		Q_OBJECT
	public:
		FirmataController();
		~FirmataController();
		std::vector<Pin> pins;
		void setIcon(const QIcon& icon);
		void writeSetting();
		void readSetting();
	public slots:
		void openui();
		void open();
		void close();
		void writeData(const QByteArray &data);
		void readData();
		void parse(QByteArray data);
		void on_message();
		void changemode(int pin, PinMode mode);
	signals:
		void on_input_up(int idx);
	private:
		QSerialPort* serial;
		SettingsDialog *settings;
		std::vector<unsigned char> parse_buf;
		QByteArray data;
		int parse_command_len;
		int parse_count;
		QString firmata_name;
	};
}
#endif
