/****************************************************************************
** Meta object code from reading C++ file 'sessionmodeldelegate.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../NetworkRecorder/NetworkRecorder/sessionmodeldelegate.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sessionmodeldelegate.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SessionModelDelegate_t {
    QByteArrayData data[7];
    char stringdata0[126];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SessionModelDelegate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SessionModelDelegate_t qt_meta_stringdata_SessionModelDelegate = {
    {
QT_MOC_LITERAL(0, 0, 20), // "SessionModelDelegate"
QT_MOC_LITERAL(1, 21, 20), // "buttonClickedRecord1"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 20), // "buttonClickedRecord2"
QT_MOC_LITERAL(4, 64, 20), // "buttonClickedRecord3"
QT_MOC_LITERAL(5, 85, 20), // "buttonClickedRecord4"
QT_MOC_LITERAL(6, 106, 19) // "buttonClickedReplay"

    },
    "SessionModelDelegate\0buttonClickedRecord1\0"
    "\0buttonClickedRecord2\0buttonClickedRecord3\0"
    "buttonClickedRecord4\0buttonClickedReplay"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SessionModelDelegate[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x0a /* Public */,
       3,    0,   40,    2, 0x0a /* Public */,
       4,    0,   41,    2, 0x0a /* Public */,
       5,    0,   42,    2, 0x0a /* Public */,
       6,    0,   43,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SessionModelDelegate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SessionModelDelegate *_t = static_cast<SessionModelDelegate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->buttonClickedRecord1(); break;
        case 1: _t->buttonClickedRecord2(); break;
        case 2: _t->buttonClickedRecord3(); break;
        case 3: _t->buttonClickedRecord4(); break;
        case 4: _t->buttonClickedReplay(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject SessionModelDelegate::staticMetaObject = {
    { &QItemDelegate::staticMetaObject, qt_meta_stringdata_SessionModelDelegate.data,
      qt_meta_data_SessionModelDelegate,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SessionModelDelegate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SessionModelDelegate::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SessionModelDelegate.stringdata0))
        return static_cast<void*>(const_cast< SessionModelDelegate*>(this));
    return QItemDelegate::qt_metacast(_clname);
}

int SessionModelDelegate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QItemDelegate::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
