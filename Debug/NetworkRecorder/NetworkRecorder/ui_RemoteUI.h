/********************************************************************************
** Form generated from reading UI file 'RemoteUI.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REMOTEUI_H
#define UI_REMOTEUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RemoteUI
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *label;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpinBox *spinBox;
    QLabel *label_3;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QSpinBox *firmataPortSpinBox;
    QPushButton *settingButton;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QDialogButtonBox *buttonBox;

    void setupUi(QWidget *RemoteUI)
    {
        if (RemoteUI->objectName().isEmpty())
            RemoteUI->setObjectName(QStringLiteral("RemoteUI"));
        RemoteUI->setWindowModality(Qt::NonModal);
        RemoteUI->resize(320, 155);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(RemoteUI->sizePolicy().hasHeightForWidth());
        RemoteUI->setSizePolicy(sizePolicy);
        RemoteUI->setMinimumSize(QSize(320, 140));
        RemoteUI->setMaximumSize(QSize(320, 155));
        verticalLayout = new QVBoxLayout(RemoteUI);
        verticalLayout->setSpacing(3);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(3, 3, 3, 3);
        groupBox = new QGroupBox(RemoteUI);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout = new QFormLayout(groupBox);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        frame_2 = new QFrame(groupBox);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_2);
        horizontalLayout_2->setSpacing(1);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(1, 1, 1, 1);
        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setLayoutDirection(Qt::LeftToRight);
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(label_2);

        spinBox = new QSpinBox(frame_2);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximum(1000000);
        spinBox->setSingleStep(1);
        spinBox->setValue(5999);

        horizontalLayout_2->addWidget(spinBox);


        formLayout->setWidget(0, QFormLayout::FieldRole, frame_2);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        frame_3 = new QFrame(groupBox);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(0, 0));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_3);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(frame_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_3->addWidget(label_4);

        firmataPortSpinBox = new QSpinBox(frame_3);
        firmataPortSpinBox->setObjectName(QStringLiteral("firmataPortSpinBox"));
        firmataPortSpinBox->setMinimum(1);
        firmataPortSpinBox->setMaximum(16);

        horizontalLayout_3->addWidget(firmataPortSpinBox);

        settingButton = new QPushButton(frame_3);
        settingButton->setObjectName(QStringLiteral("settingButton"));

        horizontalLayout_3->addWidget(settingButton);


        formLayout->setWidget(2, QFormLayout::FieldRole, frame_3);


        verticalLayout->addWidget(groupBox);

        frame = new QFrame(RemoteUI);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setSpacing(3);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(3, 3, 3, 3);
        buttonBox = new QDialogButtonBox(frame);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);

        horizontalLayout->addWidget(buttonBox);


        verticalLayout->addWidget(frame);


        retranslateUi(RemoteUI);

        QMetaObject::connectSlotsByName(RemoteUI);
    } // setupUi

    void retranslateUi(QWidget *RemoteUI)
    {
        RemoteUI->setWindowTitle(QApplication::translate("RemoteUI", "Remote Control", 0));
        groupBox->setTitle(QApplication::translate("RemoteUI", "Remode Control Setting", 0));
        label->setText(QApplication::translate("RemoteUI", "UDP Server", 0));
        label_2->setText(QApplication::translate("RemoteUI", "Port : ", 0));
        label_3->setText(QApplication::translate("RemoteUI", "Trigger", 0));
        label_4->setText(QApplication::translate("RemoteUI", "Port", 0));
        settingButton->setText(QApplication::translate("RemoteUI", "Setting", 0));
    } // retranslateUi

};

namespace Ui {
    class RemoteUI: public Ui_RemoteUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REMOTEUI_H
