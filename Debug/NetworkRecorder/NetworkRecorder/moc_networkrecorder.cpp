/****************************************************************************
** Meta object code from reading C++ file 'networkrecorder.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../NetworkRecorder/NetworkRecorder/networkrecorder.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'networkrecorder.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_NetworkRecorder_t {
    QByteArrayData data[30];
    char stringdata0[378];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NetworkRecorder_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NetworkRecorder_t qt_meta_stringdata_NetworkRecorder = {
    {
QT_MOC_LITERAL(0, 0, 15), // "NetworkRecorder"
QT_MOC_LITERAL(1, 16, 7), // "Monitor"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 11), // "StartRecord"
QT_MOC_LITERAL(4, 37, 14), // "StartRecordAll"
QT_MOC_LITERAL(5, 52, 10), // "StopRecord"
QT_MOC_LITERAL(6, 63, 12), // "UpdateStatus"
QT_MOC_LITERAL(7, 76, 17), // "UpdateStatus_1sec"
QT_MOC_LITERAL(8, 94, 7), // "ResetUi"
QT_MOC_LITERAL(9, 102, 5), // "SetUp"
QT_MOC_LITERAL(10, 108, 7), // "SetDown"
QT_MOC_LITERAL(11, 116, 6), // "Enable"
QT_MOC_LITERAL(12, 123, 7), // "Disable"
QT_MOC_LITERAL(13, 131, 14), // "UpdateSessions"
QT_MOC_LITERAL(14, 146, 15), // "InsertTagBefore"
QT_MOC_LITERAL(15, 162, 14), // "InsertTagAfter"
QT_MOC_LITERAL(16, 177, 9), // "RemoveTag"
QT_MOC_LITERAL(17, 187, 15), // "AddTagToSession"
QT_MOC_LITERAL(18, 203, 20), // "AddResetTagToSession"
QT_MOC_LITERAL(19, 224, 11), // "LoadSetting"
QT_MOC_LITERAL(20, 236, 11), // "SaveSetting"
QT_MOC_LITERAL(21, 248, 13), // "SettingServer"
QT_MOC_LITERAL(22, 262, 10), // "InitServer"
QT_MOC_LITERAL(23, 273, 15), // "InitProgressbar"
QT_MOC_LITERAL(24, 289, 14), // "SetProgressbar"
QT_MOC_LITERAL(25, 304, 21), // "SetSessionProgressbar"
QT_MOC_LITERAL(26, 326, 6), // "SetGUI"
QT_MOC_LITERAL(27, 333, 10), // "SetStorage"
QT_MOC_LITERAL(28, 344, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(29, 372, 5) // "index"

    },
    "NetworkRecorder\0Monitor\0\0StartRecord\0"
    "StartRecordAll\0StopRecord\0UpdateStatus\0"
    "UpdateStatus_1sec\0ResetUi\0SetUp\0SetDown\0"
    "Enable\0Disable\0UpdateSessions\0"
    "InsertTagBefore\0InsertTagAfter\0RemoveTag\0"
    "AddTagToSession\0AddResetTagToSession\0"
    "LoadSetting\0SaveSetting\0SettingServer\0"
    "InitServer\0InitProgressbar\0SetProgressbar\0"
    "SetSessionProgressbar\0SetGUI\0SetStorage\0"
    "on_tabWidget_currentChanged\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NetworkRecorder[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      27,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  149,    2, 0x0a /* Public */,
       3,    0,  150,    2, 0x0a /* Public */,
       4,    0,  151,    2, 0x0a /* Public */,
       5,    0,  152,    2, 0x0a /* Public */,
       6,    0,  153,    2, 0x0a /* Public */,
       7,    0,  154,    2, 0x0a /* Public */,
       8,    0,  155,    2, 0x0a /* Public */,
       9,    0,  156,    2, 0x0a /* Public */,
      10,    0,  157,    2, 0x0a /* Public */,
      11,    0,  158,    2, 0x0a /* Public */,
      12,    0,  159,    2, 0x0a /* Public */,
      13,    0,  160,    2, 0x0a /* Public */,
      14,    0,  161,    2, 0x0a /* Public */,
      15,    0,  162,    2, 0x0a /* Public */,
      16,    0,  163,    2, 0x0a /* Public */,
      17,    0,  164,    2, 0x0a /* Public */,
      18,    0,  165,    2, 0x0a /* Public */,
      19,    0,  166,    2, 0x0a /* Public */,
      20,    0,  167,    2, 0x0a /* Public */,
      21,    0,  168,    2, 0x0a /* Public */,
      22,    0,  169,    2, 0x0a /* Public */,
      23,    0,  170,    2, 0x0a /* Public */,
      24,    0,  171,    2, 0x0a /* Public */,
      25,    0,  172,    2, 0x0a /* Public */,
      26,    0,  173,    2, 0x0a /* Public */,
      27,    0,  174,    2, 0x0a /* Public */,
      28,    1,  175,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   29,

       0        // eod
};

void NetworkRecorder::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        NetworkRecorder *_t = static_cast<NetworkRecorder *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->Monitor(); break;
        case 1: _t->StartRecord(); break;
        case 2: _t->StartRecordAll(); break;
        case 3: _t->StopRecord(); break;
        case 4: _t->UpdateStatus(); break;
        case 5: _t->UpdateStatus_1sec(); break;
        case 6: _t->ResetUi(); break;
        case 7: _t->SetUp(); break;
        case 8: _t->SetDown(); break;
        case 9: _t->Enable(); break;
        case 10: _t->Disable(); break;
        case 11: _t->UpdateSessions(); break;
        case 12: _t->InsertTagBefore(); break;
        case 13: _t->InsertTagAfter(); break;
        case 14: _t->RemoveTag(); break;
        case 15: _t->AddTagToSession(); break;
        case 16: _t->AddResetTagToSession(); break;
        case 17: _t->LoadSetting(); break;
        case 18: _t->SaveSetting(); break;
        case 19: _t->SettingServer(); break;
        case 20: _t->InitServer(); break;
        case 21: _t->InitProgressbar(); break;
        case 22: _t->SetProgressbar(); break;
        case 23: _t->SetSessionProgressbar(); break;
        case 24: _t->SetGUI(); break;
        case 25: _t->SetStorage(); break;
        case 26: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject NetworkRecorder::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_NetworkRecorder.data,
      qt_meta_data_NetworkRecorder,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *NetworkRecorder::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NetworkRecorder::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_NetworkRecorder.stringdata0))
        return static_cast<void*>(const_cast< NetworkRecorder*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int NetworkRecorder::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 27)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 27;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 27)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 27;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
