/********************************************************************************
** Form generated from reading UI file 'networkrecorder.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NETWORKRECORDER_H
#define UI_NETWORKRECORDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NetworkRecorderClass
{
public:
    QAction *actionRun;
    QAction *actionInsertTagBefore;
    QAction *actionInsertTagAfter;
    QAction *actionRemoveTag;
    QAction *actionAddTagToSession;
    QAction *actionStartRecord;
    QAction *actionStopRecord;
    QAction *actionLoadSetting;
    QAction *actionSaveSetting;
    QAction *actionRunServer;
    QAction *actionTimeTrigger;
    QAction *actionAddResetTagToRecord;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QFrame *session_frame;
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QVBoxLayout *verticalLayout_5;
    QLabel *label;
    QTableView *recordingOptionTableView;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_2;
    QTableView *userTagTableView;
    QTabWidget *tabWidget;
    QWidget *channelsTab;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QTableView *recordTableView;
    QLabel *label_4;
    QTableView *replayTableView;
    QWidget *storageInformationTab;
    QVBoxLayout *verticalLayout_3;
    QTableView *storageTableView;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *NetworkRecorderClass)
    {
        if (NetworkRecorderClass->objectName().isEmpty())
            NetworkRecorderClass->setObjectName(QStringLiteral("NetworkRecorderClass"));
        NetworkRecorderClass->resize(792, 713);
        QIcon icon;
        icon.addFile(QStringLiteral(":/DaqSuite/Resources/daqscribe-logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        NetworkRecorderClass->setWindowIcon(icon);
        actionRun = new QAction(NetworkRecorderClass);
        actionRun->setObjectName(QStringLiteral("actionRun"));
        actionRun->setCheckable(true);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/DaqSuite/Resources/start.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon1.addFile(QStringLiteral(":/DaqSuite/Resources/stop_session_active.png"), QSize(), QIcon::Normal, QIcon::On);
        icon1.addFile(QStringLiteral(":/DaqSuite/Resources/start_session_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionRun->setIcon(icon1);
        actionInsertTagBefore = new QAction(NetworkRecorderClass);
        actionInsertTagBefore->setObjectName(QStringLiteral("actionInsertTagBefore"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/DaqSuite/Resources/insert a record above.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon2.addFile(QStringLiteral(":/DaqSuite/Resources/insert_above_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionInsertTagBefore->setIcon(icon2);
        actionInsertTagAfter = new QAction(NetworkRecorderClass);
        actionInsertTagAfter->setObjectName(QStringLiteral("actionInsertTagAfter"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/DaqSuite/Resources/insert a record below.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon3.addFile(QStringLiteral(":/DaqSuite/Resources/insert_below_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionInsertTagAfter->setIcon(icon3);
        actionRemoveTag = new QAction(NetworkRecorderClass);
        actionRemoveTag->setObjectName(QStringLiteral("actionRemoveTag"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/DaqSuite/Resources/delete a record.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon4.addFile(QStringLiteral(":/DaqSuite/Resources/delete_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionRemoveTag->setIcon(icon4);
        actionAddTagToSession = new QAction(NetworkRecorderClass);
        actionAddTagToSession->setObjectName(QStringLiteral("actionAddTagToSession"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/DaqSuite/Resources/set a tag.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon5.addFile(QStringLiteral(":/DaqSuite/Resources/set_tag_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionAddTagToSession->setIcon(icon5);
        actionStartRecord = new QAction(NetworkRecorderClass);
        actionStartRecord->setObjectName(QStringLiteral("actionStartRecord"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/DaqSuite/Resources/play.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon6.addFile(QStringLiteral(":/DaqSuite/Resources/play_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionStartRecord->setIcon(icon6);
        actionStopRecord = new QAction(NetworkRecorderClass);
        actionStopRecord->setObjectName(QStringLiteral("actionStopRecord"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/DaqSuite/Resources/stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon7.addFile(QStringLiteral(":/DaqSuite/Resources/stop_session_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionStopRecord->setIcon(icon7);
        actionLoadSetting = new QAction(NetworkRecorderClass);
        actionLoadSetting->setObjectName(QStringLiteral("actionLoadSetting"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/DaqSuite/Resources/load setup.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon8.addFile(QStringLiteral(":/DaqSuite/Resources/load_setup_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionLoadSetting->setIcon(icon8);
        actionSaveSetting = new QAction(NetworkRecorderClass);
        actionSaveSetting->setObjectName(QStringLiteral("actionSaveSetting"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/DaqSuite/Resources/save setup.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon9.addFile(QStringLiteral(":/DaqSuite/Resources/save_setup_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionSaveSetting->setIcon(icon9);
        actionRunServer = new QAction(NetworkRecorderClass);
        actionRunServer->setObjectName(QStringLiteral("actionRunServer"));
        actionRunServer->setCheckable(false);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/DaqSuite/Resources/settings.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon10.addFile(QStringLiteral(":/DaqSuite/Resources/settings_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionRunServer->setIcon(icon10);
        actionRunServer->setAutoRepeat(false);
        actionTimeTrigger = new QAction(NetworkRecorderClass);
        actionTimeTrigger->setObjectName(QStringLiteral("actionTimeTrigger"));
        actionTimeTrigger->setCheckable(false);
        actionTimeTrigger->setEnabled(false);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/DaqSuite/Resources/schedule.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon11.addFile(QStringLiteral(":/DaqSuite/Resources/schedule_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionTimeTrigger->setIcon(icon11);
        actionAddResetTagToRecord = new QAction(NetworkRecorderClass);
        actionAddResetTagToRecord->setObjectName(QStringLiteral("actionAddResetTagToRecord"));
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/DaqSuite/Resources/reset a tag.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon12.addFile(QStringLiteral(":/DaqSuite/Resources/reset_tag_active.png"), QSize(), QIcon::Active, QIcon::Off);
        actionAddResetTagToRecord->setIcon(icon12);
        centralWidget = new QWidget(NetworkRecorderClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(3);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(3, 3, 3, 3);
        session_frame = new QFrame(centralWidget);
        session_frame->setObjectName(QStringLiteral("session_frame"));
        session_frame->setMinimumSize(QSize(0, 200));
        session_frame->setMaximumSize(QSize(16777215, 200));
        session_frame->setFrameShape(QFrame::StyledPanel);
        session_frame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(session_frame);
        horizontalLayout->setSpacing(3);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(session_frame);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame);
        verticalLayout_5->setSpacing(3);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(9);
        font.setBold(false);
        font.setWeight(50);
        label->setFont(font);

        verticalLayout_5->addWidget(label);

        recordingOptionTableView = new QTableView(frame);
        recordingOptionTableView->setObjectName(QStringLiteral("recordingOptionTableView"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(recordingOptionTableView->sizePolicy().hasHeightForWidth());
        recordingOptionTableView->setSizePolicy(sizePolicy);
        recordingOptionTableView->setMinimumSize(QSize(0, 0));
        recordingOptionTableView->setMaximumSize(QSize(16777215, 16777215));
        QFont font1;
        font1.setPointSize(9);
        recordingOptionTableView->setFont(font1);
        recordingOptionTableView->setAlternatingRowColors(true);
        recordingOptionTableView->horizontalHeader()->setStretchLastSection(true);
        recordingOptionTableView->verticalHeader()->setDefaultSectionSize(25);
        recordingOptionTableView->verticalHeader()->setMinimumSectionSize(20);
        recordingOptionTableView->verticalHeader()->setStretchLastSection(false);

        verticalLayout_5->addWidget(recordingOptionTableView);


        horizontalLayout->addWidget(frame);

        frame_2 = new QFrame(session_frame);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame_2);
        verticalLayout_4->setSpacing(3);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);

        verticalLayout_4->addWidget(label_2);

        userTagTableView = new QTableView(frame_2);
        userTagTableView->setObjectName(QStringLiteral("userTagTableView"));
        userTagTableView->setContextMenuPolicy(Qt::ActionsContextMenu);
        userTagTableView->setAlternatingRowColors(true);
        userTagTableView->setSelectionMode(QAbstractItemView::SingleSelection);
        userTagTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        userTagTableView->horizontalHeader()->setMinimumSectionSize(25);
        userTagTableView->horizontalHeader()->setStretchLastSection(true);
        userTagTableView->verticalHeader()->setMinimumSectionSize(20);
        userTagTableView->verticalHeader()->setStretchLastSection(false);

        verticalLayout_4->addWidget(userTagTableView);


        horizontalLayout->addWidget(frame_2);


        verticalLayout_2->addWidget(session_frame);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setFont(font);
        tabWidget->setFocusPolicy(Qt::StrongFocus);
        tabWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
        channelsTab = new QWidget();
        channelsTab->setObjectName(QStringLiteral("channelsTab"));
        verticalLayout = new QVBoxLayout(channelsTab);
        verticalLayout->setSpacing(3);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(3, 3, 3, 3);
        label_3 = new QLabel(channelsTab);
        label_3->setObjectName(QStringLiteral("label_3"));
        QFont font2;
        font2.setPointSize(9);
        font2.setBold(true);
        font2.setWeight(75);
        label_3->setFont(font2);

        verticalLayout->addWidget(label_3);

        recordTableView = new QTableView(channelsTab);
        recordTableView->setObjectName(QStringLiteral("recordTableView"));
        recordTableView->setFont(font);
        recordTableView->setContextMenuPolicy(Qt::ActionsContextMenu);
        recordTableView->setAlternatingRowColors(true);
        recordTableView->setSelectionMode(QAbstractItemView::ExtendedSelection);
        recordTableView->setSelectionBehavior(QAbstractItemView::SelectItems);
        recordTableView->horizontalHeader()->setCascadingSectionResizes(true);
        recordTableView->horizontalHeader()->setDefaultSectionSize(100);
        recordTableView->horizontalHeader()->setMinimumSectionSize(30);
        recordTableView->horizontalHeader()->setStretchLastSection(true);
        recordTableView->verticalHeader()->setDefaultSectionSize(25);
        recordTableView->verticalHeader()->setMinimumSectionSize(20);

        verticalLayout->addWidget(recordTableView);

        label_4 = new QLabel(channelsTab);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font2);

        verticalLayout->addWidget(label_4);

        replayTableView = new QTableView(channelsTab);
        replayTableView->setObjectName(QStringLiteral("replayTableView"));
        replayTableView->setFont(font);
        replayTableView->setContextMenuPolicy(Qt::ActionsContextMenu);
        replayTableView->setAlternatingRowColors(true);
        replayTableView->setSelectionBehavior(QAbstractItemView::SelectItems);
        replayTableView->horizontalHeader()->setMinimumSectionSize(30);
        replayTableView->horizontalHeader()->setStretchLastSection(true);
        replayTableView->verticalHeader()->setDefaultSectionSize(25);
        replayTableView->verticalHeader()->setMinimumSectionSize(20);

        verticalLayout->addWidget(replayTableView);

        tabWidget->addTab(channelsTab, QString());
        storageInformationTab = new QWidget();
        storageInformationTab->setObjectName(QStringLiteral("storageInformationTab"));
        verticalLayout_3 = new QVBoxLayout(storageInformationTab);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        storageTableView = new QTableView(storageInformationTab);
        storageTableView->setObjectName(QStringLiteral("storageTableView"));
        storageTableView->setFont(font1);
        storageTableView->setAlternatingRowColors(true);
        storageTableView->horizontalHeader()->setCascadingSectionResizes(true);
        storageTableView->horizontalHeader()->setStretchLastSection(true);
        storageTableView->verticalHeader()->setStretchLastSection(false);

        verticalLayout_3->addWidget(storageTableView);

        tabWidget->addTab(storageInformationTab, QString());

        verticalLayout_2->addWidget(tabWidget);

        NetworkRecorderClass->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(NetworkRecorderClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setMovable(false);
        mainToolBar->setIconSize(QSize(40, 40));
        NetworkRecorderClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(NetworkRecorderClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        NetworkRecorderClass->setStatusBar(statusBar);

        mainToolBar->addAction(actionRun);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionTimeTrigger);
        mainToolBar->addAction(actionStartRecord);
        mainToolBar->addAction(actionAddTagToSession);
        mainToolBar->addAction(actionAddResetTagToRecord);
        mainToolBar->addAction(actionStopRecord);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionInsertTagBefore);
        mainToolBar->addAction(actionInsertTagAfter);
        mainToolBar->addAction(actionRemoveTag);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionRunServer);
        mainToolBar->addAction(actionLoadSetting);
        mainToolBar->addAction(actionSaveSetting);
        mainToolBar->addSeparator();

        retranslateUi(NetworkRecorderClass);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(NetworkRecorderClass);
    } // setupUi

    void retranslateUi(QMainWindow *NetworkRecorderClass)
    {
        NetworkRecorderClass->setWindowTitle(QApplication::translate("NetworkRecorderClass", "DaqScribe Network Recorder 1.7.0", 0));
        actionRun->setText(QApplication::translate("NetworkRecorderClass", "Run or Stop Session", 0));
#ifndef QT_NO_TOOLTIP
        actionRun->setToolTip(QApplication::translate("NetworkRecorderClass", "Run or Stop Session", 0));
#endif // QT_NO_TOOLTIP
        actionInsertTagBefore->setText(QApplication::translate("NetworkRecorderClass", "Insert user tag before current tag", 0));
#ifndef QT_NO_TOOLTIP
        actionInsertTagBefore->setToolTip(QApplication::translate("NetworkRecorderClass", "Insert user tag before current tag", 0));
#endif // QT_NO_TOOLTIP
        actionInsertTagAfter->setText(QApplication::translate("NetworkRecorderClass", "Insert user tag after current tag", 0));
#ifndef QT_NO_TOOLTIP
        actionInsertTagAfter->setToolTip(QApplication::translate("NetworkRecorderClass", "Insert user tag after current tag", 0));
#endif // QT_NO_TOOLTIP
        actionRemoveTag->setText(QApplication::translate("NetworkRecorderClass", "Remove the current user tag", 0));
#ifndef QT_NO_TOOLTIP
        actionRemoveTag->setToolTip(QApplication::translate("NetworkRecorderClass", "Remove the current user tag", 0));
#endif // QT_NO_TOOLTIP
        actionAddTagToSession->setText(QApplication::translate("NetworkRecorderClass", "Add set tag to the current record", 0));
#ifndef QT_NO_TOOLTIP
        actionAddTagToSession->setToolTip(QApplication::translate("NetworkRecorderClass", "Add set tag to the current record", 0));
#endif // QT_NO_TOOLTIP
        actionAddTagToSession->setShortcut(QString());
        actionStartRecord->setText(QApplication::translate("NetworkRecorderClass", "Start Record/Relay", 0));
#ifndef QT_NO_TOOLTIP
        actionStartRecord->setToolTip(QApplication::translate("NetworkRecorderClass", "Start Record/Relay", 0));
#endif // QT_NO_TOOLTIP
        actionStopRecord->setText(QApplication::translate("NetworkRecorderClass", "Stop Record/Relay", 0));
#ifndef QT_NO_TOOLTIP
        actionStopRecord->setToolTip(QApplication::translate("NetworkRecorderClass", "Stop Record/Relay", 0));
#endif // QT_NO_TOOLTIP
        actionLoadSetting->setText(QApplication::translate("NetworkRecorderClass", "LoadSetting", 0));
#ifndef QT_NO_TOOLTIP
        actionLoadSetting->setToolTip(QApplication::translate("NetworkRecorderClass", "Load Setting", 0));
#endif // QT_NO_TOOLTIP
        actionSaveSetting->setText(QApplication::translate("NetworkRecorderClass", "SaveSetting", 0));
#ifndef QT_NO_TOOLTIP
        actionSaveSetting->setToolTip(QApplication::translate("NetworkRecorderClass", "Save Setting", 0));
#endif // QT_NO_TOOLTIP
        actionRunServer->setText(QApplication::translate("NetworkRecorderClass", "Run server", 0));
#ifndef QT_NO_TOOLTIP
        actionRunServer->setToolTip(QApplication::translate("NetworkRecorderClass", "Run server", 0));
#endif // QT_NO_TOOLTIP
        actionTimeTrigger->setText(QApplication::translate("NetworkRecorderClass", "Time Trigger", 0));
#ifndef QT_NO_TOOLTIP
        actionTimeTrigger->setToolTip(QApplication::translate("NetworkRecorderClass", "Start Time Trigger", 0));
#endif // QT_NO_TOOLTIP
        actionAddResetTagToRecord->setText(QApplication::translate("NetworkRecorderClass", "Add reset tag to the current record", 0));
        label->setText(QApplication::translate("NetworkRecorderClass", "Session Information", 0));
        label_2->setText(QApplication::translate("NetworkRecorderClass", "User Tags", 0));
        label_3->setText(QApplication::translate("NetworkRecorderClass", "Record", 0));
        label_4->setText(QApplication::translate("NetworkRecorderClass", "Replay", 0));
        tabWidget->setTabText(tabWidget->indexOf(channelsTab), QApplication::translate("NetworkRecorderClass", "Channels", 0));
        tabWidget->setTabText(tabWidget->indexOf(storageInformationTab), QApplication::translate("NetworkRecorderClass", "Storage and Recording Information", 0));
    } // retranslateUi

};

namespace Ui {
    class NetworkRecorderClass: public Ui_NetworkRecorderClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NETWORKRECORDER_H
