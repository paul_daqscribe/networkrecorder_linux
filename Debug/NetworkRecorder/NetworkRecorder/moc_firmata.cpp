/****************************************************************************
** Meta object code from reading C++ file 'firmata.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../NetworkRecorder/NetworkRecorder/firmata.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'firmata.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Firmata__FirmataController_t {
    QByteArrayData data[16];
    char stringdata0[131];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Firmata__FirmataController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Firmata__FirmataController_t qt_meta_stringdata_Firmata__FirmataController = {
    {
QT_MOC_LITERAL(0, 0, 26), // "Firmata::FirmataController"
QT_MOC_LITERAL(1, 27, 11), // "on_input_up"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 3), // "idx"
QT_MOC_LITERAL(4, 44, 6), // "openui"
QT_MOC_LITERAL(5, 51, 4), // "open"
QT_MOC_LITERAL(6, 56, 5), // "close"
QT_MOC_LITERAL(7, 62, 9), // "writeData"
QT_MOC_LITERAL(8, 72, 4), // "data"
QT_MOC_LITERAL(9, 77, 8), // "readData"
QT_MOC_LITERAL(10, 86, 5), // "parse"
QT_MOC_LITERAL(11, 92, 10), // "on_message"
QT_MOC_LITERAL(12, 103, 10), // "changemode"
QT_MOC_LITERAL(13, 114, 3), // "pin"
QT_MOC_LITERAL(14, 118, 7), // "PinMode"
QT_MOC_LITERAL(15, 126, 4) // "mode"

    },
    "Firmata::FirmataController\0on_input_up\0"
    "\0idx\0openui\0open\0close\0writeData\0data\0"
    "readData\0parse\0on_message\0changemode\0"
    "pin\0PinMode\0mode"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Firmata__FirmataController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   62,    2, 0x0a /* Public */,
       5,    0,   63,    2, 0x0a /* Public */,
       6,    0,   64,    2, 0x0a /* Public */,
       7,    1,   65,    2, 0x0a /* Public */,
       9,    0,   68,    2, 0x0a /* Public */,
      10,    1,   69,    2, 0x0a /* Public */,
      11,    0,   72,    2, 0x0a /* Public */,
      12,    2,   73,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 14,   13,   15,

       0        // eod
};

void Firmata::FirmataController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FirmataController *_t = static_cast<FirmataController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_input_up((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->openui(); break;
        case 2: _t->open(); break;
        case 3: _t->close(); break;
        case 4: _t->writeData((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 5: _t->readData(); break;
        case 6: _t->parse((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 7: _t->on_message(); break;
        case 8: _t->changemode((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< PinMode(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (FirmataController::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FirmataController::on_input_up)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject Firmata::FirmataController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Firmata__FirmataController.data,
      qt_meta_data_Firmata__FirmataController,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Firmata::FirmataController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Firmata::FirmataController::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Firmata__FirmataController.stringdata0))
        return static_cast<void*>(const_cast< FirmataController*>(this));
    return QObject::qt_metacast(_clname);
}

int Firmata::FirmataController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void Firmata::FirmataController::on_input_up(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
