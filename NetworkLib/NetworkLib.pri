HEADERS += ./DataSpeedMonitor.h \
    ./DeviceRecordSetting.h \
    ./GESFPDPDevice.h \
    ./INetworkDevice.h \
    ./INetworkDeviceFactory.h \
    ./NetworkDevice.h \
    ./NetworkDeviceFactory.h \
    ./NetworkDeviceManager.h \
    ./NTNetworkDevice.h \
    ./SFPDPDevice.h \
    ./StorageManager.h \
    ./logger.h \
    ./pcap_header.h \
    ./usertagmanager.h \
    ./RecordController.h
SOURCES += ./DataSpeedMonitor.cpp \
    ./DeviceRecordSetting.cpp \
    ./GESFPDPDevice.cpp \
    ./INetworkDevice.cpp \
    ./NetworkDevice.cpp \
    ./NetworkDeviceFactory.cpp \
    ./NetworkDeviceManager.cpp \
    ./NTNetworkDevice.cpp \
    ./SFPDPDevice.cpp \
    ./StorageManager.cpp \
    ./logger.cpp \
    ./usertagmanager.cpp \
    ./RecordController.cpp

