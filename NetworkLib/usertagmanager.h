#ifndef USERTAGMANAGER_H
#define USERTAGMANAGER_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <vector>
#include <string>

namespace NetworkLib
{
	/**
	 * UserTag stores the user tag information.
	 */
	struct UserTag
	{
		/// Tag name.
		std::string Name;
		/// Description.
		std::string Description;
	};

	/// UserTagManager holds the all tag information.
	class UserTagManager
	{

	public:

		static UserTagManager * GetInstance();

		/// All user tags available.
		std::vector<UserTag> Tags;

		/// Insert tag at the index of idx.
		void Insert(int idx);

		/// Remove tag at the index of idx.
		void Remove(int idx);

	private:

		/// Singleton instance.
		static UserTagManager* instance;

		UserTagManager() {}

	};

}
#endif // USERTAGMANAGER_H
