#ifndef RECORDCONTROLLER_H
#define RECORDCONTROLLER_H

#include <string>
#include <fstream>
#ifdef WIN32
#include <Windows.h>
#else
#define Sleep(x) usleep(1000*x)
#define DWORD unsigned int
#define INVALID_HANDLE_VALUE -1
#define TRUE 1
#define FALSE 0
#endif
#include <vector>
#include <chrono>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include <memory>
#include <aio.h>

#define HEADER_NT 128

namespace NetworkLib
{
	/**
	* RecordController provides the functionality of current record state for each port
	* and asynchrronious file record and save the record tag file.
	*/
	class RecordController
	{

	public:

		/// Default constructor.
		RecordController();

		/// Destructor.
		~RecordController();

		/**
		* Open initializes the record file if it is using asynchronious file write and open record tag file.
		* @param record The flag to record/relay.
		*/
        void Open(bool bRead, int ch);

        void PreOpen(bool bRead, int ch);

		/// Close closes the record.
		void Close();

		/// Accessor for the file name.
		std::string GetFileName();

		/**
		* AddTagToRecord saves a tag to the record tag file.
		* @param tagidx The tag index to be written.
		*/
		void AddTagToRecord(int tagidx, bool bset);

		unsigned long long AddMetaTagToRecord(unsigned long long tagidx, bool bset, const std::string& time);

		/**
		* Write saves the data asynchroniously.
		* @param data The data to be stored.
		* @param size The data size to be stored.
		* @param force The flag to store without running.
		*/
		void Write(char* data, unsigned int size, bool force = false);
        void Write1(char* data, unsigned int size, int seq, int thread_leftsize, int newidx, bool force = false);
        void Write2(char* data, unsigned int size, int seq, int thread_leftsize, int newidx, bool force = false);
        void Write3(char* data, unsigned int size, int seq, int thread_leftsize, int newidx, bool force = false);
        void Write4(char* data, unsigned int size, int seq, int thread_leftsize, int newidx, bool force = false);

		bool ReadNext(std::string& buffer);

		void Flush();

		void FlushRemain();

        void FlushRemainBuffer();

		/// The getter for the file status.

        bool IsOpen() { return hFile != -1; }

		/// NextFile increases the file index and get the file.
		void NextFile();

		/// GetDurationFromTags reads the tag file and get the total duration of the record. 
		void GetDurationFromTags(bool tag);

		/// The number of record for each port.
		int RecordNumber;

		/// The recorded data size.
		long long TotalData;

		unsigned long long LastMinuteData;

		/// The record port id. Usually port + 1.
		int ID;

		/// The relay port id. Usually port + 1.
		int RelayID;

		/// The flag of recording.
        bool Running;

        bool EndRunning;

        bool m_NT_Running;

		/// Header storage to support the divided files.
		std::vector<char> FileHeader;

		/// The relay offset time in nanosecond.
		unsigned long long RelayOffset;

		/// The relay duration time in nanosecond.
		unsigned long long RelayDuration;

		/// The recorded time in nanosecond.
		unsigned long long TotalDuration;

		unsigned long long TotalDurationTag;

		/// The name of file to be saved or to be read.
		std::string filename;

		std::string error;

        int errorStatus = 0;

		bool DoesExist();


        unsigned long long RecordDuration;

		unsigned long long RemainRecordDuration();

		void TagFileOperation(bool begin);

		void SetReplaySessionByTime(unsigned long long b, unsigned long long d);

		void SetReplaySessionBySize(unsigned long long b, unsigned long long d);

		void SetReplaySessionByTag(unsigned long long b, unsigned long long d);

		unsigned long long timestamp;

		DWORD errorCode;

        unsigned long long n_current;

        unsigned long long n_written;


        long long BufferLevel;

        long long MemCpySpeed;

        aiocb my_aiocb;

        int Numa;

        bool FirstOpen;

        unsigned long long multiThread_leftsize;

        unsigned long NUM_BUFFERS;
        unsigned long BUFFER_SIZE;
        unsigned long SINGLE_BUFFER;

        unsigned long long leftsize;
        unsigned long long End_leftsize;
        int idx  =0;

        unsigned long long MAX_NUM_BUFFER_FILE;

        int m_deleteFileIndex = 1;

        /// The file index to keep track of multiple files.
        int nFiles;

        int nFilesAhead;

        void m_Test();

        int writeSizeThread[4];
        unsigned long writeSizeCount = 1;


	private:

		void GetError();



		/// The tag file stream.
		std::ofstream tag_out;

		/// The tag file stream.
		std::ofstream seg_out;

		std::shared_ptr<std::ifstream> seg_info;

		unsigned int headerlen;

		/// The record begin time.
		unsigned long long begin_time;

		/// The record begin time.
		unsigned long long record_begin_time;

		/// Handle for the asychronious file.

        int hFile;
        int hFile_remain;
		long long readdatasize;

		long long file_size;

		/// The data size to keep track of overlap writing.
		unsigned long long datasize;

		/// The file location to keep.
        /// DMA thread.
		std::shared_ptr<boost::thread> m_IOThread;

		/// DMA thread functor.
		struct Runner;
		std::shared_ptr<Runner> m_IORunner;

		/// The buffer for DMA operation.
        //static std::vector<char*> buffer;


        std::vector<char*> buffer;
        std::vector<char*> buffer_test;
        //std::vector<by*> buffer;

        char* t_buffer;

		std::vector<char*> leftbuffer;

		std::vector<unsigned long long> remainBufferSize;

        unsigned long long sagCutSize;

        unsigned long long leftSagCutSize;

        int temp = 0;


        unsigned long long threadSize1;
        unsigned long long threadSize2;
        unsigned long long threadSize3;
        unsigned long long threadSize4;

        unsigned long long threadLeftsize1;
        unsigned long long threadLeftsize2;
        unsigned long long threadLeftsize3;
        unsigned long long threadLeftsize4;

        char* threadData1;
        char* threadData2;
        char* threadData3;
        char* threadData4;

        int threadIdx1;
        int threadIdx2;
        int threadIdx3;
        int threadIdx4;

        unsigned long long last_written;

        long long second_keep;
        long long second_keep2;

        //long long makeBufferCount = 0;

		unsigned long long max_file_size;

        int DebugMode = 0;

        DWORD flag = 0;

        mode_t mode;

		bool m_bRead;

		bool m_bHeader;

		long long replybegin;
		long long replyduration;
		long long replycurrent;
		long long replydatasize;

		double PRE_LOAD_RATE;

        unsigned long total_size;

        int ThreadSleepMemcpy=0;
        int ThreadSleepWrite =0;
        int AIOSleep = 0;

        int seg_info_count = 0;

        FILE *file_debug;


	};

}

#endif
