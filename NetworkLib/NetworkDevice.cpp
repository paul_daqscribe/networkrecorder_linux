#include "NetworkDevice.h"
#include "RecordController.h"

namespace NetworkLib
{
	struct NetworkDevice::WriteRunner
	{
		WriteRunner(NetworkDevice * device)
            : WrittenBufferCount(0)
			, ReadyBufferCount(0)
            , m_bRunning(true)
            , m_device(device)
		{
		}

		/**
		 * operator() is the implementation of the device running logic.
		 */
		void operator()()
		{
			if (m_device)
			{
				while (m_bRunning)
				{
					if (ReadyBufferCount > WrittenBufferCount)
					{
						m_device->Write();
					}
				}
			}
		}

		/**
		* Reset makes to start the thread.
		* @return The device is set to run continually.
		*/
		void Reset() { m_bRunning = true; }

		/**
		* Stop stops the thread.
		* @return The device is stopped.
		*/
		void Stop() { m_bRunning = false; }

		/**
		* IsRunning is the getter for the running state.
		* @return True if the device is running otherwise return false.
		*/
		bool IsRunning() { return m_bRunning; }

		int WrittenBufferCount;

		int ReadyBufferCount;

	private:

		/// The flag for running state of the thread.
		bool m_bRunning;

		int NumBuffers;

		bool m_bRead;

		NetworkDevice * m_device;

	};

#ifdef PCAPINCLUDE
	char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen, bool ipv4)
	{
		switch (sa->sa_family)
		{
		case AF_INET:
			if (ipv4)
			{
                //inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr), s, maxlen);
			}
			break;

		case AF_INET6:
			if (!ipv4)
			{
                //inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr), s, maxlen);
			}
			break;

		default:
			strncpy(s, "Unknown AF", maxlen);
			return NULL;
		}

		return s;
	}
#endif

	NetworkDevice::NetworkDevice(
#ifdef PCAPINCLUDE
		const pcap_if_t *d
#endif
		) 
#ifdef PCAPINCLUDE
        : adhandle(0)
        , filehandle(0)
        , dumpfile(0)
        , duration(0)
#endif
	{
		networkType = PCAP;
#ifdef PCAPINCLUDE
		if (d != 0)
		{
			Name = "Network Device";
			DeviceName = d->name;
			Description = d->description;
			pcap_addr_t *a;
			char ip6str[128] = { '\0' };

			for (a = d->addresses; a; a = a->next)
			{
				get_ip_str(a->addr, ip6str, sizeof(ip6str), true);
				Address = ip6str;
				if (Address != "") break;
			}
			if (Address.empty())
			{
				for (a = d->addresses; a; a = a->next)
				{
					get_ip_str(a->addr, ip6str, sizeof(ip6str), false);
					Address = ip6str;
					if (Address != "") break;
				}
			}
		}
#endif
      FileName = "NetworkDevice";
	}

	NetworkDevice::~NetworkDevice()
	{
		Stop();
	}

    void NetworkDevice::Monitor(int ch)
	{
		Status = "";
		if (!m_bActive) return;
		m_nSavedPackets = 0;
		m_SpeedMonitor.Reset();
		ErrorInfo.clear();
#ifdef PCAPINCLUDE
		char errbuf[PCAP_ERRBUF_SIZE];
		if ((adhandle = pcap_open_live(DeviceName.c_str(), 65536, 1, 1000, errbuf)) == NULL)
		{
			ErrorInfo = "NetworkDevice::Monitor() Failed to open device.";
			return;
		}

		if (m_runOption == READ_FROM_DEVICE)
		{
			dumpfile = pcap_dump_open(adhandle, recorder->GetFileName().c_str());

			if (PCAPFilter.length() > 0)
			{
				bpf_u_int32 NetMask = 0xffffff;
				bpf_program fcode;
				if (pcap_compile(adhandle, &fcode, PCAPFilter.c_str(), 1, NetMask) < 0)
				{
					ErrorInfo = "NetworkDevice::Monitor() Failed to compile filter.";
					return;
				}

				if (pcap_setfilter(adhandle, &fcode) < 0)
				{
					ErrorInfo = "NetworkDevice::Monitor() Failed to set filter.";
					return;
				}
			}

			if (dumpfile == NULL)
			{
				ErrorInfo = "NetworkDevice::Monitor() Failed to open file to save.";
				return;
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			filehandle = pcap_open_offline(FileName.c_str(), errbuf);
			
			if (PCAPFilter.length() > 0)
			{
				bpf_u_int32 NetMask = 0xffffff;
				bpf_program fcode;
			
				if (pcap_compile(adhandle, &fcode, PCAPFilter.c_str(), 1, NetMask) < 0)
				{
					ErrorInfo = "NetworkDevice::Monitor() Failed to compile filter.";
					return;
				}
			
				if (pcap_setfilter(adhandle, &fcode)<0)
				{
					ErrorInfo = "NetworkDevice::Monitor() Failed to set filter.";
					return;
				}
			}
			
			if (filehandle == NULL)
			{
				ErrorInfo = "NetworkDevice::Monitor() Failed to open file.";
				return;
			}
		}
#endif
		if (m_WriteRunner) m_WriteRunner.release();
		m_WriteRunner = std::unique_ptr<WriteRunner>(new WriteRunner(this));
		m_WriteThread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_WriteRunner)));

		if (m_Runner) m_Runner.release();
		m_Runner = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<NetworkDevice>(this)));
		m_Thread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner)));
	}

	void NetworkDevice::Write()
	{
#ifdef PCAPINCLUDE
		if (m_WriteRunner->ReadyBufferCount > m_WriteRunner->WrittenBufferCount)
		{
			int nPackets = 0;
			if (m_runOption == READ_FROM_DEVICE)
			{
				int idx = m_WriteRunner->WrittenBufferCount % NUM_BUFFER;
				if (m_nSavedPackets == 0)
					m_SpeedMonitor.Reset();

				if (recorder->Running)
				{
                    double t =(double)nt_clock();
					pcap_dump((unsigned char *)dumpfile, header[idx], pkt_data[idx]);
                    duration += (nt_clock() - t);
				}
				++m_nSavedPackets;
				nPackets = header[idx]->len;
			}
			else if (m_runOption == WRITE_TO_DEVICE)
			{
				if (recorder->Running)
				{
					int idx = m_WriteRunner->WrittenBufferCount % NUM_BUFFER;
					pcap_sendpacket(adhandle, pkt_data[idx], header[idx]->len);
					++m_nSavedPackets;
					nPackets = header[idx]->len;
				}
			}
			m_SpeedMonitor.UpdateData(nPackets);
			m_SpeedMonitor.EndSample();
			m_SpeedMonitor.BeginSample();
			for (size_t i = 0; i < m_Monitors.size(); ++i)
			{
				m_Monitors[i]->Update((int)m_nSavedPackets);
			}
			++m_WriteRunner->WrittenBufferCount;
		}
#endif
	}

	void NetworkDevice::HandlePacket()
	{
#ifdef PCAPINCLUDE
		int res = 0;
		if (m_runOption == READ_FROM_DEVICE)
		{
			int idx = m_WriteRunner->ReadyBufferCount % NUM_BUFFER;
			res = pcap_next_ex(adhandle, &header[idx], &pkt_data[idx]);
			if (res > 0)
			{
				++m_WriteRunner->ReadyBufferCount;
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			int idx = m_WriteRunner->ReadyBufferCount % NUM_BUFFER;
			res = pcap_next_ex(filehandle, &header[idx], &pkt_data[idx]);
			if (res > 0)
			{
				++m_WriteRunner->ReadyBufferCount;
			}
		}
#endif
	}

	void NetworkDevice::Stop()
	{
		if (m_Runner) m_Runner->Stop();
		if (m_WriteRunner) m_WriteRunner->Stop();

#ifdef PCAPINCLUDE
		if (adhandle)
		{
			pcap_stat ps;
			if (pcap_stats(adhandle, &ps) == 0)
			{
				if (m_runOption == READ_FROM_DEVICE)
				{
					Status = "\n  Received packets : " + std::to_string((long long)ps.ps_recv) + "\n  Droped packets (OS) : " + std::to_string((long long)ps.ps_drop);
					if (recorder->Running)
					{
						Status += "\n  Recorded data (MBytes) : " + std::to_string((long double)m_SpeedMonitor.GetTotalDataBytes() / 1000000.0);
						Status += "\n  Record time " + std::to_string((long double)(duration / 1000.0));
					}
				}
				else
				{
					Status = "\n  Sent packets : " + std::to_string((long long)ps.ps_recv) + "\n  Droped packets (OS) : " + std::to_string((long long)ps.ps_drop);
				}
			}
			pcap_close(adhandle);
		}
		adhandle = 0;
		if (dumpfile)
			pcap_dump_close(dumpfile);
		dumpfile = 0;

		if (filehandle)
			pcap_close(filehandle);
		filehandle = 0;
#endif
	}

	void NetworkDevice::StopThread()
	{
		if (m_Thread)
		{
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
            m_Thread.release();
		}
		if (m_WriteThread)
		{
			try
			{
				m_WriteThread->join();
			}
			catch (...)
			{
			}
			m_WriteThread.release();
		}
	}

	void NetworkDevice::RecordBegin()
	{
        recorder->Open(true, 0);
	}

	void NetworkDevice::RecordEnd()
	{
		recorder->Close();
	}
}
