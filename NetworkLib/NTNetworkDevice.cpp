#include "NTNetworkDevice.h"
#ifdef WIN32
#include <Windows.h>
#endif
#include "logger.h"
#include "RecordController.h"
#include "DeviceRecordSetting.h"
#include "NetworkDeviceManager.h"
#include <list>
#include <pthread.h>
#include <sched.h>
#include <iostream>
#include <fstream>
#include <mutex>
#include <QProcess>

std::mutex myMutex;

//std::ofstream outfile;

namespace NetworkLib
{

	NTNetworkDevice::NTNetworkDevice()
		: INetworkDevice()
	{
        Name = "NT-20E2-CH";
		FileName = "";
	}

	NTNetworkDevice::~NTNetworkDevice()
	{
	}

    void NTNetworkDevice::Monitor(bool bRead, int ch)
	{
        multiThreadSeq = 1;
        second_keep = nt_clock();

        m_NTRxGet_run = true;

        //kjk
        recorder->filename = FileName;
        recorder->PreOpen(bRead, ch);

		Status = "";
		m_nSavedPackets = 0;
		ErrorInfo.clear();
		m_SpeedMonitor.Reset();
		firstPacket = 1;
		status = NT_SUCCESS;
        int Numa = 0;
        auto pRecordSetting = DeviceRecordSetting::GetInstance();

		if (!m_bActive) return;

		bReplay = false;
		if (m_runOption == READ_FROM_DEVICE)
		{
			// Open a config stream to assign a filter to a stream ID.
			if ((status = NT_ConfigOpen(&hCfgStream, Name.c_str())) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_ConfigOpen() failed : " + std::string(errorBuffer);
				return;
			}

            std::string ntpl = "";

            //get group numa
            for(int x=0; x < pRecordSetting->v_Group.size(); x++)
            {
                for(int y = 0; y < pRecordSetting->v_Group[x].m_NumPort; y++)
                {
                     if(ch == pRecordSetting->v_Group[x].v_Port[y])
                     {
                         Numa = pRecordSetting->v_Group[x].m_NUMAnode;
                     }
                }
            }

            //Numa = 1;
            ntpl = "Setup[NUMANode=" + std::to_string(Numa)
                                + "] = StreamId=="+ std::to_string((long long)(GetID()));

            // Assign traffic to stream ID 1 and mask all traffic matching the assign statement color=7.
            //
            if ((status = NT_NTPL(hCfgStream, ntpl.c_str(), &ntplInfo, NT_NTPL_PARSER_VALIDATE_NORMAL)) != NT_SUCCESS)
            {
                // Get the status code as text
                NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                ErrorInfo = "NTNetworkDevice::Monitor() NT_NTPL() failed : " + std::string(errorBuffer);
                return;
            }

            //NET-44 add Filtered stream
            int portNum = NTNetworkDeviceManager::GetInstance()->nPorts;
            if(pRecordSetting->m_UserStream[0] != "" && GetID() > portNum)
            {
                ntpl = pRecordSetting->m_UserStream[GetID()- portNum - 1];
            }
            else
                ntpl = "Assign[streamid=" + std::to_string((long long)(GetID())) + ";color=7] = port == " + std::to_string((long long)(GetID()-1));

            //NET-44 add Filtered stream
            // Assign traffic to stream ID 1 and mask all traffic matching the assign statement +color=7.
            //
			if ((status = NT_NTPL(hCfgStream, ntpl.c_str(), &ntplInfo, NT_NTPL_PARSER_VALIDATE_NORMAL)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));

                //NET-44 add Filtered stream
                QString temp = QString::fromStdString(std::string(errorBuffer));
                if(temp.contains("Syntax"))
                    ErrorInfo = "Parsing failed : " + ntpl;
                else
                    ErrorInfo = "NTNetworkDevice::Monitor() NT_NTPL() failed : " + std::string(errorBuffer);
				return;
			}
			// Close the config stream
			if ((status = NT_ConfigClose(hCfgStream)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_ConfigClose() failed : " + std::string(errorBuffer);
				return;
			}
			// Used if PCAP header used
			pTs = (struct ntpcap_ts_s *)(void *)&(ntplInfo.ts);
			// Get a stream handle with stream ID 1. NT_NET_INTERFACE_SEGMENT specify that we will receive data in a segment based matter.
            if ((status = NT_NetRxOpen(&hNetRx, Name.c_str(), NT_NET_INTERFACE_SEGMENT, GetID(), -1)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Monitor() NT_NetRxOpen() failed : " + std::string(errorBuffer);
				return;
			}
		}

        m_Mode = pRecordSetting->m_Mode;
        if(pRecordSetting->m_Mode != "Y")
        {
            if (m_Runner) m_Runner.release();
            m_Runner = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
            m_Thread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner)));

            //// set cpu affinity ///////////
            unsigned long mask = 0;
            //auto pRecordSetting = DeviceRecordSetting::GetInstance();

            mask = pRecordSetting->v_CPUAffinityRead[ch-1];
            int reseult = 0;
            pthread_t threadID = (pthread_t)m_Thread->native_handle();


            //set
            reseult = pthread_setaffinity_np(threadID, sizeof(mask), (cpu_set_t *)&mask);
            //// set cpu affinity ////////////

            ///// set therad priority /////////

            int retcode;
            int policy;

            //pthread_t threadID = (pthread_t)m_Thread->native_handle();
            struct sched_param param;

            policy = SCHED_FIFO;
            int temp = pRecordSetting->ThreadPriorityMemcpy;
            param.sched_priority = temp;

            if ((retcode = pthread_setschedparam(threadID, policy, &param)) != 0)
            {
                errno = retcode;
                perror("pthread_setschedparam");
                exit(EXIT_FAILURE);
            }

            ///// set therad priority /////////
        }
        else if(pRecordSetting->m_Mode == "Y")
        {
            if (m_Runner) m_Runner.release();
            m_Runner = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
            m_Thread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner)));
            m_Runner->seq = 1;

            if (m_Runner2) m_Runner2.release();
            m_Runner2 = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
            m_Thread2 = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner2)));
            m_Runner2->seq = 2;

            if (m_Runner3) m_Runner3.release();
            m_Runner3 = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
            m_Thread3 = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner3)));
            m_Runner3->seq = 3;

            if (m_Runner4) m_Runner4.release();
            m_Runner4 = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
            m_Thread4 = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner4)));
            m_Runner4->seq = 4;

            //// set cpu affinity ///////////
            unsigned long mask = 0;
            //auto pRecordSetting = DeviceRecordSetting::GetInstance();

            mask = pRecordSetting->v_CPUAffinityRead[ch-1];

//            if (ch == 0)//port1
//                mask = pRecordSetting->v_CPUAffinityRead[ch-1];
//            else//port2
//                mask = pRecordSetting->v_CPUAffinityRead[ch-1];

            int reseult = 0;
            pthread_t threadID = (pthread_t)m_Thread->native_handle();

            //set
            reseult = pthread_setaffinity_np(threadID, sizeof(mask), (cpu_set_t *)&mask);

            mask = mask << 1;
            pthread_t threadID2 = (pthread_t)m_Thread2->native_handle();
            pthread_setaffinity_np(threadID2, sizeof(mask), (cpu_set_t *)&mask);

            mask = mask << 1;
            pthread_t threadID3 = (pthread_t)m_Thread3->native_handle();
            pthread_setaffinity_np(threadID3, sizeof(mask), (cpu_set_t *)&mask);

            mask = mask << 1;
            pthread_t threadID4 = (pthread_t)m_Thread4->native_handle();
            pthread_setaffinity_np(threadID4, sizeof(mask), (cpu_set_t *)&mask);
            //// set cpu affinity ////////////

            ///// set therad priority /////////

            int retcode;
            int policy;

            //pthread_t threadID = (pthread_t)m_Thread->native_handle();
            struct sched_param param;

            policy = SCHED_FIFO;
            int temp = pRecordSetting->ThreadPriorityMemcpy;
            param.sched_priority = temp;

            if ((retcode = pthread_setschedparam(threadID, policy, &param)) != 0)
            {
                errno = retcode;
                perror("pthread_setschedparam");
                exit(EXIT_FAILURE);
            }

            if ((retcode = pthread_setschedparam(threadID2, policy, &param)) != 0)
            {
                errno = retcode;
                perror("pthread_setschedparam");
                exit(EXIT_FAILURE);
            }

            if ((retcode = pthread_setschedparam(threadID3, policy, &param)) != 0)
            {
                errno = retcode;
                perror("pthread_setschedparam");
                exit(EXIT_FAILURE);
            }

            if ((retcode = pthread_setschedparam(threadID4, policy, &param)) != 0)
            {
                errno = retcode;
                perror("pthread_setschedparam");
                exit(EXIT_FAILURE);
            }

            ///// set therad priority /////////
        }


	}

	void NTNetworkDevice::Stop()
	{

        m_NT_run = false;

        if (!m_bActive) return;
        if (m_Runner) m_Runner->Stop();
        if (m_Runner2) m_Runner2->Stop();
        if (m_Runner3) m_Runner3->Stop();
        if (m_Runner4) m_Runner4->Stop();
        multiThread_leftsize = 0;

        usleep(1000);

        //recorder->FirstOpen = false;
		if (m_runOption == READ_FROM_DEVICE)
		{
			// Open a config stream to delete a filter.
			if ((status = NT_ConfigOpen(&hCfgStream, "TestStream")) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Stop() NT_ConfigOpen() failed : " + std::string(errorBuffer);

				return;
			}
			char tmpBuffer[20];
			// Delete the filter
            snprintf(tmpBuffer, 20, "delete=%d", ntplInfo.ntplId);
            //snprintf(tmpBuffer, 20, "delete=%d", 10);
			if ((status = NT_NTPL(hCfgStream, tmpBuffer, &ntplInfo, NT_NTPL_PARSER_VALIDATE_NORMAL)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Stop() NT_NTPL() failed : " + std::string(errorBuffer);

				return;
			}
			// Close the config stream
			if ((status = NT_ConfigClose(hCfgStream)) != NT_SUCCESS)
			{
				// Get the status code as text
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				ErrorInfo = "NTNetworkDevice::Stop() NT_ConfigClose() failed : " + std::string(errorBuffer);

				return;
			}
			// Close the stream and release the hostbuffer. This will also remove the NTPL assignments performed.
			NT_NetRxClose(hNetRx);
		}

	}

    void NTNetworkDevice::HandlePacket(int seq)
	{

        usleep(10);
        if(m_Mode != "Y")
        {
            if (!m_bActive) return;
            //if (!ErrorInfo.empty() || status != NT_SUCCESS) return;
            if (status != NT_SUCCESS) return;


            int numSegments = 0;              // The number of segments received
            int numBytes = 0;                 // The number of bytes received
            if (m_runOption == READ_FROM_DEVICE)
            {
                if (firstPacket == 1)
                {
                    // Read the file header.
                    readCmd.cmd = NT_NETRX_READ_CMD_GET_FILE_HEADER;
                    if ((status = NT_NetRxRead(hNetRx, &readCmd)) != NT_SUCCESS)
                    {
                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                        ErrorInfo = "NTNetworkDevice::HandlePacket() NT_NetRxRead() failed : " + std::string(errorBuffer);
                        return;
                    }
                    recorder->FileHeader.resize(readCmd.u.fileheader.size);

                    memcpy(&recorder->FileHeader[0], readCmd.u.fileheader.data, readCmd.u.fileheader.size);
                    firstPacket = 0;
                }
                else
                {
                    if ((status = NT_NetRxGet(hNetRx, &hNetBuf, 1000)) != NT_SUCCESS)
                    {
                        if ((status == NT_STATUS_TIMEOUT) || (status == NT_STATUS_TRYAGAIN))
                        {
                            // Timeouts are ok, we just need to wait a little longer for a segment
                            return;
                        }
                        // Get the status code as text
                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                        return;
                    }
                    // We got a segment. Check if the timestamp is newer than when the NTPL assign command was applied
                    // If PCAP header configured, we need to convert the timestamps received
                    if (NT_NET_GET_SEGMENT_TIMESTAMP_TYPE(hNetBuf) == NT_TIMESTAMP_TYPE_PCAP)
                    {
//                        if ((((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->sec * 1000000
//                            + ((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->usec) >
//                            (pTs->sec * 1000000 + pTs->usec))
//                        {
//                            return; // Break out, we have received a segment that is received after the NTPL assign command was applied
//                        }
                    }
                    else
                    {
//                        if (NT_NET_GET_SEGMENT_TIMESTAMP_TYPE(hNetBuf) == NT_TIMESTAMP_TYPE_PCAP_NANOTIME)
//                        {
//                            if ((((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->sec * 1000000000 + ((struct ntpcap_ts_s *)NT_NET_GET_SEGMENT_PTR(hNetBuf))->usec) >
//                                (pTs->sec * 1000000000 + pTs->usec))
//                            {
//                                return; // Break out, we have received a segment that is received after the NTPL assign command was applied
//                            }
//                        }


                        //uint64_t spaceLeftInSegment = _nt_net_get_next_packet(hNetBufTx,NT_NET_GET_SEGMENT_LENGTH(hNetBufTx), &pktNetBuf);

                        recorder->Write((char*)NT_NET_GET_SEGMENT_PTR(hNetBuf), NT_NET_GET_SEGMENT_LENGTH(hNetBuf));
                        // Increment the number of segments processed.
                        numSegments++;
                        // Increment the bytes received
                        numBytes += NT_NET_GET_SEGMENT_LENGTH(hNetBuf);
                    }

                    recorder->timestamp = _NT_NET_GET_PKT_TIMESTAMP(hNetBuf);

                    // Release the current segment
                    if ((status = NT_NetRxRelease(hNetRx, hNetBuf)) != NT_SUCCESS)
                    {
                        // Get the status code as text
                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                        return;
                    }


                }
            }
            //else if (m_runOption == WRITE_TO_DEVICE && recorder->NUM_BUFFERS == 64)
            else if (m_runOption == WRITE_TO_DEVICE)
            {
                Replay();
            }
            if (numBytes > 0)
            {
                ErrorInfo = recorder->error;
                m_SpeedMonitor.UpdateData(numBytes);
    //			m_SpeedMonitor.EndSample();
    //			m_SpeedMonitor.BeginSample();
            }
            unsigned long long dur = nt_clock() - second_keep;
            if (dur > 500)
            {
                m_SpeedMonitor.EndSample();
                m_SpeedMonitor.BeginSample();
                second_keep += dur;
            }
        }
        else if(m_Mode == "Y")
        {

            if (!m_bActive) return;
            //if (!ErrorInfo.empty() || status != NT_SUCCESS) return;

            //if (status != NT_SUCCESS) return;


            //old
    //        int numSegments = 0;              // The number of segments received
    //        int numBytes = 0;                 // The number of bytes received

            if (m_runOption == READ_FROM_DEVICE)
            {
                if (firstPacket == 1)
                {
                    {
                    std::lock_guard<std::mutex> guard(myMutex);
                    // Read the file header.
                    readCmd.cmd = NT_NETRX_READ_CMD_GET_FILE_HEADER;
                    if ((status = NT_NetRxRead(hNetRx, &readCmd)) != NT_SUCCESS)
                    {
                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                        ErrorInfo = "NTNetworkDevice::HandlePacket() NT_NetRxRead() failed : " + std::string(errorBuffer);
                        return;
                    }
                    recorder->FileHeader.resize(readCmd.u.fileheader.size);

                    memcpy(&recorder->FileHeader[0], readCmd.u.fileheader.data, readCmd.u.fileheader.size);
                    firstPacket = 0;
                    }
                }
                else
                {

                    {//lock
                    std::lock_guard<std::mutex> guard(myMutex);

                    //if((recorder->m_NT_Running==false) && m_NT_run)
                    if(recorder->m_NT_Running == false)
                    {
                        //m_NT_run = false;
                        RecordEnd();
                    }

                    if(m_NT_run)
                    {
                        if(multiThreadSeq % 4 == 1 && isbusy_th1 == true)
                            multiThreadSeq++;
                        if(multiThreadSeq % 4 == 2 && isbusy_th2 == true)
                            multiThreadSeq++;
                        if(multiThreadSeq % 4 == 3 && isbusy_th3 == true)
                            multiThreadSeq++;
                        if(multiThreadSeq % 4 == 0 && isbusy_th4 == true)
                            multiThreadSeq++;
                    }
                    }

                    if(seq % 4 == 1 && isbusy_th1 == false)
                    {
                        if ( multiThreadSeq % 4 != 1)
                            return;

                        if(m_NTRxGet_run)
                        {
                            {//lock
                            std::lock_guard<std::mutex> guard(myMutex);
                            if ((status = NT_NetRxGet(hNetRx, &hNetBuf, 1000)) != NT_SUCCESS)
                            {
                                if ((status == NT_STATUS_TIMEOUT) || (status == NT_STATUS_TRYAGAIN) || hNetBuf == NULL)
                                {
                                    // Timeouts are ok, we just need to wait a little longer for a segment
                                    return;
                                }
                                // Get the status code as text
                                NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                                return;
                            }
                            }
                        }
                        else
                            return;

                    }
                    else if (seq % 4 == 2 && isbusy_th2 == false)
                    {
                        if ( multiThreadSeq % 4 != 2)
                           return;

                        if(m_NTRxGet_run)
                        {
                            {//lock
                            std::lock_guard<std::mutex> guard(myMutex);
                            if ((status2 = NT_NetRxGet(hNetRx, &hNetBuf2, 1000)) != NT_SUCCESS)
                            {
                                if ((status2 == NT_STATUS_TIMEOUT) || (status2 == NT_STATUS_TRYAGAIN) || hNetBuf2 == NULL)
                                {
                                    // Timeouts are ok, we just need to wait a little longer for a segment
                                    return;
                                }
                                // Get the status code as text
                                NT_ExplainError(status2, errorBuffer, sizeof(errorBuffer));
                                return;
                            }
                            }
                        }
                        else
                            return;
                    }
                    else if (seq % 4 == 3 && isbusy_th3 == false)
                    {
                         if ( multiThreadSeq % 4 != 3)
                            return;

                        if(m_NTRxGet_run)
                        {
                            {//lock
                            std::lock_guard<std::mutex> guard(myMutex);
                            if ((status3 = NT_NetRxGet(hNetRx, &hNetBuf3, 1000)) != NT_SUCCESS)
                            {
                                if ((status3 == NT_STATUS_TIMEOUT) || (status3 == NT_STATUS_TRYAGAIN) || hNetBuf3 == NULL)
                                {
                                    // Timeouts are ok, we just need to wait a little longer for a segment
                                    return;
                                }
                                // Get the status code as text
                                NT_ExplainError(status3, errorBuffer, sizeof(errorBuffer));
                                return;
                            }
                            }
                        }
                        else
                            return;

                    }
                    else if (seq % 4 == 0 && isbusy_th4 == false)
                    {
                        if ( multiThreadSeq % 4 != 0)
                            return;

                        if(m_NTRxGet_run)
                        {
                            {//lock
                            std::lock_guard<std::mutex> guard(myMutex);
                            if ((status4 = NT_NetRxGet(hNetRx, &hNetBuf4, 1000)) != NT_SUCCESS)
                            {
                                if ((status4 == NT_STATUS_TIMEOUT) || (status4 == NT_STATUS_TRYAGAIN) || hNetBuf4 == NULL)
                                {
                                    // Timeouts are ok, we just need to wait a little longer for a segment
                                    return;
                                }
                                // Get the status code as text
                                NT_ExplainError(status4, errorBuffer, sizeof(errorBuffer));
                                return;
                            }
                            }
                        }
                        else
                            return;
                    }


                    else
                    {
                        return;
                    }

                    //
                    // NT_NetRxRelease - >  recorder->Write()
                    //
                    if(seq % 4 == 1)
                    {

                        char *temp;
                        {//lock
                        std::lock_guard<std::mutex> guard(myMutex);
                        threadSize1 = NT_NET_GET_SEGMENT_LENGTH(hNetBuf);
                        temp =(char*) NT_NET_GET_SEGMENT_PTR(hNetBuf);

                        if(m_NT_run)
                        {
                            if(idx_up)
                            {
                                n_current++;
                                idx_up = false;
                            }

                            if(n_current > 0)
                                newidx1 = n_current % recorder->NUM_BUFFERS;

                            leftsize1 = multiThread_leftsize;
                            multiThread_leftsize = multiThread_leftsize + threadSize1;

                            isbusy_th1 = true;

                            if(multiThread_leftsize > recorder->SINGLE_BUFFER)
                            {
                                //next file
                                if (n_current % recorder->MAX_NUM_BUFFER_FILE == (recorder->MAX_NUM_BUFFER_FILE - 1))
                                {
                                    multiThread_leftsize = recorder->FileHeader.size();
                                    idx_up = true;
                                }
                                else
                                {
                                    multiThread_leftsize = multiThread_leftsize - recorder->SINGLE_BUFFER;
                                    idx_up = true;
                                }
                            }
                            multiThreadSeq++;
                        }
                        else
                        {
                            isbusy_th1 = true;
                            multiThreadSeq++;
                        }
                        }

                        if(m_NT_run)
                        {
                            recorder->Write1(temp, threadSize1, leftsize1, seq , newidx1);
                        }
                        m_SpeedMonitor.UpdateData(NT_NET_GET_SEGMENT_LENGTH(hNetBuf));

                        {//lock
                        std::lock_guard<std::mutex> guard(myMutex);
                        if ((status = NT_NetRxRelease(hNetRx, hNetBuf)) != NT_SUCCESS)
                        {
                            NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                            return;
                        }
                        }

                        isbusy_th1 = false;

                    }
                    else if (seq % 4 == 2 )
                    {
                        char* temp;
                        {
                        std::lock_guard<std::mutex> guard(myMutex);
                        threadSize2 = NT_NET_GET_SEGMENT_LENGTH(hNetBuf2);
                        temp =(char*) NT_NET_GET_SEGMENT_PTR(hNetBuf2);

                        if(m_NT_run)
                        {
                            if(idx_up)
                            {
                                n_current++;
                                idx_up = false;
                            }
                            if(n_current > 0)
                                newidx2 = n_current % recorder->NUM_BUFFERS;

                            leftsize2 = multiThread_leftsize;
                            multiThread_leftsize = multiThread_leftsize + threadSize2;

                            isbusy_th2 = true;

                            if(multiThread_leftsize > recorder->SINGLE_BUFFER)
                            {
                                //next file
                                if (n_current % recorder->MAX_NUM_BUFFER_FILE == (recorder->MAX_NUM_BUFFER_FILE - 1))
                                {
                                    multiThread_leftsize = recorder->FileHeader.size();
                                    idx_up = true;
                                }
                                else
                                {
                                    multiThread_leftsize = multiThread_leftsize - recorder->SINGLE_BUFFER;
                                    idx_up = true;
                                }
                            }
                            multiThreadSeq++;
                        }
                        else
                        {
                            isbusy_th2 = true;
                            multiThreadSeq++;
                        }
                        }

                        if(m_NT_run)
                        {
                            recorder->Write2(temp, threadSize2, leftsize2, seq, newidx2);
                        }

                        {//lock
                        std::lock_guard<std::mutex> guard(myMutex);
                        if ((status2 = NT_NetRxRelease(hNetRx, hNetBuf2)) != NT_SUCCESS)
                        {
                            NT_ExplainError(status2, errorBuffer, sizeof(errorBuffer));
                            return;
                        }
                        }
                        m_SpeedMonitor.UpdateData(NT_NET_GET_SEGMENT_LENGTH(hNetBuf2));
                        isbusy_th2 = false;

                    }
                    else if (seq % 4 == 3 )
                    {
                        char* temp;
                        {
                        std::lock_guard<std::mutex> guard(myMutex);
                        threadSize3 = NT_NET_GET_SEGMENT_LENGTH(hNetBuf3);
                        temp =(char*)NT_NET_GET_SEGMENT_PTR(hNetBuf3);

                        if(m_NT_run)
                        {
                            if(idx_up)
                            {
                                n_current++;
                                idx_up = false;
                            }

                            if(n_current > 0)
                                newidx3 = n_current % recorder->NUM_BUFFERS;

                            leftsize3 = multiThread_leftsize;
                            multiThread_leftsize = multiThread_leftsize + threadSize3;

                            isbusy_th3 = true;

                            if(multiThread_leftsize > recorder->SINGLE_BUFFER)
                            {
                                //next file
                                if (n_current % recorder->MAX_NUM_BUFFER_FILE == (recorder->MAX_NUM_BUFFER_FILE - 1))
                                {
                                    multiThread_leftsize = recorder->FileHeader.size();
                                    idx_up = true;
                                }
                                else
                                {
                                    multiThread_leftsize = multiThread_leftsize - recorder->SINGLE_BUFFER;
                                    idx_up = true;
                                }
                            }
                            multiThreadSeq++;
                        }
                        else
                        {
                            isbusy_th3 = true;
                            multiThreadSeq++;
                        }
                        }

                        if(m_NT_run)
                        {
                            recorder->Write3(temp, threadSize3, leftsize3,seq, newidx3);
                        }

                        {//lock
                        std::lock_guard<std::mutex> guard(myMutex);
                        if ((status3 = NT_NetRxRelease(hNetRx, hNetBuf3)) != NT_SUCCESS)
                        {
                            NT_ExplainError(status3, errorBuffer, sizeof(errorBuffer));
                            return;
                        }
                        }
                        m_SpeedMonitor.UpdateData(NT_NET_GET_SEGMENT_LENGTH(hNetBuf3));
                        isbusy_th3 = false;

                    }
                    else if (seq % 4 == 0 )
                    {
                        char* temp;
                        {
                        std::lock_guard<std::mutex> guard(myMutex);
                        threadSize4 = NT_NET_GET_SEGMENT_LENGTH(hNetBuf4);
                        temp = (char*)NT_NET_GET_SEGMENT_PTR(hNetBuf4);

                        if(m_NT_run)
                        {
                            if(idx_up)
                            {
                                n_current++;
                                idx_up = false;
                            }

                            if(n_current > 0)
                                newidx4 = n_current % recorder->NUM_BUFFERS;

                            leftsize4 = multiThread_leftsize;
                            multiThread_leftsize = multiThread_leftsize + threadSize4;

                            isbusy_th4 = true;

                            if(multiThread_leftsize > recorder->SINGLE_BUFFER)
                            {
                                //next file
                                if (n_current % recorder->MAX_NUM_BUFFER_FILE == (recorder->MAX_NUM_BUFFER_FILE - 1))
                                {
                                    multiThread_leftsize = recorder->FileHeader.size();
                                    idx_up = true;
                                }
                                else
                                {
                                    multiThread_leftsize = multiThread_leftsize - recorder->SINGLE_BUFFER;
                                    idx_up = true;
                                }
                            }
                            multiThreadSeq++;

                        }
                        else
                        {
                            isbusy_th4 = true;
                            multiThreadSeq++;
                        }
                        }

                        if(m_NT_run)
                        {
                            recorder->Write4(temp, threadSize4, leftsize4, seq, newidx4);
                        }

                        {//lock
                        std::lock_guard<std::mutex> guard(myMutex);
                        if ((status4 = NT_NetRxRelease(hNetRx, hNetBuf4)) != NT_SUCCESS)
                        {
                            NT_ExplainError(status4, errorBuffer, sizeof(errorBuffer));
                            return;
                        }
                        }
                        m_SpeedMonitor.UpdateData(NT_NET_GET_SEGMENT_LENGTH(hNetBuf4));
                        isbusy_th4 = false;

                    }
                }
            }


            //else if (m_runOption == WRITE_TO_DEVICE && recorder->NUM_BUFFERS == 64)
            else if (m_runOption == WRITE_TO_DEVICE)
            {
                Replay();
            }

            if (seq % 4 == 1)
            {
                unsigned long long dur = nt_clock() - second_keep;
                if (dur > 1000)
                {

                    m_SpeedMonitor.EndSample();
                    m_SpeedMonitor.BeginSample();
                    second_keep += dur;

                    //ADD because overflowing
                    ErrorInfo = recorder->error;
                }
            }

        }
	}

	void NTNetworkDevice::StopThread()
	{
		if (m_Thread)
		{
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			m_Thread.release();
		}
        if (m_Thread2)
           {
               try
               {
                   m_Thread2->join();
               }
               catch (...)
               {
               }
               m_Thread2.release();
           }
           if (m_Thread3)
           {
               try
               {
                   m_Thread3->join();
               }
               catch (...)
               {
               }
               m_Thread3.release();
           }
           if (m_Thread4)
           {
               try
               {
                   m_Thread4->join();
               }
               catch (...)
               {
               }
               m_Thread4.release();
           }
	}

    void NTNetworkDevice::RecordBegin(int ch)
	{
        if (!m_bActive) return;

        if (recorder->EndRunning || status != NT_SUCCESS) return;

		ErrorInfo = "";
		check = true;
        m_SpeedMonitor.Reset();
        recorder->LastMinuteData = 0;
        recorder->error = "";

        //NET-49
        recorder->m_deleteFileIndex = 1;

		if (m_runOption == WRITE_TO_DEVICE)
		{
			if (hNetTx != 0)
			{
				// Close the TX stream
				NT_NetTxClose(hNetTx);
				hNetTx = 0;
			}
			firstPacket = 1;
			if (ReplayFlag == SECOND)
			{
				recorder->SetReplaySessionByTime(recorder->RelayOffset, recorder->RelayDuration);
			}
			else
			{
				recorder->SetReplaySessionByTag(recorder->RelayOffset, recorder->RelayDuration);
			}
			bReplay = true;
		}

		recorder->filename = FileName;
		if (m_runOption != WRITE_TO_DEVICE)
		{
            recorder->Open(false, ch);
		}
		else
		{
			// Open the infostream.
			if ((status = NT_InfoOpen(&hInfo, "replay")) != NT_SUCCESS)
			{
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				return;
			}

            // Check whether or not absolut TX timing is supported
            infoRead.cmd = NT_INFO_CMD_READ_ADAPTER_V6;
            infoRead.u.adapter_v6.adapterNo = 0;  // Adapter is hardcoded to adapter 0 as the TX port is hardcoded to port 0

			if ((status = NT_InfoRead(hInfo, &infoRead)) != NT_SUCCESS)
			{
				NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
				NT_InfoClose(hInfo);
				return;
			}
			// Get the TX mode
            txTiming = infoRead.u.adapter_v6.data.txTiming;

            //NET-48
            const struct NtInfoCmdAdapter_s *pi = &infoRead.u.adapter;
            char cbuffer[32];
            for(int i = 0; i < sizeof(cbuffer); i++)
            {
                cbuffer[i] = pi->data.name[i+8];
            }
            std::string adapterName(cbuffer);
            QString q_adapterName;
            q_adapterName = QString::fromStdString(adapterName);
            if(q_adapterName.contains("NT20E"))
            {
                m_replayType = "3GA"; //or "4GA"
            }

            if(m_replayType == "4GA")
            {
                std::string fname = recorder->filename + "/" + "1.ntcap";

                // Open the capture file to replay (captured with the capture example)
                //if((status = NT_NetFileOpen(&hNetFile, "FileStream", NT_NET_INTERFACE_SEGMENT, "capfile.ntcap")) != NT_SUCCESS) {
                if((status = NT_NetFileOpen(&hNetFile, "FileStream", NT_NET_INTERFACE_SEGMENT, fname.c_str())) != NT_SUCCESS)
                {
                  // Get the status code as text
                  NT_ExplainError(status, errorBuffer, sizeof(errorBuffer)-1);
                  fprintf(stderr, "NT_NetFileOpen() failed: %s\n", errorBuffer);
                  //return -1;
                }

                // Read one segment from the file to get the first packet timestamp
                if ((status = NT_NetFileGet(hNetFile, &hNetBufFile)) != NT_SUCCESS) {
                  if (status == NT_STATUS_END_OF_FILE) {
                    fprintf(stderr, "The file %s has no data\n", "capfile.ntpcap");
                    //return -1;
                  }
                }
                _nt_net_build_pkt_netbuf(hNetBufFile, &pktNetBuf);
                firstPacketTS = NT_NET_GET_PKT_TIMESTAMP(&pktNetBuf) * 10; // Convert 10ns ticks to 1ns

                // Close the file again. We will open it again later to actually transmit packets.
                NT_NetFileClose(hNetFile);

                // Open the config stream
                if ((status = NT_ConfigOpen(&hConfig, "replay")) != NT_SUCCESS) {
                  NT_ExplainError(status, errorBuffer, sizeof(errorBuffer)-1);
                  fprintf(stderr, "NT_ConfigOpen() failed: %s\n", errorBuffer);
                  NT_ConfigClose(hConfig);
                  //return 0;
                }

                // Read the adapter time
                configRead.parm = NT_CONFIG_PARM_ADAPTER_TIMESTAMP;
                configRead.u.timestampRead.adapter = 0;
                if ((status = NT_ConfigRead(hConfig, &configRead)) != NT_SUCCESS) {
                  NT_ExplainError(status, errorBuffer, sizeof(errorBuffer)-1);
                  fprintf(stderr, "NT_ConfigRead() failed: %s\n", errorBuffer);
                  NT_ConfigClose(hConfig);
                  //return 0;
                }
                adapterTS = configRead.u.timestampRead.data.nativeUnixTs * 10; // Convert 10ns ticks to 1ns

                // Calculate time delta - we add 1 second to give ourselves some headroom
                timeDelta = (adapterTS - firstPacketTS) + 1000000000;

                // Configure transmit on timestamp
                configWrite.parm = NT_CONFIG_PARM_PORT_TRANSMIT_ON_TIMESTAMP;
                //configWrite.u.transmitOnTimestamp.portNo = (uint8_t)0;
                configWrite.u.transmitOnTimestamp.portNo = ch;
                configWrite.u.transmitOnTimestamp.data.timeDelta = timeDelta;
                configWrite.u.transmitOnTimestamp.data.enable = true;
                configWrite.u.transmitOnTimestamp.data.forceTxOnTs = true;
                if ((status = NT_ConfigWrite(hConfig, &configWrite)) != NT_SUCCESS) {
                  NT_ExplainError(status, errorBuffer, sizeof(errorBuffer)-1);
                  fprintf(stderr, "NT_ConfigWrite() failed: %s\n", errorBuffer);
                  NT_ConfigClose(hConfig);
                  //return 0;
                }
            }
            //NET-48

			NT_InfoClose(hInfo);
		}

        multiThread_leftsize = recorder->FileHeader.size();
        m_NT_run = true;

	}

	void NTNetworkDevice::RecordEnd()
	{
        if (!recorder->Running) return;

        //NET-59

        m_NTRxGet_run = false;
        Sleep(1);

        m_NT_run = false;
        Sleep(1);

        m_NTRxGet_run = true;

        multiThread_leftsize = 0;

        isbusy_th1 = false;
        isbusy_th2 = false;
        isbusy_th3 = false;
        isbusy_th4 = false;
        n_current = 0;

        newidx1 = 0;
        newidx2 = 0;
        newidx3 = 0;
        newidx4 = 0;

        leftsize1 = 0;
        leftsize2 = 0;
        leftsize3 = 0;
        leftsize4 = 0;

		recorder->Close();
        usleep(10);

		if (m_runOption == WRITE_TO_DEVICE)
		{
            usleep(10);
			if (hNetTx != 0)
			{
				// Close the TX stream
				NT_NetTxClose(hNetTx);
				hNetTx = 0;
			}
			bReplay = false;
		}
		if (recorder->error != "")
		{
			ErrorInfo = recorder->error;
		}

//        multiThread_leftsize = 0;

//        isbusy_th1 = false;
//        isbusy_th2 = false;
//        isbusy_th3 = false;
//        isbusy_th4 = false;
//        n_current = 0;

//        newidx1 = 0;
//        newidx2 = 0;
//        newidx3 = 0;
//        newidx4 = 0;

//        leftsize1 = 0;
//        leftsize2 = 0;
//        leftsize3 = 0;
//        leftsize4 = 0;


	}

	void NTNetworkDevice::Replay()
	{
#ifdef NPTINCLUDE
        usleep(10);
		if (!bReplay) return;
		bReplay = false;
        recorder->Open(true, 0);
		if (hNetTx != 0)
		{
			// Close the TX stream
			NT_NetTxClose(hNetTx);
			hNetTx = 0;
        }

		std::string buffer;
		// Open a TX hostbuffer on NUMA node 0 that can transmit to port 0
		int port = GetID() - 1;
        if ((status = NT_NetTxOpen(&hNetTx, "TxStreamPort", 1 << port, NTNetworkDeviceManager::GetInstance()->numaIdx, 0)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		m_SpeedMonitor.Reset();
		NtNetBuf_t hNetBufTxE = 0;
        //unsigned long long nseg = 0;

        //kjk
        int numBytes=0;

		while (recorder->ReadNext(buffer))
		{
			if (buffer.size() == 0) continue;
            {
                // Get a TX buffer for this segment
                if ((status = NT_NetTxGet(hNetTx, &hNetBufTx, port, buffer.size(), NT_NETTX_SEGMENT_OPTION_RAW, -1 /* wait forever */)) != NT_SUCCESS)
                {
                    // Get the status code as text
                    NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                    break;
                }

                // Copy the segment into the TX buffer
                memcpy(NT_NET_GET_SEGMENT_PTR(hNetBufTx), &buffer[0], buffer.size());

                //NET-48
                if(m_replayType == "4GA")
                {
                    _nt_net_build_pkt_netbuf(hNetBufTx, &pktNetBuf);

//                    // Get a packet buffer pointer from the segment.
//                    _nt_net_build_pkt_netbuf(hNetBufFile, &pktNetBuf);

                    // Store the timestamp
                    uint64_t ts = NT_NET_GET_PKT_TIMESTAMP((&pktNetBuf));
                    lastPacketTS = ts * 10;

                }
                else //3GA
                {
                    // Build a packet netbuf structure
                    if (firstPacket == 1)
                    {
                        _nt_net_build_pkt_netbuf(hNetBufTx, &pktNetBuf);

                        if (txTiming == NT_TX_TIMING_ABSOLUTE)
                        {
                            // Absolute TX mode is supported.
                            // If transmit tx relative is disabled i.e. we are using absolut transmit and
                            // the txclock must be synched to the timestamp in the first packet.
                            NT_NET_SET_PKT_TXNOW((&pktNetBuf), 0);                    // Wait for tx delay before the packet is sent
                            NT_NET_SET_PKT_TXSETCLOCK((&pktNetBuf), 1);               // Synchronize tx nt_clock to timestamp in first packet.
                        }
                        else
                        {
                            // Legacy mode/Relative tx timing.
                            // First packet must be sent with txnow=1
                            NT_NET_SET_PKT_TXNOW((&pktNetBuf), 1);                    // Send the first packet now
                        }
                        firstPacket = 0;
                    }
                }//NET-48

                // Release the TX buffer and the packets within the segment will be transmitted
                if ((status = NT_NetTxRelease(hNetTx, hNetBufTx)) != NT_SUCCESS)
                {
                    // Get the status code as text
                    NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
                    break;
                }

//old source using two buffer by sunwha
//                if (hNetBufTxE == 0)
//                {
//                    hNetBufTxE = hNetBufTx;
//                }
//                else
//                {
//                    // Release the TX buffer and the packets within the segment will be transmitted
//                    if ((status = NT_NetTxRelease(hNetTx, hNetBufTxE)) != NT_SUCCESS)
//                    {
//                        // Get the status code as text
//                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
//                        break;
//                    }
//                    // Release the TX buffer and the packets within the segment will be transmitted
//                    if ((status = NT_NetTxRelease(hNetTx, hNetBufTx)) != NT_SUCCESS)
//                    {
//                        // Get the status code as text
//                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
//                        break;
//                    }

//                    hNetBufTxE = 0;
//                }
//old source
			}

            m_SpeedMonitor.UpdateData(buffer.size());

            unsigned long long dur = nt_clock() - second_keep;
            if (dur > 500)
            {
                m_SpeedMonitor.EndSample();
                m_SpeedMonitor.BeginSample();
                second_keep += dur;
            }
            //m_SpeedMonitor.EndSample();
            //m_SpeedMonitor.BeginSample();
            recorder->TotalData += buffer.size();

		}

        recorder->Running = true;
        recorder->Close();

        //m_Runner->Stop();
        //recorder->m_IORunner->Stop();

		if (hNetTx != 0)
		{
            //NET-48
            if(m_replayType == "4GA")
            {
                // Wait until all packets should have been sent
                while(1)
                {
                    // Read the adapter time
                    configRead.parm = NT_CONFIG_PARM_ADAPTER_TIMESTAMP;
                    configRead.u.timestampRead.adapter = 0;
                    if ((status = NT_ConfigRead(hConfig, &configRead)) != NT_SUCCESS) {
                        NT_ExplainError(status, errorBuffer, sizeof(errorBuffer)-1);
                        fprintf(stderr, "NT_ConfigRead() failed: %s\n", errorBuffer);
                        NT_ConfigClose(hConfig);
                    }

                    adapterTS = configRead.u.timestampRead.data.nativeUnixTs * 10; // Convert 10ns ticks to 1ns

                    if (adapterTS > (lastPacketTS + timeDelta))
                    {
                        // The last packet should be sending now
                        break;
                    }
                    usleep(1000);
                }
            }
            else
                Sleep(100);

			// Close the TX stream
            NT_NetTxClose(hNetTx);
            // Close the file stream
            NT_NetFileClose(hNetFile);

			hNetTx = 0;
		}
		if (recorder->error != "")
		{
			ErrorInfo = recorder->error;
#ifdef WIN32
            log->WriteLog(ErrorInfo);
#endif
		}
#endif
	}

	NTNetworkDeviceManager * NTNetworkDeviceManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new NTNetworkDeviceManager();
		}
		return instance;
	}

	void NTNetworkDeviceManager::Init()
	{
		m_bInitialized = false;
		numaIdx = 0;
#ifdef NPTINCLUDE
		NtInfoStream_t hInfoStream;     // Info stream handle
		NtInfo_t hInfo;                 // Info handle
		char errorBuffer[NT_ERRBUF_SIZE];         // Error buffer
		int status;                   // Status variable
		numAdapters = 0;
		// Initialize the NTAPI library and thereby check if NTAPI_VERSION can be used together with this library
		if ((status = NT_Init(NTAPI_VERSION)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		// Open the info stream
		if ((status = NT_InfoOpen(&hInfoStream, "Info")) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		// Read the system info
		hInfo.cmd = NT_INFO_CMD_READ_SYSTEM;
		if ((status = NT_InfoRead(hInfoStream, &hInfo)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}
		nPorts = hInfo.u.system.data.numPorts;
		numAdapters = hInfo.u.system.data.numAdapters;
		// Close the info stream
		if ((status = NT_InfoClose(hInfoStream)) != NT_SUCCESS)
		{
			// Get the status code as text
			NT_ExplainError(status, errorBuffer, sizeof(errorBuffer));
			return;
		}

        //NET-44 add Filtered stream
        auto pRecordSetting = DeviceRecordSetting::GetInstance();
        for (int i = 0; i < FILTERED_STREAM_SIZE; i++)
        {
            if ( pRecordSetting->m_UserStream[i] != "")
                m_UserStream[i] = pRecordSetting->m_UserStream[i];
        }
        //m_UserStream = "Assign[streamid=10;color=7] = port == 0,1";


#endif
		m_bInitialized = true;
	}

	bool NTNetworkDeviceManager::IsOpen()
	{
		return m_bInitialized;
	}

	void NTNetworkDeviceManager::Close()
	{
#ifdef NPTINCLUDE
		NT_Done();
#endif
	}

	void NTNetworkDeviceManager::Clear()
	{
		delete instance;
		instance = 0;
	}

	NTNetworkDeviceManager * NTNetworkDeviceManager::instance = 0;
}
