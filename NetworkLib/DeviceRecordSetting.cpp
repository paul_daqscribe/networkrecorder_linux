#include "DeviceRecordSetting.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>
#include "StorageManager.h"
#include <stdio.h>
#include <QProcess>

static vector<std::string> v_SSDmodel;
static vector<int> v_SSDWBT;


namespace NetworkLib
{
	std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters)
	{
		size_t current;
		size_t next = -1;
		std::vector<std::string> columns;
		int nColumns = 0;
		do
		{
			current = next + 1;
			next = line.find_first_of(delimiters, current);
			std::string field = line.substr(current, next - current);
			columns.push_back(field);
			++nColumns;
		} while (next != std::string::npos);
		return columns;
	}

    DeviceRecordSetting* DeviceRecordSetting::m_Instance = 0;

	DeviceRecordSetting* DeviceRecordSetting::GetInstance()
	{
		if (m_Instance == 0)
		{
			m_Instance = new DeviceRecordSetting();
		}
		return m_Instance;
	}

	void DeviceRecordSetting::Clear()
	{
		delete m_Instance;
		m_Instance = 0;
	}

    void DeviceRecordSetting::SetSSDEndurace(std::string value)
    {
        std::vector<std::string> strs;
        boost::split(strs, value, boost::is_any_of(","));

        if (strs.size() > 1)
        {
            SSD m_SSD;
            m_SSD.m_value = value;
            m_SSD.m_SSDmodel = strs[0];
            m_SSD.m_SSDWBT = std::stod(strs[1]);
            v_SSD.push_back(m_SSD);
        }
    }

     void DeviceRecordSetting::SetGroup(int NUMAnode, string SetPort, string CPUAffinity, string Path)
     {
         GROUP m_group;

         m_group.m_NUMAnode = NUMAnode;
         m_group.m_SetPort = SetPort;
         m_group.m_CPUAffinity = CPUAffinity;
         m_group.m_Path = Path;

         std::vector<std::string> strs;
         boost::split(strs, SetPort, boost::is_any_of(","));
         for(int i = 0; i < strs.size(); i ++)
         {
             m_group.v_Port.push_back(atoi(strs[i].c_str()));
         }
         m_group.m_NumPort = strs.size();
         v_Group.push_back(m_group);

         //NET-44 add Filtered stream
         //bug fix at "Recorder path~"
         QString temp = QString::fromStdString(SetPort);
         temp = temp.replace(",", " ");
         SetPort = temp.toStdString();
         if(Path == "path1")
            m_pathCH[0].append(" " + SetPort);
         else if(Path == "path2")
            m_pathCH[1].append(" " + SetPort);
         else if(Path == "path3")
            m_pathCH[2].append(" " + SetPort);
         else if(Path == "path4")
            m_pathCH[3].append(" " + SetPort);

     }

     void DeviceRecordSetting::SetGroupCPUAffinity()
     {
         //split
         for(int y = 0; y < v_Group.size(); y++)   // GROUP
         {
             std::vector<std::string> strs;
             boost::split(strs, v_Group[y].m_CPUAffinity, boost::is_any_of(","));

             int result = 1;
             int temp = 0;
             int temp2 = 0;
             int temp3 = 0;
             int more = 0;

             for(int i = 0; i < strs.size(); i ++)
             {
                 more = strs[i].find('-');

                 // 1more cpu
                 if(more > 0)
                 {
                     std::vector<std::string> sub_strs;
                     boost::split(sub_strs, strs[i], boost::is_any_of("-"));

                     //string to int
                     temp2 = atoi(sub_strs[0].c_str());
                     temp3 = atoi(sub_strs[1].c_str());

                     for(int i = 1 ; i <= temp3 - temp2; i++)
                     {
                         result = 1 + (result << 1);
                     }

                     //convert cpu bit mask
                     for (int y=0; y< temp2-1; y++)
                     {
                         //1110 000
                         result = result << 1;
                     }

                     //set value
                     for(int z = 0; z < v_Group[y].v_Port.size(); z++)  // PORT
                     {
                         if (i == 0)
                            v_CPUAffinityRead[v_Group[y].v_Port[z] - 1] = result;
                         else if (i == 1)
                            v_CPUAffinityWrite[v_Group[y].v_Port[z] - 1]= result;
                     }

                     result = 1;

                 }

                 // just 1 cpu
                 else
                 {
                     //string to int
                     temp = atoi(strs[i].c_str());

                     //convert cpu bit mask
                     for (int y=0; y< temp-1; y++)
                     {
                         result = result << 1;
                     }

                     //set value
                     for(int z = 0; z < v_Group[y].v_Port.size(); z++)  // PORT
                     {
                         if (i == 0)
                            v_CPUAffinityRead[v_Group[y].v_Port[z] - 1] = result;
                         else if (i == 1)
                            v_CPUAffinityWrite[v_Group[y].v_Port[z] - 1] = result;
                     }

                     result = 1;
                 }
             }

         }
     }


    void DeviceRecordSetting::setProgramCPUAffinity(std::string value)
    {
        //split

        std::string strs;
        strs = value;
        int result = 1;
        int temp = 0;
        int temp2 = 0;
        int temp3 = 0;
        int more = 0;

        for(int i = 0; i < 1; i ++)
        {
            more = strs.find('-');

            // 1more cpu
            if(more > 0)
            {
                std::vector<std::string> sub_strs;
                boost::split(sub_strs, strs, boost::is_any_of("-"));

                //string to int
                temp2 = atoi(sub_strs[0].c_str());
                temp3 = atoi(sub_strs[1].c_str());

//                result = result + pow(2, temp3 - temp2 - 1);
                for(int i = 1 ; i <= temp3 - temp2; i++)
                {
                    result = 1 + (result << 1);
                }

                //convert cpu bit mask
                for (int y=0; y< temp2-1; y++)
                {
                    //1110 000
                    result = result << 1;
                }

                ThreadPriorityProgram = result;

            }

            // just 1 cpu
            else
            {
                //string to int
                temp = atoi(strs.c_str());

                //convert cpu bit mask
                for (int y=0; y< temp-1; y++)
                {
                    result = result << 1;
                }

                ThreadPriorityProgram = result;
            }
        }
    }

    void DeviceRecordSetting::SetThreadPriority(std::string value)
    {
        //split
        std::vector<std::string> strs;
        boost::split(strs, value, boost::is_any_of(","));
        int temp = 0;

        for(int i = 0; i < strs.size(); i ++)
        {
            //string to int
            temp = atoi(strs[i].c_str());

            //set value
            if (i == 0)
                ThreadPriorityMemcpy = temp;
            else if (i == 1)
                ThreadPriorityWrite = temp;
        }
    }

    void DeviceRecordSetting::SetThreadSleep(std::string value)
    {
        //split
        std::vector<std::string> strs;
        boost::split(strs, value, boost::is_any_of(","));
        int temp = 0;

        for(int i = 0; i < strs.size(); i ++)
        {
            //string to int
            temp = atoi(strs[i].c_str());

            //set value
            if (i == 0)
                ThreadSleepMemcpy = temp;
            else if (i == 1)
                ThreadSleepWrite = temp;
        }
    }

    std::string DeviceRecordSetting::GetNetworkRecordFileName(size_t id, int record, int num_path, bool bsession)
    {
        std::stringstream namebuilder;
        namebuilder << m_RecordPath[num_path] << "/" << m_RecordName;
        StorageManager::CreatePath(namebuilder.str());
        namebuilder << "/" << m_RecordName;
        if (!bsession) namebuilder << "_" << id << "_";
        namebuilder.width(3); namebuilder.fill('0');
        namebuilder << record;
        return namebuilder.str();
    }


	DeviceRecordSetting::DeviceRecordSetting()
        : WRITE_NUM_SINGLE_BUFFERS(48)
        , WRITE_NUM_BUFFERS(32)
        , WRITE_BUFFER_SIZE(1024*1024)
        , READ_NUM_SINGLE_BUFFERS(60)
        , READ_NUM_BUFFERS(64)
        , READ_BUFFER_SIZE(1024*1024*4)
        , PRE_LOAD_RATE(0.25)
        , m_RecordName("Record")
        , m_RecordMaxSize(1000000000)
//        , ONONBLOCK(0)
//        , ODSYNC(0)
//        , OSYNC(0)
//        , ORSYNC(0)
//        , ODIRECT(0)
//        , OASYNC(0)
//        , ONDELAY(0)
//        , ONOATIME(0)
        , m_DebugMode(0)
        , AIOSleep(0)
	{
	}




	DeviceRecordSetting::~DeviceRecordSetting()
	{
	}

	unsigned long long DeviceRecordSetting::GetRecordMaximumSize()
	{
		return m_RecordMaxSize;
	}

	void DeviceRecordSetting::SetRecordMaximumSize(unsigned long long recordsize)
	{
		m_RecordMaxSize = recordsize;
	}

}
