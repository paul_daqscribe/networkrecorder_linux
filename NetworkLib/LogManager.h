#ifndef LOGMANAGER_H
#define LOGMANAGER_H
#include "string"
#include "iostream"
#include "fstream"
#include <ctime>
#ifdef WIN32
#include <windows.h>

using namespace std;

namespace NetworkLib
{
	class LogManager
	{
	private:
		std::string date;
		std::string splite = ">>>>     ";
		fstream os;
		SYSTEMTIME today;

		/// Default constructor.
		LogManager();

		/// Destructor.
		~LogManager();

	public:
		static LogManager* GetInstance();
		static LogManager* instance;
		void GetDate();
		void WriteLog(string message);
		void WriteTimeLog(string method, clock_t message);

	};
}
#endif
#endif
