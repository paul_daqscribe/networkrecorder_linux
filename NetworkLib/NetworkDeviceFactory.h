#ifndef NETWORKDEVICEFACTORY_H
#define NETWORKDEVICEFACTORY_H

#include "INetworkDeviceFactory.h"

#define FILTERED_STREAM_SIZE 4

namespace NetworkLib
{
	/// Generic implementation for INetworkDeviceFactory.
	class NetworkDeviceFactory : public INetworkDeviceFactory
	{
	public:
		/**
		 * Constructor
		 * @param networktype The flag for the network device type. Currently support SFPDP, PCAP and NT.
		 */ 
		NetworkDeviceFactory(NetworkType networktype) : m_NetworkType(networktype) {}

		/// Destructor
		~NetworkDeviceFactory() {}

		/// Implementation of the interface.
		virtual void GetNetworkDevices(std::vector<INetworkDevice*>& devices);

	private:
		/// Network device type to load.
		NetworkType m_NetworkType;
	};

}

#endif
