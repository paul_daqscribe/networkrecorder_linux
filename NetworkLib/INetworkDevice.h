#ifndef INETWORKDEVICE_H
#define INETWORKDEVICE_H

#include <string>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include "DataSpeedMonitor.h"
#include <fstream>

#ifdef _WIN32
//#define SFPDPINCLUDE
#endif

#define PCAPINCLUDE

#define NPTINCLUDE


namespace NetworkLib
{
	enum SFPDPSpeed { SPEED_1G, SPEED_2_1G, SPEED_2_5G, SPEED_3_1G, SPEED_4_25G };
	enum RunOption { READ_FROM_DEVICE, WRITE_TO_DEVICE };
	enum NetworkType { PCAP, SFPDP, GESFPDP, NAPA_TECH };
	enum ReplayOption { SECOND, TAG };
	class INetworkDevice;

	class RecordController;
	/**
	 * IPacketMonitor defines the interface for the packet monitor which will called when the packets are captured.
	 */
	struct IPacketMonitor
	{
		/// Default destructor.
		virtual ~IPacketMonitor() {}

		/// Update must be implemented to get update of the number of packets.
		virtual void Update(int nPackets) = 0;
	};


	/**
	 * NetworkDeviceRunner provides the thread to run the network device.
	 */
	struct NetworkDeviceRunner
	{
		/**
		 * NetworkDeviceRunner is the full constructor.
		 * @param pDevice The device pointer to be run. set.
		 * @return The virtual device is set to be used.
		 */
		NetworkDeviceRunner(std::shared_ptr<INetworkDevice> pDevice) : m_pDevice(pDevice), m_bRunning(true) {}

		/**
		 * operator() is the implementation of the device running logic.
		 */
		void operator()();

		/**
		 * Reset makes to start the thread.
		 * @return The device is set to run continually.
		 */
		void Reset() { m_bRunning = true; }

		/**
		 * Stop stops the thread.
		 * @return The device is stopped.
		 */
		void Stop() { m_bRunning = false; }

		/**
		 * IsRunning is the getter for the running state.
		 * @return True if the device is running otherwise return false.
		 */
		bool IsRunning() { return m_bRunning; }

	private:

		/// The network device pointer to run.
		std::shared_ptr<INetworkDevice> m_pDevice;

		/// The flag for running state of the thread.
		bool m_bRunning;

    public:

        int seq;
	};

	/**
	 * INetworkDevice defines the interface for the network devices.
	 */
	class INetworkDevice
	{

	public:
		/// Default constructor.
		INetworkDevice();

		/// Default destructor.
		virtual ~INetworkDevice();

		/**
		 * Monitor starts the thread to monitor the network packets.
		 * Must to be implemented for specific network device interfaces.
		 */
        virtual void Monitor(bool bRead, int ch) = 0;

		/**
		 * Stop stops recording the network packets.
		 * Must to be implemented for specific network device interfaces.
		 */
		virtual void Stop() = 0;

		/**
		 * StopThread stops the thread to record the network packets.
		 * Must to be implemented for specific network device interfaces.
		 */
		virtual void StopThread() = 0;

		/**
		 * HandlePacket handles the packets from the the network device.
		 * Must to be implemented for specific network device interfaces.
		 */
        virtual void HandlePacket(int seq) = 0;

		/**
		 * RecordBegin begins the recording operation the packets from the the network device.
		 * Must to be implemented for specific network device interfaces.
		 */
        virtual void RecordBegin(int ch);

		/**
		 * RecordEnd ends the recording operation the packets from the the network device.
		 * Must to be implemented for specific network device interfaces.
		 */
		virtual void RecordEnd();

		/**
		 * AddTag ends the recording operation the packets from the the network device.
		 * @param tagidx The index of user tag defined in UserTagManager.
		 * Must to be implemented for specific network device interfaces.
		 */
		virtual void AddTag(int tagidx, bool bset);

		virtual unsigned long long AddTag(unsigned long long tagidx, bool bset, const std::string& time);

		bool IsRecording();

		std::string GetReplayFile() { return FileName; }

        //std::string GetRecordFile() { return FileName; }

		void SetReplayFile(const std::string& filename);

        void SetRecordFile(const std::string& filename);

        //NET-44 add Filtered stream
        //Fixed channel name to be changeable
        void SetName(std::string m_name) { Name = m_name; }

		std::string GetRecordFile();

		/// The network type.
		NetworkType networkType;

		/// The name of network interface card.
		std::string Name;

		/// The description of network interface.
		std::string Description;

		/// The IP address.
		std::string Address;


		/// The filter to used for the network device capture.
		std::string PCAPFilter;

		/// The error information to error report.
		std::string ErrorInfo;

		/// The status information to report.
		std::string Status;

		/// The getter to get the flag to monitor the device.
		bool GetActiveFlag() { return m_bActive; }

		/// The setter to set the flag to monitor the device.
		void SetActiveFlag(bool flag) { m_bActive = flag; }

		/// The getter to get the flag to save the device.
		virtual RunOption GetRunOption() { return m_runOption; }

		/// The setter to set the flag to save the device.
		virtual void SetRunOption(RunOption flag);

		/**
		 * AddPacketMonitor adds the packet monitor to be notified when the packet is updated.
		 * @param pMonitor The pointer of IPacketMonitor to be added.
		 */
		void AddPacketMonitor(const std::shared_ptr<IPacketMonitor>& pMonitor);

		/// The getter to get the number of saved packets.
		long long GetSavedPackets() { return m_nSavedPackets; }

		/// The getter to get the status of running.
        bool IsRunning() { return m_Thread != 0; }

		/// The getter to get the data average speed.
        double GetCurrentSpeed() { return m_SpeedMonitor.GetCurrentSpeed(); }
        //double GetCurrentSpeed() { return m_SpeedMonitor.GetDataSampleSpeed(); }

		/// The getter to get the data peak speed.
		double GetCurrentSampleSpeed() { return m_SpeedMonitor.GetDataSampleSpeed(); }

		/// The getter to get the total saved data size.
		long long GetTotalDataSize();

		/// The accessor for monitoring duration.
		double GetDuration() { return m_SpeedMonitor.GetDuration(); }

		/// The accessor for monitoring duration.
		double GetDataDuration() { return m_SpeedMonitor.GetDataDuration(); }

		unsigned long long GetRelayRecordDuration();

		void SetRelayRecordDuration(unsigned long long ns);

		/// The accessor for the network buffer size to be used in client/server network.
		SFPDPSpeed GetSFPDPSpeedOption() { return m_SFPDPSpeed; }

		/// The setter for the network buffer size to be used in client/server network.
		void SetSFPDPSpeedOption(SFPDPSpeed speed) { m_SFPDPSpeed = speed; }

        //unsigned long long GetBufferPoint();
        //unsigned long long GetWriterPoint();

		/// The accessor for the run number.
		int GetRunNumber();

		/// The setter for the run number.
		void SetRunNumber(int num);

		/// Device id to track of relay device.
		int GetID();

		void SetID(int id);

		/// The getter for the relay device 
		int GetRelayID();

		void SetRelayID(int id);

		void SetRelayDuration(unsigned long long ns);

		unsigned long long GetRelayDuration();

		void SetRelayOffset(unsigned long long ns);

		unsigned long long GetRelayOffset();
		
		ReplayOption  ReplayFlag;

		unsigned long long GetRecordDuration();

		void SetRecordDuration(unsigned long long d);

		long long RemainRecordTime();

        unsigned long long RecordSpeed();

        long long GetBufferLevel();

        long long GetMemcpySpeed();

        /// The flag to active or not.
        bool m_bActive;

	protected:
		
		/// The file name to save packet data.
		std::string FileName;
		
		/// The flag for the relay option.
		bool m_bRelay;

		/// The thread to record the packets.
        std::unique_ptr<boost::thread> m_Thread;
        std::unique_ptr<boost::thread> m_Thread2;
        std::unique_ptr<boost::thread> m_Thread3;
        std::unique_ptr<boost::thread> m_Thread4;
        //boost::thread m_Thread;

		/// The thread logic.
		std::unique_ptr<NetworkDeviceRunner> m_Runner;
        std::unique_ptr<NetworkDeviceRunner> m_Runner2;
        std::unique_ptr<NetworkDeviceRunner> m_Runner3;
        std::unique_ptr<NetworkDeviceRunner> m_Runner4;

		/// The number of saved packets.
		long long m_nSavedPackets;



		/// The option to read/write.
		RunOption m_runOption;

		/// The packet monitors to be notified.
		std::vector<std::shared_ptr<IPacketMonitor>> m_Monitors;

		/// The data speed monitor.
		DataSpeedMonitor m_SpeedMonitor;

		/// The speed option for SFPDP.
		SFPDPSpeed m_SFPDPSpeed;

		/// Record controller.
		std::shared_ptr<RecordController> recorder;

	};
}

#endif
