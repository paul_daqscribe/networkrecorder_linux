TEMPLATE = lib
TARGET = NetworkLib
DESTDIR = ../build
CONFIG += staticlib
CONFIG += c++11


//QMAKE_CXXFLAGS

win32: DEFINES += _XKEYCHECK_H
DEFINES += _NT_TOOLS \
        _NTAPI_EXTDESCR_7_ \
        HAVE_STRING_H \
        _NT_HOST_64BIT \
         D_FILE_OFFSET_BITS=64

INCLUDEPATH += . \
    ./../../../boost_1_63_0 \
    ./../../ThirdParty/Napatech/include

win32: INCLUDEPATH += ./../../ThirdParty/winpcap/Include \
    ./../../ThirdParty/SFPDP/include

unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_thread

INCLUDEPATH += $$PWD/../../../boost_1_63_0
DEPENDPATH += $$PWD/../../../boost_1_63_0

unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_thread
unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_filesystem
unix: LIBS += -L$$PWD/../../../boost_1_63_0/stage/lib/ -lboost_system


INCLUDEPATH += $$PWD/../ /opt/napatech3/include
DEPENDPATH += $$PWD/../

LIBS += -L"./../../../boost_1_63_0/lib32-msvc-12.0" \

win32: LIBS += -L"./../../ThirdParty/winpcap/lib" \   -lwpcap \
    -lWs2_32 \
    -l./../../ThirdParty/Napatech/lib -llibntapi64 \
    -l./../../ThirdParty/Napatech/lib -llibntutil64

unix: LIBS += -L/opt/napatech3/lib -lpcap \
                -L/opt/napatech3/lib -lntapi \
                -L/opt/napatech3/lib -lntutil

#######  ubuntu #############

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/ -lrt
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/debug/ -lrt
#else:unix: LIBS += -L$$PWD/../../../../../usr/lib/x86_64-linux-gnu/ -lrt

#INCLUDEPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu
#DEPENDPATH += $$PWD/../../../../../usr/lib/x86_64-linux-gnu

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/librt.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/librt.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/release/rt.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/debug/rt.lib
#else:unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/lib/x86_64-linux-gnu/librt.a

#unix:!macx: LIBS += -L$$PWD/../../../../../../usr/lib/x86_64-linux-gnu/ -lnuma

#INCLUDEPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu
#DEPENDPATH += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu

#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../../../../usr/lib/x86_64-linux-gnu/libnuma.a

#######  ubuntu #############


#######  RHEL ##############
unix:!macx: LIBS += -L$$PWD/../../../../../usr/lib64/ -lnuma

INCLUDEPATH += $$PWD/../../../../../usr/lib64
DEPENDPATH += $$PWD/../../../../../usr/lib64
#######  RHEL ##############


include(NetworkLib.pri)
