#include "SFPDPDevice.h"
#include "RecordController.h"

/// constant variable for memory handling.
const int PLX_PHYSICAL_BUFFER_SIZE = PACKET_BNUM * 20;

namespace NetworkLib
{
	struct SFPDPDevice::DMARunner
	{
		DMARunner(int port, int nBuffer, bool bread, int buffersize = PACKET_BNUM)
            : WrittenBufferCount(0)
            , ReadyBufferCount(0)
            , m_port(port)
            , m_bRunning(true)
            , NumBuffers(nBuffer)
            , bufferSize(buffersize)
			, dataSize(buffersize * 4)
			, m_bRead(bread)
		{
			pDeviceManager = SFPDPManager::GetInstance();
		}

		/**
		 * operator() is the implementation of the device running logic.
		 */
		void operator()()
		{
			if (m_bRead)
			{
#ifdef SFPDPINCLUDE
				EK_SFM_FIFO_Flush(pDeviceManager->hDev, m_port);
#endif
				while (m_bRunning)
				{
					int rtn = 0;
#ifdef SFPDPINCLUDE
               unsigned long idx = WrittenBufferCount % NumBuffers;
               rtn = EK_SFM_RegRead(pDeviceManager->hDev, m_port, TGT_RD_AV);
					if (rtn * 2 >= bufferSize)
					{
						rtn = EK_SFM_DMA_Read(pDeviceManager->hDev, m_port, m_port - 1, idx * dataSize, bufferSize);
					}
#endif
					rtn = dataSize;
					double d = (double)nt_clock();
					while (nt_clock() - d < 10) {}
					if (rtn >= dataSize)
					{
						++WrittenBufferCount;
					}
				}
			}
			else
			{
				while (m_bRunning)
				{
					if (ReadyBufferCount > WrittenBufferCount)
					{
						int rtn = 0;
#ifdef SFPDPINCLUDE
                  unsigned long idx = WrittenBufferCount % NumBuffers;
                  rtn = EK_SFM_DMA_Write(pDeviceManager->hDev, m_port, m_port - 1, idx * dataSize, bufferSize);
#else
						rtn = dataSize;
						double d = (double)nt_clock();
						while (nt_clock() - d < 10) {}
#endif
						if (rtn == dataSize)
						{
							++WrittenBufferCount;
						}
					}
				}
			}
		}

		/**
		* Reset makes to start the thread.
		* @return The device is set to run continually.
		*/
		void Reset() { m_bRunning = true; }

		/**
		* Stop stops the thread.
		* @return The device is stopped.
		*/
		void Stop() { m_bRunning = false; }

		/**
		* IsRunning is the getter for the running state.
		* @return True if the device is running otherwise return false.
		*/
		bool IsRunning() { return m_bRunning; }

		int WrittenBufferCount;

		int ReadyBufferCount;

	private:

		/// The SFDPD device port to run.
		int m_port;

		/// The flag for running state of the thread.
		bool m_bRunning;

		int NumBuffers;

		int bufferSize;

		int dataSize;

		bool m_bRead;

		SFPDPManager * pDeviceManager;
	};

	SFPDPDevice::SFPDPDevice(int port)
		: INetworkDevice()
		, m_BufferSize(PACKET_BNUM)
		, m_DataSize(PACKET_BNUM * 4)
	{
		networkType = SFPDP;

		pDeviceManager = SFPDPManager::GetInstance();
		Name = "SFPDP Device";
		Address = std::to_string((long long)port);
      FileName = "SFPDP" + Address;
	}

	SFPDPDevice::~SFPDPDevice()
	{
		Stop();
	}

	void SFPDPDevice::Monitor()
	{
		Status = "";
		if (!m_bActive) return;
#ifdef SFPDPINCLUDE
      int port = std::stoi(Address);
      if (!pDeviceManager->IsOpen())
		{
			Status = "Memory is not allocated";
			return;
		}
		unsigned int regVal = EK_SFM_RegRead(pDeviceManager->hDev, port, SFP_SPEED);
		regVal = regVal & MASK_2_5G;
		if (m_SFPDPSpeed == SPEED_1G)
			regVal = regVal | MASK_1G;
		if (m_SFPDPSpeed == SPEED_2_1G)
			regVal = regVal | MASK_2_1G;
		EK_SFM_RegWrite(pDeviceManager->hDev, port, SFP_SPEED, regVal | MASK_SOFTWARE);
#endif
		bool bread = true;
		if (m_runOption == READ_FROM_DEVICE)
		{
			try
			{
				m_WriteFileStream.open(FileName.c_str(), std::ios::binary);
				if (!m_WriteFileStream.good())
				{
					ErrorInfo = FileName + " is not opened";
				}
			}
			catch (const std::exception& e)
			{
				ErrorInfo = e.what();
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			if (recorder->Running)
			{
				try
				{
					m_ReadFileStream.open(FileName.c_str(), std::ios::binary);
					if (!m_ReadFileStream.good())
					{
						ErrorInfo = FileName + " is not opened";
					}
				}
				catch (const std::exception& e)
				{
					ErrorInfo = e.what();
				}
			}
			bread = false;
		}
		if (m_DMARunner) m_DMARunner.release();
		m_DMARunner = std::unique_ptr<DMARunner>(new DMARunner(std::stoi(Address), 5, bread, (int)m_BufferSize));
		m_DMAThread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_DMARunner)));
		Status = "";
		m_nSavedPackets = 0;
		ErrorInfo.clear();
		m_SpeedMonitor.Reset();
		if (m_Runner) m_Runner.release();
		m_Runner = std::unique_ptr<NetworkDeviceRunner>(new NetworkDeviceRunner(std::shared_ptr<INetworkDevice>(this)));
		m_Thread = std::unique_ptr<boost::thread>(new boost::thread(boost::ref(*m_Runner)));
	}

	void SFPDPDevice::Stop()
	{
		if (m_Runner) m_Runner->Stop();
		if (m_DMARunner) m_DMARunner->Stop();
		if (m_runOption == READ_FROM_DEVICE)
		{
			if (recorder->Running)
			{
				WriteDuration();
				m_WriteFileStream.close();
			}
			Status = recorder->Running ? "\n  Recorded" : "\n  Received";
			Status += " data (MBytes) : " + std::to_string((long double)m_SpeedMonitor.GetTotalDataBytes() / 1000000.0);
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			if (recorder->Running)
			{
				m_ReadFileStream.close();
			}
			Status = "\n  Sent data (MBytes) : " + std::to_string((long double)m_SpeedMonitor.GetTotalDataBytes() / 1000000.0);
		}
	}

	void SFPDPDevice::StopThread()
	{
		if (m_Thread)
		{
			try
			{
				m_Thread->join();
			}
			catch (...)
			{
			}
			m_Thread.release();
		}
		if (m_DMAThread)
		{
			try
			{
				m_DMAThread->join();
			}
			catch (...)
			{
			}
			m_DMAThread.release();
		}
	}

	void SFPDPDevice::RecordBegin()
	{
        recorder->Open(true, 0);
	}

	void SFPDPDevice::RecordEnd()
	{
		recorder->Close();
	}

	void SFPDPDevice::WriteDuration()
	{
        m_WriteFileStream.seekp(sizeof(double));
		double t = m_SpeedMonitor.GetDataDuration();
		m_WriteFileStream.write((char*)&t, sizeof(double));
	}

	void SFPDPDevice::HandlePacket()
	{
#ifdef SFPDPINCLUDE
      int port = std::stoi(Address) - 1;
#endif
		int nPackets = 0;
		if (m_runOption == READ_FROM_DEVICE)
		{
			if (m_DMARunner && m_DMARunner->WrittenBufferCount > m_DMARunner->ReadyBufferCount)
			{
#ifdef SFPDPINCLUDE
            int idx = m_DataSize * (m_DMARunner->ReadyBufferCount % 5);
            memcpy((char*)&readbuffer[0], (void*)(pDeviceManager->hDev.PhysBuffer[port].UserAddr + idx), m_DataSize);
#endif
				if (!recorder->Running)
				{
                    unsigned int first = readbuffer[0];
					unsigned int last = readbuffer[m_DataSize / 4 - 1];
					ErrorInfo = "first : " + std::to_string((long long)first) + " last : " + std::to_string((long long)last) + " : counter " + std::to_string((long long)m_DMARunner->ReadyBufferCount);
				}
				m_DMARunner->ReadyBufferCount++;
				nPackets = m_DataSize;
				if (recorder->Running)
				{
					double ts[2] = { m_SpeedMonitor.GetDuration(), 0 };
					m_WriteFileStream.write((char*)&ts[0], sizeof(double) * 2);
					m_WriteFileStream.write((char*)&readbuffer[0], m_DataSize);
					m_nSavedPackets += m_DataSize;
				}
			}
		}
		else if (m_runOption == WRITE_TO_DEVICE)
		{
			if (m_DMARunner &&  (m_DMARunner->ReadyBufferCount - m_DMARunner->WrittenBufferCount) < 5)
			{
				double ts[2] = { 0, 0 };
				if (!m_ReadFileStream.eof() && !m_ReadFileStream.fail() && !m_ReadFileStream.bad())
				{
					m_ReadFileStream.read((char*)&ts[0], sizeof(double) * 2);
					m_ReadFileStream.read((char*)&readbuffer[0], m_DataSize);
				}
				if (!recorder->Running)
				{
					readbuffer[0] = m_DMARunner->ReadyBufferCount;
					readbuffer[m_DataSize / 4 - 1] = m_DMARunner->ReadyBufferCount;
				}
#ifdef SFPDPINCLUDE
            int idx = m_DataSize * (m_DMARunner->ReadyBufferCount % 5);
            memcpy((void*)(pDeviceManager->hDev.PhysBuffer[port].UserAddr + idx), (char*)&readbuffer[0], m_DataSize);
#endif
				m_DMARunner->ReadyBufferCount++;
				nPackets = m_DataSize;
			}
		}

		m_SpeedMonitor.UpdateData(nPackets);
		m_SpeedMonitor.EndSample();
		m_SpeedMonitor.BeginSample();
		for (size_t i = 0; i < m_Monitors.size(); ++i)
		{
			m_Monitors[i]->Update((int)m_nSavedPackets);
		}
	}

	SFPDPManager * SFPDPManager::GetInstance()
	{
		if (instance == 0)
		{
            instance = new SFPDPManager();
		}
		return instance;
	}

   void SFPDPManager::Clear()
   {
      delete instance;
      instance = 0;
   }

	void SFPDPManager::Init()
	{
		m_MemoryOK = false;
#ifdef SFPDPINCLUDE
		EK_OpenDevice(0x10EE, 0x0009, &hDev);
		if (hDev.PLXObj.hDevice > 0)
		{
			m_MemoryOK = true;
			EK_SFM_SetTimeStampMode(hDev, 0);
			EK_SFM_Reset(hDev, 0x800000ff);
			Sleep(20);
			EK_AllocPCIPhyBuffer(&hDev, PLX_PHYSICAL_BUFFER_SIZE, 1);
			for (int i = 0; i < 4; ++i)
			{
				int port = i + 1;
				if (hDev.PhysBuffer[i].Size < PLX_PHYSICAL_BUFFER_SIZE)
				{
					m_MemoryOK = false;
				}
				unsigned int regVal = EK_SFM_RegRead(hDev, port, SFP_SPEED);
				regVal = regVal & MASK_2_5G;
				EK_SFM_RegWrite(hDev, port, SFP_SPEED, regVal | MASK_SOFTWARE);
			}
		}
#else
		m_MemoryOK = true;
#endif
	}

	bool SFPDPManager::IsOpen()
	{
#ifdef SFPDPINCLUDE
		return (hDev.PLXObj.hDevice > 0 && m_MemoryOK);
#else
		return true;
#endif
	}

   SFPDPManager::~SFPDPManager()
	{
#ifdef SFPDPINCLUDE
		EK_FreePCIPhyBuffer(&hDev);
		EK_CloseDevice(&hDev);
#endif
	}

	void SFPDPManager::Reset()
	{
#ifdef SFPDPINCLUDE
		EK_SFM_Reset(hDev, 0x800000ff);
		Sleep(20);
#endif
	}

	SFPDPManager * SFPDPManager::instance = 0;
}
