#include "NetworkDevice.h"
#include "SFPDPDevice.h"
#include "GESFPDPDevice.h"
#include "NTNetworkDevice.h"
#include "NetworkDeviceManager.h"
#include "NetworkDeviceFactory.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "DeviceRecordSetting.h"
#include <boost/filesystem.hpp>
#include "logger.h"
#include "usertagmanager.h"
#include <iomanip>

//#define RedHat
#define Ubuntu

namespace NetworkLib
{
	NetworkDeviceManager* NetworkDeviceManager::instance = 0;

	NetworkDeviceManager* NetworkDeviceManager::GetInstance()
	{
		if (instance == 0)
		{
			instance = new NetworkDeviceManager();
		}
		return instance;
	}

	void NetworkDeviceManager::Clear()
	{
		Logger::Clear();
		DeviceRecordSetting::Clear();
		SFPDPManager::Clear();
		GESFPDPManager::Clear();
		instance = 0;
		delete instance;
		
	}

	NetworkDeviceManager::NetworkDeviceManager()
        : ServerPort(5999)
        , TriggerPort(1)
        , m_bRunning(false)
		, m_MonitoringMode(STOP)
		, m_PacketSize(2048)
		, m_Option(NAPA_TECH)
	{
	}

	NetworkDeviceManager::~NetworkDeviceManager()
	{
	}

	void NetworkDeviceManager::AddPacketMonitor(const std::shared_ptr<IPacketMonitor>& pMonitor)
	{
		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = recordDevices[i];
			pDevice->AddPacketMonitor(pMonitor);
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = replayDevices[i];
			pDevice->AddPacketMonitor(pMonitor);
		}
	}

	void NetworkDeviceManager::Monitor()
	{
		if (m_bRunning) return;
		m_bRunning = true;
		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			const std::shared_ptr<INetworkDevice>& pDevice = recordDevices[i];
            //if (pDevice)
            if (pDevice->m_bActive)
			{
                pDevice->Monitor(false, i+1);
			}
		}
        for (size_t i = 0; i < replayDevices.size(); ++i)
        {
            const std::shared_ptr<INetworkDevice>& pDevice = replayDevices[i];
            //if (pDevice)
            if (pDevice->m_bActive)
            {
                pDevice->Monitor(true, i+1);
            }
        }
		m_Monitor.Reset();
	}

	void NetworkDeviceManager::Stop()
	{
		if (!m_bRunning) return;
		m_bRunning = false;

		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			auto& pDevice = recordDevices[i];
            if (pDevice)
			{
				pDevice->RecordEnd();
                pDevice->Stop();
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			auto& pDevice = replayDevices[i];
			if (pDevice)
			{
				pDevice->RecordEnd();
				pDevice->Stop();
			}
		}

		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			auto& pDevice = recordDevices[i];
			if (pDevice)
			{
                pDevice->StopThread();
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			auto& pDevice = replayDevices[i];
			if (pDevice)
			{
				pDevice->StopThread();
			}
		}
		m_MonitoringMode = STOP;
	}

	std::string NetworkDeviceManager::GetErrorInfo()
	{
		std::string error;
		for (size_t i = 0; i < recordDevices.size(); ++i)
		{
			auto& pDevice = recordDevices[i];
			if (pDevice && pDevice->ErrorInfo != "")
			{
				error += std::to_string((long long)i + 1) + ":" + pDevice->ErrorInfo + ";";
			}
		}
		for (size_t i = 0; i < replayDevices.size(); ++i)
		{
			auto& pDevice = replayDevices[i];
			if (pDevice && pDevice->ErrorInfo != "")
			{
				error += std::to_string((long long)i + 1) + ":" + pDevice->ErrorInfo + ";";
			}
		}
		return error;
	}

	void NetworkDeviceManager::Init(const std::string& filename)
	{
		recordDevices.clear();
		replayDevices.clear();
		NetworkDeviceFactory factory(m_Option);

        //NET-44 add Filtered stream
        //Add new function for Filtered_Stream at xml.
        PreReadSystemInfo(filename);

		AddDevices(factory);
        ReadSystemInfo(filename);

		for (int i = 0; i < (int)recordDevices.size(); ++i)
		{
			recordDevices[i]->SetID(i + 1);
		}
		for (int i = 0; i < (int)replayDevices.size(); ++i)
		{
			replayDevices[i]->SetID(i + 1);
		}


	}

	void NetworkDeviceManager::AddDevices(INetworkDeviceFactory& factory)
	{
		std::vector<INetworkDevice*> devices;
		factory.GetNetworkDevices(devices);
		for (int i = 0; i < (int)devices.size(); ++i)
		{
			INetworkDevice* pDevice = devices[i];
			if (pDevice)
			{
				if (pDevice->GetRunOption() != WRITE_TO_DEVICE)
				{
					pDevice->SetID((int)recordDevices.size() + 1);
					recordDevices.push_back(std::shared_ptr<INetworkDevice>(pDevice));
				}
				else
				{
					pDevice->SetID((int)replayDevices.size() + 1);
					replayDevices.push_back(std::shared_ptr<INetworkDevice>(pDevice));
				}
			}
		}
	}

	void NetworkDeviceManager::Record()
	{
		if (!IsRunning())
		{
			Monitor();
		}
        for (int i = 0; i < (int)recordDevices.size(); ++i)
        {

            auto setting = DeviceRecordSetting::GetInstance();

            //get group path
            for(int x=0; x < setting->v_Group.size(); x++)
            {
                for(int y = 0; y < setting->v_Group[x].m_NumPort; y++)
                {
                     if(i+1 == setting->v_Group[x].v_Port[y])
                     {
                         string path = setting->v_Group[x].m_Path;

                         if(path=="path1")
                             recordDevices[i]->SetRecordFile(setting->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber(), 0));
                         else if (path=="path2")
                             recordDevices[i]->SetRecordFile(setting->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber(), 1));
                         else if (path=="path3")
                             recordDevices[i]->SetRecordFile(setting->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber(), 2));
                         else if (path=="path4")
                             recordDevices[i]->SetRecordFile(setting->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber(), 3));
                         else
                             recordDevices[i]->SetRecordFile(setting->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber(), 0));
                     }
                }
            }

            //recordDevices[i]->SetReplayFile(DeviceRecordSetting::GetInstance()->GetNetworkRecordFileName(recordDevices[i]->GetID(), recordDevices[i]->GetRunNumber()));
            recordDevices[i]->RecordBegin(i);

        }
	}

    //NET-44 add Filtered stream
    void NetworkDeviceManager::PreReadSystemInfo(const std::string& xmlFile)
    {
        try
        {
            auto pRecordSetting = DeviceRecordSetting::GetInstance();
            using boost::property_tree::ptree;
            ptree pt;
            read_xml(xmlFile, pt);
            ptree group = pt.get_child("Record");
            int index = 0;
            for (ptree::iterator itr = group.begin(); itr != group.end(); ++itr)
            {
                if (itr->first == "Filtered_Stream")
                {
                    if (index >= FILTERED_STREAM_SIZE)
                        return;

                    pRecordSetting->m_UserStream[index] = itr->second.get<string>("Expression");
                    index++;
                }
            }
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what() << "\n";
        }

    }

	void NetworkDeviceManager::ReadSystemInfo(const std::string& xmlFile)
	{
		try
		{
			using boost::property_tree::ptree;
			ptree pt;
			read_xml(xmlFile, pt);
			m_Option = (NetworkType)pt.get<int>("Network.Setting");
			m_PacketSize = pt.get<int>("Network.BufferSize");
			ServerPort = pt.get<int>("Network.ServerPort");
			TriggerPort = pt.get<int>("Network.TriggerPort");
			auto pRecordSetting = DeviceRecordSetting::GetInstance();

            //ptree group2 = pt.get_child("Mode");
            m_Mode =  pt.get<string>("Mode.MultiThreadUse");
            m_StorageNum =  pt.get<int>("Mode.StoregeNum");
            m_RaidUse =  pt.get<string>("Mode.RaidUse");
            if (m_StorageNum <= 0) m_StorageNum = 1;
            pRecordSetting->m_Mode = m_Mode;
            pRecordSetting->m_StorageNum = m_StorageNum;
            pRecordSetting->m_RaidUse = m_RaidUse;

            //NET-49 Add auto file remove function.
            pRecordSetting->m_AutoFileRemoveUse  = pt.get<string>("Mode.AutoFileRemoveUse");
            pRecordSetting->m_AutoFileRemoveRate = pt.get<int>("Mode.AutoFileRemoveRate");
            if (pRecordSetting->m_AutoFileRemoveRate  <= 0) pRecordSetting->m_AutoFileRemoveRate  = 90;

            ptree group = pt.get_child("Record");
            //NET-51
            m_Extension = pt.get<std::string>("Record.Extension");

            for (ptree::iterator itr = group.begin(); itr != group.end(); ++itr)
            {
                if (itr->first == "Group")
                {
                    m_group.NUMAnode = itr->second.get<int>("NUMAnode");
                    m_group.SetPort = itr->second.get<string>("SetPort");
                    m_group.CPUAffinity = itr->second.get<string>("CPUAffinity");
                    m_group.Path = itr->second.get<string>("Path");
                    v_group.push_back(m_group);
                    pRecordSetting->SetGroup(m_group.NUMAnode, m_group.SetPort, m_group.CPUAffinity, m_group.Path);
                }
            }
            pRecordSetting->SetGroupCPUAffinity();

            ProgramCPUAffinity= pt.get<std::string>("Record.ProgramCPUAffinity");
            ThreadPriority = pt.get<std::string>("Record.ThreadPriority");
            ThreadSleep = pt.get<std::string>("Record.ThreadSleep");
            pRecordSetting->setProgramCPUAffinity(ProgramCPUAffinity);
            pRecordSetting->SetThreadPriority(ThreadPriority);
            pRecordSetting->SetThreadSleep(ThreadSleep);
            pRecordSetting->AIOSleep = pt.get<unsigned long>("Record.AIOSleep");
            pRecordSetting->SetDebugMode(pt.get<long long>("Record.DebugMode"));

            pRecordSetting->SetRecordPath(pt.get<std::string>("Record.Path1"),0);
            pRecordSetting->SetRecordPath(pt.get<std::string>("Record.Path2"),1);
            pRecordSetting->SetRecordPath(pt.get<std::string>("Record.Path3"),2);
            pRecordSetting->SetRecordPath(pt.get<std::string>("Record.Path4"),3);

//            pRecordSetting->SetRecordPathDeviceName(pt.get<std::string>("Record.Path1DeviceName"),0);
//            pRecordSetting->SetRecordPathDeviceName(pt.get<std::string>("Record.Path2DeviceName"),1);
//            pRecordSetting->SetRecordPathDeviceName(pt.get<std::string>("Record.Path3DeviceName"),2);
//            pRecordSetting->SetRecordPathDeviceName(pt.get<std::string>("Record.Path4DeviceName"),3);


			pRecordSetting->SetRecordName(pt.get<std::string>("Record.RecordName"));
			pRecordSetting->SetRecordMaximumSize(pt.get<long long>("Record.RecordMaxSize"));
			pRecordSetting->READ_NUM_SINGLE_BUFFERS = pt.get<unsigned long>("Record.READ_NUM_SINGLE_BUFFERS");
			pRecordSetting->READ_NUM_BUFFERS = pt.get<unsigned long>("Record.READ_NUM_BUFFERS");
			pRecordSetting->READ_BUFFER_SIZE = pt.get<unsigned long>("Record.READ_BUFFER_SIZE");
			pRecordSetting->WRITE_NUM_SINGLE_BUFFERS = pt.get<unsigned long>("Record.WRITE_NUM_SINGLE_BUFFERS");
			pRecordSetting->WRITE_NUM_BUFFERS = pt.get<unsigned long>("Record.WRITE_NUM_BUFFERS");
			pRecordSetting->WRITE_BUFFER_SIZE = pt.get<unsigned long>("Record.WRITE_BUFFER_SIZE");
			pRecordSetting->PRE_LOAD_RATE = pt.get<double>("Record.PRE_LOAD_RATE");


            //namebuilder << m_RecordPath[num_path] << "/" << m_RecordName;

#ifdef RedHat
            std::stringstream ss;
            std::time_t t = std::time(0);   // get time now
                std::tm* now = std::localtime(&t);
                ss << (now->tm_year + 1900) << ':'
                     << (now->tm_mon + 1) << ':'
                     <<  now->tm_mday;
//                     << now->hour << ':'
//                     << now->min << ':'
//                     << now->sec;
            string temp = ss.str();
            pRecordSetting->RecordTriggerTime = (*now);
#else

//            std::cout << (now->tm_year + 1900) << '-'
//                 << (now->tm_mon + 1) << '-'
//                 <<  now->tm_mday
//                 << "\n";


            tm tt;
            std::string ti = pt.get<std::string>("Record.RecordTriggerTime");
            std::istringstream ss(ti);
            ss >> std::get_time(&tt, "%EY:%m:%d-%H:%M:%S");
            time_t t = mktime(&tt);
            auto tm = localtime(&t);

            if (tm)
            {
                pRecordSetting->RecordTriggerTime = (*tm);
            }
            NTNetworkDeviceManager::GetInstance()->numaIdx = pt.get<int>("Record.NUMA");
#endif


            ptree SSD = pt.get_child("SSDEndurance");
            for (ptree::iterator itr = SSD.begin(); itr != SSD.end(); ++itr)
            {
                std::string test = itr->second.get_value<std::string>();
                pRecordSetting->SetSSDEndurace(test);
            }

            //
            //NetworkDevices
            //
            ptree su = pt.get_child("NetworkDevices");
			std::vector<bool> setdevs(recordDevices.size(), false);
            for (ptree::iterator itr = su.begin(); itr != su.end(); ++itr)
			{
				if (itr->first == "RecordDevice")
				{
					std::string name = itr->second.get<std::string>("Name");
					std::string address = itr->second.get<std::string>("Address(Port)");
                    std::string filename = itr->second.get<std::string>("FileRecordName");
					std::string Filter = itr->second.get<std::string>("Filter");
					int num = itr->second.get<int>("RunNumber");
					ReplayOption relayflag = (ReplayOption)itr->second.get<int>("ReplayFlag");
					unsigned long long relayoffset = itr->second.get<unsigned long long>("RelayOffset");
					unsigned long long relayduration = itr->second.get<unsigned long long>("RelayDuration");
					unsigned long long recordduration = itr->second.get<unsigned long long>("RecordDuration");
					int nLink = itr->second.get<int>("LinkRate");
					bool bSave = itr->second.get<bool>("Active");
					for (int i = 0; i < (int)recordDevices.size(); ++i)
					{
						if (setdevs[i]) continue;
						auto& dev = recordDevices[i];
						if (dev->Address == address)
						{
							setdevs[i] = true;
							dev->Name = name;
                            dev->SetRecordFile(filename);
							dev->SetActiveFlag(bSave);
							dev->ReplayFlag = relayflag;
							dev->SetRecordDuration(recordduration);
							dev->SetRelayOffset(relayoffset);
							dev->SetRelayDuration(relayduration);
							dev->PCAPFilter = Filter;
							dev->SetRunNumber(num);
							auto pDev = dynamic_cast<INetworkDevice*>(dev.get());
							auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
							if (pSFPDP) pSFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
							if (pGESFPDP) pGESFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							break;
						}
					}
				}
			}
			std::vector<bool> setreplaydevs(replayDevices.size(), false);
			for (ptree::iterator itr = su.begin(); itr != su.end(); ++itr)
			{
				if (itr->first == "ReplayDevice")
				{
					std::string name = itr->second.get<std::string>("Name");
					std::string address = itr->second.get<std::string>("Address(Port)");
                    std::string filename = itr->second.get<std::string>("FileReplayName");
					std::string Filter = itr->second.get<std::string>("Filter");
					int num = itr->second.get<int>("RunNumber");
					ReplayOption relayflag = (ReplayOption)itr->second.get<int>("ReplayFlag");
					unsigned long long relayoffset = itr->second.get<unsigned long long>("RelayOffset");
					unsigned long long relayduration = itr->second.get<unsigned long long>("RelayDuration");
					unsigned long long recordduration = itr->second.get<unsigned long long>("RecordDuration");
					unsigned long long replyrecordduration = itr->second.get<unsigned long long>("RelayRecordDuration");
					int nLink = itr->second.get<int>("LinkRate");
					bool bSave = itr->second.get<bool>("Active");
					for (int i = 0; i < (int)replayDevices.size(); ++i)
					{
						if (setreplaydevs[i]) continue;
						auto& dev = replayDevices[i];
						if (dev->Address == address)
						{
							setreplaydevs[i] = true;
							dev->Name = name;
							dev->SetReplayFile(filename);
							dev->SetActiveFlag(bSave);
							dev->PCAPFilter = Filter;
							dev->SetRunNumber(num);
							dev->ReplayFlag = relayflag;
							dev->SetRecordDuration(recordduration);
							dev->SetRelayOffset(relayoffset);
							dev->SetRelayDuration(relayduration);
							dev->SetRelayRecordDuration(replyrecordduration);
							auto pDev = dynamic_cast<INetworkDevice*>(dev.get());
							auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
							if (pSFPDP) pSFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
							if (pGESFPDP) pGESFPDP->SetSFPDPSpeedOption(SFPDPSpeed(nLink));
							break;
						}
					}
				}
			}
			auto tu = pt.get_child_optional("UserTags");
			if (tu)
			{
				auto& tags = UserTagManager::GetInstance()->Tags;
				tags.clear();
				for (ptree::iterator itr = tu->begin(); itr != tu->end(); ++itr)
				{
					if (itr->first == "UserTag")
					{
						UserTag tag;
						tag.Name = itr->second.get<std::string>("Name");
						tag.Description = itr->second.get<std::string>("Description");
						tags.push_back(tag);
					}
				}
			}
		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << "\n";
		}
	}

	void NetworkDeviceManager::WriteSystemInfo(const std::string& xmlFile)
	{
        if ((int)recordDevices.size() < 1 || (int)replayDevices.size() < 1)
            return;

        try
        {
            using boost::property_tree::ptree;
            ptree pt;
            pt.put("NetworkRecorder.version", "v1.7.0");
            pt.put("Network.Setting", m_Option);
            pt.put("Network.Setting.Descirption", "0:PCAP, 1:SFPDP, 2:GESFPDP, 3:NAPATECH");
            pt.put("Network.BufferSize", m_PacketSize);
            pt.put("Network.ServerPort", ServerPort);
            pt.put("Network.TriggerPort", TriggerPort);
            auto pRecordSetting = DeviceRecordSetting::GetInstance();
            if (ProgramCPUAffinity == "")
                ProgramCPUAffinity = "7-10";
            if (ThreadPriority== "")
                ThreadPriority = "80,50";
            if (ThreadSleep == "")
                ThreadSleep = "1,20";

            if(m_Mode =="")
                m_Mode = "N";

            if(m_RaidUse =="")
                m_RaidUse = "N";

            if(m_StorageNum == 0)
                m_StorageNum =  1;

            pt.put("Mode.MultiThreadUse", m_Mode);
            pt.put("Mode.StoregeNum", m_StorageNum);
            pt.put("Mode.RaidUse", m_RaidUse);

            //NET-49 Add auto file remove function.
            pt.put("Mode.AutoFileRemoveUse", pRecordSetting->m_AutoFileRemoveUse );
            pt.put("Mode.AutoFileRemoveRate", pRecordSetting->m_AutoFileRemoveRate);

            //NET-51
            pt.put("Record.Extension", m_Extension);

            for (int i = 0; i < v_group.size(); ++i)
            {
                ptree & node = pt.add("Record.Group", "");
                node.put("NUMAnode", v_group[i].NUMAnode);
                node.put("SetPort", v_group[i].SetPort);
                node.put("CPUAffinity", v_group[i].CPUAffinity);

                node.put("Path", v_group[i].Path);
            }

            if (v_group.size() <= 0)
            {
                {
                    ptree & node = pt.add("Record.Group", "");
                    node.put("NUMAnode", "0");
                    node.put("SetPort", "1,2");
                    node.put("CPUAffinity", "1-4,5");
                    node.put("Path", "path1");
                }
                {
                    ptree & node = pt.add("Record.Group", "");
                    node.put("NUMAnode", "1");
                    node.put("SetPort", "3,4");
                    node.put("CPUAffinity", "11-14,15");
                    node.put("Path", "path2");
                }
            }

            pt.put("Record.ProgramCPUAffinity", ProgramCPUAffinity);
            pt.put("Record.ThreadPriority", ThreadPriority);
            pt.put("Record.ThreadSleep", ThreadSleep);
            pt.put("Record.AIOSleep", pRecordSetting->AIOSleep);
            pt.put("Record.DebugMode", pRecordSetting->GetDebugMode());
            pt.put("Record.Path1", pRecordSetting->GetRecordPath(0));
            pt.put("Record.Path2", pRecordSetting->GetRecordPath(1));
            pt.put("Record.Path3", pRecordSetting->GetRecordPath(2));
            pt.put("Record.Path4", pRecordSetting->GetRecordPath(3));
//            pt.put("Record.Path1DeviceName", pRecordSetting->GetRecordPathDeviceName(0));
//            pt.put("Record.Path2DeviceName", pRecordSetting->GetRecordPathDeviceName(1));
//            pt.put("Record.Path3DeviceName", pRecordSetting->GetRecordPathDeviceName(2));
//            pt.put("Record.Path4DeviceName", pRecordSetting->GetRecordPathDeviceName(3));
            pt.put("Record.NUMA", NTNetworkDeviceManager::GetInstance()->numaIdx);
            pt.put("Record.RecordName", pRecordSetting->GetRecordName());
            pt.put("Record.RecordMaxSize", pRecordSetting->GetRecordMaximumSize());
            pt.put("Record.READ_NUM_SINGLE_BUFFERS", pRecordSetting->READ_NUM_SINGLE_BUFFERS);
            pt.put("Record.READ_NUM_BUFFERS", pRecordSetting->READ_NUM_BUFFERS);
            pt.put("Record.READ_BUFFER_SIZE", pRecordSetting->READ_BUFFER_SIZE);
            pt.put("Record.WRITE_NUM_SINGLE_BUFFERS", pRecordSetting->WRITE_NUM_SINGLE_BUFFERS);
            pt.put("Record.WRITE_NUM_BUFFERS", pRecordSetting->WRITE_NUM_BUFFERS);
            pt.put("Record.WRITE_BUFFER_SIZE", pRecordSetting->WRITE_BUFFER_SIZE);
            pt.put("Record.PRE_LOAD_RATE", pRecordSetting->PRE_LOAD_RATE);
            auto t = pRecordSetting->RecordTriggerTime;
            std::stringstream str;
            str << t.tm_year + 1900 << ":" << std::setfill('0') << std::setw(2) << t.tm_mon + 1
                << ":" << std::setfill('0') << std::setw(2) << t.tm_mday
                //<< "-" << std::setfill('0') << std::setw(2) << t.tm_hour + 1
                << "-" << std::setfill('0') << std::setw(2) << t.tm_hour
                << ":" << std::setfill('0') << std::setw(2) << t.tm_min
                << ":" << std::setfill('0') << std::setw(2) << t.tm_sec;
            pt.put("Record.RecordTriggerTime", str.str());

            //NET-44 add Filtered stream
            for(int i =0; i < FILTERED_STREAM_SIZE; i ++)
            {
                if (pRecordSetting->m_UserStream[i] != "")
                {
                    ptree & node2 = pt.add("Record.Filtered_Stream", "");
                    node2.put("Expression", pRecordSetting->m_UserStream[i]);
                }
                else
                {
                    ptree & node2 = pt.add("Record.Filtered_Stream", "");
                    node2.put("Expression", "");
                }
            }


            ptree & node = pt.add("SSDEndurance", "");
            for (int x= 0; x < pRecordSetting->v_SSD.size(); x++)
            {
                std::stringstream ss;
                ss << x;
                node.put("TBW" + ss.str(), pRecordSetting->v_SSD[x].m_value);
            }

            for (int i = 0; i < (int)recordDevices.size(); ++i)
            {
                auto pDevice = recordDevices[i];
                ptree & node = pt.add("NetworkDevices.RecordDevice", "");
                node.put("Name", pDevice->Name);
                node.put("Address(Port)", pDevice->Address);
                //node.put("FileRecordName", pDevice->GetRecordFile());
                node.put("FileRecordName","");
                node.put("Filter", pDevice->PCAPFilter);
                node.put("Active", pDevice->GetActiveFlag());
                node.put("RunNumber", pDevice->GetRunNumber());
                node.put("RecordDuration", (unsigned long long)pDevice->GetRecordDuration());
                node.put("RelayOffset", (unsigned long long)pDevice->GetRelayOffset());
                node.put("RelayDuration", (unsigned long long)pDevice->GetRelayDuration());
                node.put("RelayRecordDuration", (unsigned long long)pDevice->GetRelayRecordDuration());
                node.put("ReplayFlag", pDevice->ReplayFlag);
                auto pDev = dynamic_cast<INetworkDevice*>(pDevice.get());
                auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
                auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
                node.put("LinkRate", pSFPDP ? pSFPDP->GetSFPDPSpeedOption() : (pGESFPDP ? pGESFPDP->GetSFPDPSpeedOption() : 0));
            }
            for (int i = 0; i < (int)replayDevices.size(); ++i)
            {
                auto pDevice = replayDevices[i];
                ptree & node = pt.add("NetworkDevices.ReplayDevice", "");
                node.put("Name", pDevice->Name);
                node.put("Address(Port)", pDevice->Address);
                node.put("FileReplayName", pDevice->GetReplayFile());
                node.put("Filter", pDevice->PCAPFilter);
                node.put("Active", pDevice->GetActiveFlag());
                node.put("RunNumber", pDevice->GetRunNumber());
                node.put("RecordDuration", (unsigned long long)pDevice->GetRecordDuration());
                node.put("RelayOffset", (unsigned long long)pDevice->GetRelayOffset());
                node.put("RelayDuration", (unsigned long long)pDevice->GetRelayDuration());
                node.put("RelayRecordDuration", (unsigned long long)pDevice->GetRelayRecordDuration());
				
                node.put("ReplayFlag", pDevice->ReplayFlag);
                auto pDev = dynamic_cast<INetworkDevice*>(pDevice.get());
                auto pSFPDP = dynamic_cast<SFPDPDevice*>(pDev);
                auto pGESFPDP = dynamic_cast<GESFPDPDevice*>(pDev);
                node.put("LinkRate", pSFPDP ? pSFPDP->GetSFPDPSpeedOption() : (pGESFPDP ? pGESFPDP->GetSFPDPSpeedOption() : 0));
            }
            auto tags = UserTagManager::GetInstance()->Tags;
            for (int i = 0; i < (int)tags.size(); ++i)
            {
                auto tag = tags[i];
                ptree & node = pt.add("UserTags.UserTag", "");
                node.put("Name", tag.Name);
                node.put("Description", tag.Description);
            }
            boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
            write_xml(xmlFile, pt, std::locale(), settings);
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what() << "\n";
        }
	}

	std::string NetworkDeviceManager::GetExtension()
	{
        if (m_Extension == "PCAP")
        {
            return ".pcap";
        }
        else
        {
            return (m_Option == PCAP ? ".pcap" : (m_Option == NAPA_TECH ? ".ntcap" : ".sfpdp"));
        }
	}

}
