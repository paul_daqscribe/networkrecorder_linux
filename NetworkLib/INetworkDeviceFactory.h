#ifndef INETWORKDEVICEFACTORY_H
#define INETWORKDEVICEFACTORY_H

#include "INetworkDevice.h"

namespace NetworkLib
{

	/**
	 * INetworkDeviceFactory is the interface fo DeviceFactory to add the new network devices.
	 */
	class INetworkDeviceFactory
	{
	public:
		/// Default destrucctor.
		~INetworkDeviceFactory() {}

		/**
		 * Interface to get the device.
		 * @param devices The storage for the devices to be added.
		 */
		virtual void GetNetworkDevices(std::vector<INetworkDevice*>& devices) = 0;
	};

}
#endif