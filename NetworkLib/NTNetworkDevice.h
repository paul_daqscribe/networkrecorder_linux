#ifndef NTNETWORKDEVICE_H
#define NTNETWORKDEVICE_H

#include "NetworkDevice.h"
#include "nt.h"
#include "LogManager.h"
#include <boost/thread/mutex.hpp>
#include <boost/array.hpp>>
#include <stdio.h>
#include <iostream>
#include <fstream>

#define FILTERED_STREAM_SIZE 4

namespace NetworkLib
{

	/// ntpcap_ts_s is the time structure for Napatech device.
	struct ntpcap_ts_s
	{
		/// Second
		uint32_t sec;

		/// Used second
		uint32_t usec;
	};

	/**
	* NTNetworkDevice implementation of INetworkDevice for Napatech devices with ntpcap file support.
	*/
	class NTNetworkDevice : public INetworkDevice
	{

	public:

		/// Default constructor.
		NTNetworkDevice();

		/// Default destructor.
		~NTNetworkDevice();

		/// Implementation of INetworkDevice.
        virtual void Monitor(bool bRead, int ch);

		virtual void Stop();

		virtual void StopThread();

        virtual void HandlePacket(int seq);

        virtual void RecordBegin(int ch);

		virtual void RecordEnd();

		void Replay();

	private:
		

		/// Handle to the RX stream
		NtNetStreamRx_t hNetRx;

		/// Handle to a config stream
		NtConfigStream_t hCfgStream;

		/// Return data structure from the NT_NTPL() call.
		NtNtplInfo_t ntplInfo;

		/// Net buffer container. Segment data is returned in this when calling NT_NetRxGet().
		NtNetBuf_t hNetBuf;
        NtNetBuf_t hNetBuf2;
        NtNetBuf_t hNetBuf3;
        NtNetBuf_t hNetBuf4;
        NtNetBuf_t hNetBuf5;
        NtNetBuf_t hNetBuf6;

		/// NetRx read command structure.
		NtNetRx_t readCmd;

		/// Packet netbuf structure.
		NtNetBuf_s pktNetBuf;


		/// The time stamp.
		ntpcap_ts_s *pTs;

		/// Handle to the TX stream
		NtNetStreamTx_t hNetTx;

		/// Net buffer container. Used when getting a transmit buffer
		NtNetBuf_t hNetBufTx;

		/// Handle to a info stream
		NtInfoStream_t hInfo;

		/// Buffer to hold data from infostream
		NtInfo_t infoRead;

		/// TX mode - Do we use absolut or relative mode
		NtTxTimingMethod_e txTiming;

        NtNetStreamFile_t hNetFile;     // Handle to the File stream
        NtNetBuf_t hNetBufFile;         // Net buffer container. Used to return segments from the file stream
        uint64_t firstPacketTS=0;       // Timestamp of first packet
        uint64_t lastPacketTS=0;        // Timestamp of last packet
        uint64_t adapterTS=0;           // Timestamp on the adapter
        uint64_t timeDelta=0;           // Calculated time delta
        NtConfigStream_t hConfig;       // Handle to a config stream
        NtConfig_t configWrite;         // Config stream data container
        NtConfig_t configRead;          // Config stream data container

		/// The error information storage.
		char errorBuffer[NT_ERRBUF_SIZE];

		bool bReplay;

		bool check = true;

        //kjk
        long long second_keep;
        typedef boost::array<int, 1000000> array;
        array a;

        int arr[100000];

        unsigned long long timeValue1 = 0;
        unsigned long long timeValue2 = 0;
        unsigned long long count = 0;

        struct timespec time_start;
        struct timespec time_end;
        unsigned long long nanosec = 1000000000;

		int firstPacket;

        /// multi thread
        int status;
        int status2;
        int status3;
        int status4;
        int status5;
        int status6;

        boost::mutex io_mutex;

        bool m_NT_run;
        //bool m_NT_write;
        bool m_NTRxGet_run;

        bool test = true;

        int multiThreadSeq = 1;
        unsigned long n_current = 0;

        unsigned long long multiThread_leftsize = 0;
        bool idx_up = false;
        bool file_idx_up = false;

        unsigned long long leftsize1 = 0;
        unsigned long long leftsize2 = 0;
        unsigned long long leftsize3 = 0;
        unsigned long long leftsize4 = 0;
        unsigned long long leftsize5 = 0;
        unsigned long long leftsize6 = 0;

        unsigned long long size = 0;


        unsigned long long threadSize1 = 0;
        unsigned long long threadSize2 = 0;
        unsigned long long threadSize3 = 0;
        unsigned long long threadSize4 = 0;
        unsigned long long threadSize5 = 0;
        unsigned long long threadSize6 = 0;

        bool isbusy_th1;
        bool isbusy_th2;
        bool isbusy_th3;
        bool isbusy_th4;
        bool isbusy_th5;
        bool isbusy_th6;

        unsigned long newidx1 = 0;
        unsigned long newidx2 = 0;
        unsigned long newidx3 = 0;
        unsigned long newidx4 = 0;
        unsigned long newidx5 = 0;
        unsigned long newidx6 = 0;

        char * m_msg1;
        char * m_msg2;
        char * m_msg3;
        char * m_msg4;
        char * m_msg5;
        char * m_msg6;

        std::string m_Mode;
        ///multi thread end

        //NET-48
        std::string m_replayType = "4GA";


	};

	struct NTNetworkDeviceManager
	{

		static NTNetworkDeviceManager * GetInstance();

		static void Clear();

		void Init();

		bool IsOpen();

		void Close();

		uint8_t numAdapters;

		int nPorts;

		int numaIdx;

        //NET-44 add Filtered stream
        std::string m_UserStream[FILTERED_STREAM_SIZE];

	private:

		bool m_bInitialized;

		static NTNetworkDeviceManager * instance;



	};

}
#endif
