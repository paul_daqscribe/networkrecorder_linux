#ifndef PCAP_HEADER_H
#define PCAP_HEADER_H

#if defined(WIN32) || defined(WIN64)
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "pcap.h"

#ifndef WIN32 
#include <sys/socket.h>
#include <netinet/in.h>
#else
#include <winsock2.h>
#endif

#endif