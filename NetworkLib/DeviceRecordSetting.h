#ifndef NETWORK_DEVICERECORDSETTING_H
#define NETWORK_DEVICERECORDSETTING_H

#include <string>
#ifdef max
#undef max
#endif

#include <algorithm>
#include <vector>

#define FILTERED_STREAM_SIZE 4
#define MAM_STORAGE 4

using namespace std;

namespace NetworkLib
{
    typedef struct GROUP
    {
        int m_NUMAnode;
        int m_NumPort;
        string m_SetPort;
        string m_CPUAffinity;
        string m_Path;
        vector<int> v_Port;
    } GROUP;

    typedef struct SSD
    {
        string m_value;
        string m_SSDmodel;
        double m_SSDWBT;
    } SSD;


	std::vector<std::string> GetFields(const std::string& line, const std::string& delimiters);

	/**
	 * DeviceRecordSetting stores the setting for the recording such as the trigger information and the recording path.
	 */
	class DeviceRecordSetting
	{

	public:


		/// GetInstance supports singleton architecture.
		static DeviceRecordSetting* GetInstance();

		static void Clear();

		/**
		 * GetNetworkRecordFileName for the device index. This handles session name.
		 * @param id The index of the device.
		 * @param record The run number of device.
		 * @param bsession The flag for session file instead of device file.
		 * @return The file name to record the device with the index id.
		 */
        std::string GetNetworkRecordFileName(size_t id, int record, int num_path, bool bsession = false);

//        std::string GetNetworkRecordFileName1(size_t id, int record, bool bsession = false);
//        std::string GetNetworkRecordFileName2(size_t id, int record, bool bsession = false);
//        std::string GetNetworkRecordFileName3(size_t id, int record, bool bsession = false);
//        std::string GetNetworkRecordFileName4(size_t id, int record, bool bsession = false);

		/// The getter for record durataion.
		unsigned long long GetRecordMaximumSize();

		/// The setter for record duration.
		void SetRecordMaximumSize(unsigned long long recordsize);

		/// The getter for record path.
//        const std::string& GetRecordPath1() { return m_RecordPath1; }
//        const std::string& GetRecordPath2() { return m_RecordPath2; }
//        const std::string& GetRecordPath3() { return m_RecordPath3; }
//        const std::string& GetRecordPath4() { return m_RecordPath4; }

        std::string& GetRecordPath(int index) { return m_RecordPath[index]; }
        //std::string& GetRecordPathDeviceName(int index) { return m_RecordPathDeviceName[index]; }

        int GetDebugMode() { return m_DebugMode; }

		/// The setter for record path.
//        void SetRecordPath1(const std::string& path) { m_RecordPath1 = path; }
//        void SetRecordPath2(const std::string& path) { m_RecordPath2 = path; }
//        void SetRecordPath3(const std::string& path) { m_RecordPath3 = path; }
//        void SetRecordPath4(const std::string& path) { m_RecordPath4 = path; }

        void SetRecordPath(const std::string& path, int index) { m_RecordPath[index] = path; }
        //void SetRecordPathDeviceName(const std::string& path, int index) { m_RecordPathDeviceName[index] = path; }

        void SetDebugMode(int mode) { m_DebugMode = mode; }

        void setProgramCPUAffinity(std::string value);
        //void SetCPUAffinity (std::string value);
        void SetGroupCPUAffinity();
        void SetGroup(int NUMAnode, string SetPort, string CPUAffinity, string Path);
        void SetThreadPriority (std::string value);
        void SetThreadSleep (std::string value);

		/// The getter for replay path.
        const std::string& GetReplayPath() { return m_ReplayPath; }

		/// The setter for replay path.
		void SetReplayPath(const std::string& path) { m_ReplayPath = path; }

		/// The getter for record section name.
		const std::string& GetRecordName() { return m_RecordName; }

		/// The setter for record section name.
		void SetRecordName(const std::string& recordName) { m_RecordName = recordName; }

		/// The getter for replay section name.
		const std::string& GetReplayName() { return m_ReplayName; }

		/// The setter for replay section name.
		void SetReplayName(const std::string& replayName) { m_ReplayName = replayName; }

        /// SSDEndurace
        void SetSSDEndurace(std::string value);

		/// Trigger Time.
        tm RecordTriggerTime;

		unsigned long WRITE_NUM_SINGLE_BUFFERS;

		unsigned long WRITE_NUM_BUFFERS;

		unsigned long WRITE_BUFFER_SIZE;

		unsigned long READ_NUM_SINGLE_BUFFERS;

		unsigned long READ_NUM_BUFFERS;

		unsigned long READ_BUFFER_SIZE;

		double PRE_LOAD_RATE;

        int m_DebugMode;

        vector<GROUP> v_Group;
        vector<SSD> v_SSD;

        int v_CPUAffinityRead[8];
        int v_CPUAffinityWrite[8];

        int ThreadPriorityMemcpy;
        int ThreadPriorityWrite;
        int ThreadPriorityProgram;

        int ThreadSleepMemcpy;
        int ThreadSleepWrite;

        int AIOSleep;
        int CPUAmount;

        std::string temp;

        std::string m_Mode;
        std::string m_RaidUse;
        int m_StorageNum = 1;

        //NET-44 add Filtered stream
        std::string m_UserStream[FILTERED_STREAM_SIZE];
        std::string m_pathCH[MAM_STORAGE];

        //NET-49 Add auto file remove function.
        std::string m_AutoFileRemoveUse;
        int m_AutoFileRemoveRate = 90;

        int m_StorageUsage[MAM_STORAGE];

        /// The directory to store the device data.
        std::string m_RecordPath[MAM_STORAGE];

        //std::string m_RecordPathDeviceName[MAM_STORAGE];


	private:

		/// The directory to store the device data.
		std::string m_ReplayPath;

		/// The record name for the file name.
		std::string m_RecordName;

		/// The replay name for the file name.
		std::string m_ReplayName;

		/// The record beginning time.
		long long m_RecordMaxSize;

		/// The single instance to support singleton.
		static DeviceRecordSetting* m_Instance;

		DeviceRecordSetting();

		~DeviceRecordSetting();

	};

}

#endif
