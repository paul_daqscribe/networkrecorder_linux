#include "StorageManager.h"
#include <iostream>
#include <boost/filesystem.hpp>
#ifdef WIN32
#include <windows.h>
#else
#include <QProcess>
#include <unistd.h>
#endif

#include "DeviceRecordSetting.h"
#include <sys/statvfs.h>

//#define RedHat
#define Ubuntu

namespace NetworkLib
{

	StorageManager::StorageManager()
	{
	}

	StorageManager::~StorageManager()
	{
	}

	void StorageManager::GetSpaceInformation(const std::string& path, double& capacity, double& available, double& free)
	{
		try
		{
			boost::filesystem::path p(path);
			boost::filesystem::space_info space = boost::filesystem::space(p);
			capacity = space.capacity / 1000000.0;
			available = space.available / 1000000.0;
			free = space.available / 1000000.0;
		}
		catch (...)
		{
		}
	}

    bool StorageManager::CreatePath(const std::string& path)
	{
		boost::filesystem::path dir(path);
		if (!boost::filesystem::exists(dir))
		{
            try
            {
                boost::filesystem::path pa = dir.parent_path();
                if (!boost::filesystem::exists(pa))
                {
                    CreatePath(pa.string());
                }
                boost::filesystem::create_directory(dir);
                return true;
            }
            catch(const std::exception&)
            {
                return false;
            }
		}
	}

    //NET-57
    int StorageManager::GetStorageUsage(std::string& path)
    {
        struct statvfs stat;
        statvfs(path.c_str(), &stat);
        long freeSize = ((long long)stat.f_bsize * stat.f_bavail / 1024);
        long totalSize = ((long long)stat.f_bsize * stat.f_blocks / 1024);

        long usageSize = totalSize - freeSize;
        int frate = (double)usageSize / (double)totalSize * 100;

        return frate;

        //QString Q_path = QString::fromStdString(path);

        //path = "/home/ddr70dev" ;
        //path = "/media/ddr70dev/u2data/Record";

//        QProcess *process = new QProcess;
//        process->start("/bin/df -t ext4");
//        process->waitForFinished(1000);
//        QString res = process->readAll().data();
//        QStringList parts = res.split("\n");
//        for (int i = 0; i < parts.size(); ++i)
//        {
//            QString& part = parts[i];
//            QStringList sub_parts = part.split(" ");

//            QString& last_part = sub_parts[0];
//            if (Q_path == last_part)
//            {
//                QString rate = sub_parts[sub_parts.size()-2];
//                rate.chop(1);
//                return rate.toInt();
//            }

//        }

    }

    std::vector<DiskInfo> StorageManager::HardDiskInformation()
	{

        auto setting = DeviceRecordSetting::GetInstance();

        //dev
        std::vector<DiskInfo> infos;
        QProcess process;
        process.start("/bin/df -t ext4");
        process.waitForFinished();
        QString res = process.readAll().data();
        QStringList parts = res.split("\n");
        for (int i = 0; i < parts.size(); ++i)
        {
            QString& part = parts[i];
            if (part.contains("/dev/"))
            {
                QStringList items = part.split(" ");
                items.removeAll("");
                if (items.size() > 5)
                {
                    DiskInfo info;
                    info.name = items[0].toStdString();
                    info.info = items[5].toStdString();
                    info.capacity = items[1].toDouble() / 1000;
                    //info.available = info.capacity - items[2].toDouble() / 1000;
                    info.available = items[2].toDouble() / 1000;
                    info.free = items[3].toDouble() / 1000;
                    info.free_Percentage = items[4].toDouble();
                    infos.push_back(info);
                }
            }
        }
        //
        //set serial
        //
        for (int x = 0; x < infos.size(); x++)
        {
            QString name = QString::fromStdString(infos[x].name);
            if (name.contains("nvme"))
            {
                QProcess process2;

                #ifdef Ubuntu
                //ubuntu
                QString command = "sudo smartctl -x " + name;
                #else
                //redhat
                QString command = "sudo /usr/local/sbin/smartctl -x " + name;
                #endif


                process2.start(command);
                process2.waitForFinished();
                QString res2 = process2.readAll().data();
                QStringList parts2 = res2.split("\n");
                for (int i = 0; i < parts2.size(); ++i)
                {
                    QString& part = parts2[i];
                    if (part.contains("Serial Number"))
                    {
                        part.replace(" ", "");
                        QStringList items = part.split(":");
                        items.removeAll("");
                        if (items.size() > 1)
                        {
                            infos[x].serial_number = items[1].toStdString();

                        }
                    }
                }
            }
        }

        //
        // campare with serial and set Model Number & Data Units Written
        //
        int index = 0;
        for (int x = 0; x < infos.size(); x++)
        {
            int serial_index = -1;
            std::string model_name="";
            std::string display_model_name="";

            QString name = QString::fromStdString(infos[x].name);
            if (name.contains("nvme"))
            {
                QProcess process3;

                #ifdef Ubuntu
                //ubuntu
                QString command = "sudo smartctl -x /dev/nvme";
                #else
                //redhat
                QString command = "sudo /usr/local/sbin/smartctl -x /dev/nvme";
                #endif


                command = command + QString::number(index);;
                process3.start(command);
                process3.waitForFinished();
                QString res3 = process3.readAll().data();
                QStringList parts3 = res3.split("\n");
                for (int i = 0; i < parts3.size(); ++i)
                {
                    QString& part = parts3[i];
                    if (part.contains("Model Number"))
                    {
                        part.replace(" ", "");
                        QStringList items = part.split(":");
                        items.removeAll("");

                        if (items.size() > 1)
                        {
                            QStringList items_sub = items[1].split("_");
                            if (items_sub.size() > 2)
                            {
                                QString part_sub = items_sub[0];
                                if(part_sub.contains("Micron"))
                                {
                                    model_name = items[1].toStdString();
                                    display_model_name = items_sub[2].toStdString();
                                }
                                else
                                {
                                    model_name = items[1].toStdString();
                                    display_model_name = items[1].toStdString();
                                }
                            }
                        }
                    }

                    if (part.contains("Serial Number"))
                    {
                        part.replace(" ", "");
                        QStringList items = part.split(":");

                        for(int z = 0; z < infos.size(); z++)
                        {
                            if (infos[z].serial_number == items[1].toStdString())
                            {
                                infos[z].model_name = model_name;
                                infos[z].display_model_name = display_model_name;

                                serial_index = z;
                                for (int y =0; y < setting->v_SSD.size(); y++)
                                {
                                    QString name = QString::fromStdString(infos[z].model_name);

                                    if(name.contains("Micron"))
                                    {
                                        string model_name = setting->v_SSD[y].m_SSDmodel;
                                        if(name.contains(model_name.c_str()))
                                        {
                                            infos[z].life_cycle = setting->v_SSD[y].m_SSDWBT;
                                        }
                                    }
                                    else
                                    {
                                        string model_name = setting->v_SSD[y].m_SSDmodel;
                                        if(name.contains(model_name.c_str()))
                                        {
                                            infos[z].life_cycle = setting->v_SSD[y].m_SSDWBT;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (part.contains("Data Units Written"))
                    {
                        if (serial_index != -1)
                        {
                            QStringList items = part.split("[");
                            items.removeAll("");
                            QString m_write = items[1];
                            m_write.replace("]", "");

                            QStringList v_written = m_write.split(" ");
                            infos[serial_index].written = v_written[0].toDouble();
                            infos[serial_index].unit = v_written[1].toStdString();
                        }
                    }
                }
                index ++;
            }
        }

        ///
        /// Start add logic when use raid
        ///

        if (setting->m_RaidUse != "N")
        {
            //set name
            QProcess process4;
            process4.start("sudo fdisk -l");
            process4.waitForFinished();
            QString res4 = process4.readAll().data();
            QStringList parts4 = res4.split("\n");
            for (int i = 0; i < parts4.size(); ++i)
            {
                QString& part = parts4[i];
                if (part.contains("/dev/nvme"))
                {
                    QStringList items = part.split(" ");
                    items.removeAll("");
                    if (items.size() > 5)
                    {
                        DiskInfo info;
                        string temp = items[1].toStdString();
                        info.name = temp.substr(0, temp.length()-3);
                        info.info = temp.substr(0, temp.length()-3);
                        infos.push_back(info);
                    }
                }
            }


            //
            //set serial
            //
            for (int x = 0; x < infos.size(); x++)
            {
                QString name = QString::fromStdString(infos[x].name);
                if (name.contains("/dev/nvme"))
                {
                    QProcess process2;

                    #ifdef Ubuntu
                    //ubuntu
                    QString command = "sudo smartctl -x " + name;
                    #else
                    //redhat
                    QString command = "sudo /usr/local/sbin/smartctl -x " + name;
                    #endif

                    process2.start(command);
                    process2.waitForFinished();
                    QString res2 = process2.readAll().data();
                    QStringList parts2 = res2.split("\n");
                    for (int i = 0; i < parts2.size(); ++i)
                    {

                        QString& part = parts2[i];
                        if (part.contains("Model Number"))
                        {
                            part.replace(" ", "");
                            QStringList items = part.split(":");
                            items.removeAll("");
                            if (items.size() > 1)
                            {
                                infos[x].model_name = items[1].toStdString();
                                infos[x].display_model_name = items[1].toStdString();
                            }

                            for (int y =0; y < setting->v_SSD.size(); y++)
                            {
                                QString name = QString::fromStdString(infos[x].model_name);

                                string model_name = setting->v_SSD[y].m_SSDmodel;
                                if(name.contains(model_name.c_str()))
                                {
                                    infos[x].life_cycle = setting->v_SSD[y].m_SSDWBT;
                                }
                            }
                        }

                        else if (part.contains("Serial Number"))
                        {
                            part.replace(" ", "");
                            QStringList items = part.split(":");
                            items.removeAll("");
                            if (items.size() > 1)
                            {
                                infos[x].serial_number = items[1].toStdString();

                            }
                        }
                        else if (part.contains("Data Units Written"))
                        {
                            QStringList items = part.split("[");
                            items.removeAll("");
                            QString m_write = items[1];
                            m_write.replace("]", "");

                            QStringList v_written = m_write.split(" ");
                            infos[x].written = v_written[0].toDouble();
                            infos[x].unit = v_written[1].toStdString();
                        }
                    }

                }
            }
        }
        return infos;
    }

}
