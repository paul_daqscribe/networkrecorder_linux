#ifndef NETWORK_DATASPEEDMONITOR_H
#define NETWORK_DATASPEEDMONITOR_H
#include <ctime>

namespace NetworkLib
{
    inline unsigned long long nt_clock()
    {

#ifdef WIN32
        return (unsigned long long)clock();
#else
        timespec spec;
        clock_gettime(CLOCK_REALTIME, &spec);
        return (unsigned long long) spec.tv_sec * 1000 + spec.tv_nsec / 1000000;
#endif
    }

	/**
	 * DataSpeedMonitor supports monitoring the data speed.
	 */
	class DataSpeedMonitor
	{

	public:

		/// Default constructor.
		DataSpeedMonitor();

		/// Destructor.
		~DataSpeedMonitor();

		/**
		 * UpdateData updates the current speed.
		 * @param datasize The byte size of data.
		 */
		void UpdateData(long long datasize);

		/// Get the current time.
		void BeginSample();

		/// Refresh the current time.
		void EndSample();

		/// The accessor for the current data speed.
		double GetCurrentSpeed();

		/// The accessor for the current data sample (peak) speed.
		double GetDataSampleSpeed();

		/// Reset clears the current speed.
		void Reset();

		/// The accessor for the total data size in bytes.
		long long GetTotalDataBytes();

		/// The accessor for monitoring duration.
		double GetDuration();

		/// The accessor for data duration
		double GetDataDuration() { return m_CurrentTime - m_StartTime; }

	private:

		/// The current time of duration of monitoring.
		double m_CurrentTime;

		// The monitoring start time.
		double m_StartTime;

		/// The sample time.
		double m_SampleTime;

		/// The sample data size.
		long long m_SampleSize;

		/// The current data speed.
		double m_CurrentSpeed;

		/// The current data sample (peak) speed.
		double m_CurrentSampleSpeed;

		/// The total data bytes.
		long long m_TotalDataBytes;



	};

}

#endif
