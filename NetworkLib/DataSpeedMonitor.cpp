#include "DataSpeedMonitor.h"
#include <ctime>
#include <algorithm>

namespace NetworkLib
{
	DataSpeedMonitor::DataSpeedMonitor()
        : m_CurrentTime(0)
        , m_StartTime(0)
        , m_SampleTime(0)
        , m_SampleSize(0)
        , m_CurrentSpeed(0)
        , m_CurrentSampleSpeed(0)
        , m_TotalDataBytes(0)
	{
	}

	DataSpeedMonitor::~DataSpeedMonitor()
	{
	}

	void DataSpeedMonitor::BeginSample()
	{
        m_SampleTime = nt_clock() / (double)1000;
	}

	void DataSpeedMonitor::EndSample()
	{
        double diff = m_SampleTime - m_CurrentTime;
        m_CurrentSampleSpeed = m_SampleSize / std::max(0.0001, diff);

        m_CurrentTime = nt_clock() / (double)1000;
        m_TotalDataBytes += m_SampleSize;

        m_CurrentSpeed = m_SampleSize / std::max(0.0001, (m_CurrentTime - m_SampleTime));
        m_SampleSize = 0;
	}

	void DataSpeedMonitor::UpdateData(long long datasize)
	{
		if (datasize == 0) return;
		m_SampleSize += datasize;
	}

	double DataSpeedMonitor::GetCurrentSpeed()
	{
		return m_CurrentSpeed;
	}

	double DataSpeedMonitor::GetDataSampleSpeed()
	{
		return m_CurrentSampleSpeed;
	}

	void DataSpeedMonitor::Reset()
	{
		m_CurrentSpeed = 0;
		m_CurrentSampleSpeed = 0;
        m_CurrentTime = nt_clock() / (double)1000;
		m_StartTime = m_CurrentTime;
		m_TotalDataBytes = 0;
	}

	long long DataSpeedMonitor::GetTotalDataBytes()
	{
		return m_TotalDataBytes;
	}

	double DataSpeedMonitor::GetDuration()
	{
        return nt_clock() / (double)1000 - m_StartTime;
	}
}
