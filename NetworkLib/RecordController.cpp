﻿#include "RecordController.h"
#include <boost/filesystem.hpp>
#include "usertagmanager.h"
#include "logger.h"
#include "DeviceRecordSetting.h"
#include "NetworkDeviceManager.h"
#include "StorageManager.h"

#ifndef WIN32
#include <sys/stat.h>
#include <fcntl.h>
#include <boost/mpl/aux_/config/dtp.hpp>
#include <numa.h>
#endif

#include <mutex>
#include <QProcess>





//#define RedHat
#define Ubuntu

namespace NetworkLib
{

//    struct timespec
//    {
//        time_t   tv_sec;        /* seconds */
//        long     tv_nsec;       /* nanoseconds */
//    };

   struct RecordController::Runner
	{
		RecordController* recorder;
		Runner(RecordController* r)
            : recorder(r)
            , m_bDone(false)
            , m_bRunning(true)
		{

		}

		/**
		* operator() is the implementation of the device running logic.
		*/
		//commit test
		void operator()()
		{
			m_bDone = false;
			while (m_bRunning)
			{
				recorder->Flush();
			}

//            auto pRecordSetting = DeviceRecordSetting::GetInstance();
//            if (pRecordSetting->m_Mode == "N")
//            {
//                int temp = recorder->error.find("Error");
//                if(temp < 0)
//                    recorder->FlushRemainBuffer();
//            }
//            //recorder->Running = false;

            m_bDone = true;

            if (recorder->error == "")
                recorder->FlushRemainBuffer();

			++recorder->RecordNumber;
            if (recorder->seg_info) recorder->seg_info->close();
            recorder->seg_out.close();
            //m_bDone = true;

            recorder->EndRunning = false;


        }

		/**
		* Reset makes to start the thread.
		* @return The device is set to run continually.
		*/
		void Reset() { m_bRunning = true; }

		/**
		* Stop stops the thread.
		* @return The device is stopped.
		*/
		void Stop() { m_bRunning = false; }

		/**
		* IsRunning is the getter for the running state.
		* @return True if the device is running otherwise return false.
		*/
		bool IsRunning() { return m_bRunning; }

		bool IsDone() { return m_bDone; }



	private:

		bool m_bDone;

        /// The flag for running state of the thread.
        bool m_bRunning;

	};

	std::ifstream::pos_type filesize(const std::string& filename)
	{
		std::ifstream in(filename.c_str(), std::ifstream::ate | std::ifstream::binary);
		return in.tellg();
	}

	RecordController::RecordController()
        : RecordNumber(0)
        , TotalData(0)
        , LastMinuteData(0)
        , ID(0)
        , RelayID(0)
        , Running(false)
        , EndRunning(false)
        , RelayOffset(0)
        , RelayDuration(1)
        , TotalDuration(0)
        , RecordDuration(0)
        , errorCode(0)
        , nFiles(0)
        , nFilesAhead(0)
        , begin_time(0)
        , hFile(INVALID_HANDLE_VALUE)
        , readdatasize(0)
        , file_size(0)
        , datasize(0)
        , m_IOThread(0)
        , m_IORunner(0)
		, leftsize(0)
        , End_leftsize(0)
        , sagCutSize(0) //512k
        , leftSagCutSize(0)
        , n_current(0)
        , n_written(0)
        , last_written(0)
        , m_bRead(false)
        , m_bHeader(true)
		, replybegin(0)
		, replyduration(0)
		, replydatasize(0)
		, PRE_LOAD_RATE(0.25)
        , BufferLevel(0)
        , MemCpySpeed(0)
        , AIOSleep(0)
        , second_keep(0)
        , Numa(0)
        , multiThread_leftsize(0)
        ,m_NT_Running(false)
        //,test1(new NTNetworkDevice())
	{
#ifdef WIN32
        flag = FILE_FLAG_NO_BUFFERING | FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH;
#else


        //S_IRUSR : 읽기 권한
        //S_IWUSR : 쓰기 권한
        //S_IRGRP : 읽기 권한 그룹
        //S_IROTH : 일기 권한 기타사용자

        mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
        //flag = O_DIRECT | O_SYNC;
        //flag = O_DIRECT | O_ASYNC;
        //flag = O_NONBLOCK | O_ASYNC; //2000
        //flag = O_NONBLOCK; //2000
        //flag = O_DIRECT;


#endif
        begin_time = nt_clock();
    }

	RecordController::~RecordController()
	{
		Close();

        //delete buffer when preopen
//		for (unsigned long i = 0; i < (unsigned long)buffer.size(); ++i)
//		{
//			delete[] buffer[i];
//		}
//		for (unsigned long i = 0; i < (unsigned long)leftbuffer.size(); ++i)
//		{
//			delete[] leftbuffer[i];
//		}
	}

	unsigned long long RecordController::RemainRecordDuration()
	{
		if (Running)
		{
            return RecordDuration - (nt_clock() - record_begin_time);
		}
		else
		{
			return 0;
		}
	}

    std::string RecordController::GetFileName()
	{
		return filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
	}

    void RecordController::PreOpen(bool bRead, int ch)
    {
        if (!Running)
        {
            auto setting = DeviceRecordSetting::GetInstance();
//            if (setting->ONONBLOCK == 1) { flag |= O_NONBLOCK; }
//            if (setting->ONDELAY == 1) { flag |= O_NDELAY; }
//            if (setting->ONOATIME == 1) { flag |= O_NOATIME; }
//            if (setting->ODSYNC == 1) { flag |= O_DSYNC; }
//            if (setting->OSYNC == 1) { flag |= O_SYNC; }
//            if (setting->ORSYNC == 1) { flag |= O_RSYNC; }
//            if (setting->ODIRECT == 1) { flag |= O_DIRECT; }
//            if (setting->OASYNC == 1) { flag |= O_ASYNC; }

            DebugMode = setting->GetDebugMode();
            ThreadSleepMemcpy = setting->ThreadSleepMemcpy;
            ThreadSleepWrite = setting->ThreadSleepWrite;
            AIOSleep = setting->AIOSleep;


            //auto setting = DeviceRecordSetting::GetInstance();
            max_file_size = setting->GetRecordMaximumSize();
            error = "";
            m_bRead = bRead;
            TotalData = 0;
            if(!FirstOpen)
            {
                NUM_BUFFERS = bRead ? setting->READ_NUM_BUFFERS : setting->WRITE_NUM_BUFFERS;
                BUFFER_SIZE = bRead ? setting->READ_BUFFER_SIZE : setting->WRITE_BUFFER_SIZE;
                SINGLE_BUFFER = BUFFER_SIZE * (bRead ? setting->READ_NUM_SINGLE_BUFFERS : setting->WRITE_NUM_SINGLE_BUFFERS);
                PRE_LOAD_RATE = setting->PRE_LOAD_RATE;

                for (unsigned long i = 0; i < (unsigned long)buffer.size(); ++i)
                {
                    delete[] buffer[i];
                }
                for (unsigned long i = 0; i < (unsigned long)leftbuffer.size(); ++i)
                {
                    delete[] leftbuffer[i];
                }

                buffer_test.resize(1, 0);
                buffer.resize(NUM_BUFFERS, 0);
                leftbuffer.resize(NUM_BUFFERS, 0);
                remainBufferSize.resize(NUM_BUFFERS, 0);
                flag = O_DIRECT;

                for (unsigned long i = 0; i < NUM_BUFFERS; ++i)
                {
                    //get group numa
                    for(int x=0; x < setting->v_Group.size(); x++)
                    {
                        for(int y = 0; y < setting->v_Group[x].m_NumPort; y++)
                        {
                             if(ch == setting->v_Group[x].v_Port[y])
                             {
                                 Numa = setting->v_Group[x].m_NUMAnode;
                             }
                        }
                    }
                    //set buffer at node
                    buffer[i] = (char *)numa_alloc_onnode(SINGLE_BUFFER, Numa);
                    if(buffer[i] == NULL)
                    {
                            numa_free(buffer[i], SINGLE_BUFFER);
                    }
                    memset(buffer[i], 2, SINGLE_BUFFER);

                    leftbuffer[i] = new char[SINGLE_BUFFER];
                    memset(leftbuffer[i], 0, SINGLE_BUFFER);
                }

                FirstOpen = true;
            }
        }
    }

    void RecordController::Open(bool bRead, int ch)
	{
		if (!Running)
		{
            last_written = 0;
            TotalData = 0;
            datasize = 0;
            nFiles = 1;
            nFilesAhead = 1;
            n_current = n_written = 0;
            leftsize = 0;
            End_leftsize = 0;
            MAX_NUM_BUFFER_FILE = max_file_size / SINGLE_BUFFER;
            m_bRead = bRead;
            if (!m_bRead)
            {
                StorageManager::CreatePath(filename);
            }
            std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();



            //for debuggig
            //std::string seg_name = filename + "/" + "debug_seg";
            //file_debug = fopen(seg_name.c_str(), "w+t");


            //record
            if (!m_bRead)
			{
                record_begin_time = nt_clock();

                TagFileOperation(true);
                seg_out.close();
                seg_out.open((filename + "/record.seg").c_str(), std::ios::binary);
				std::wstring name;
				for (int i = 0; i < (int)fname.size(); ++i)
				{
					name.push_back(fname[i]);
				}

                hFile = open(fname.c_str(), O_CREAT | O_APPEND | O_WRONLY | O_TRUNC| flag, mode);

                int flags = fcntl(hFile, F_GETFL, 0);
                fcntl(hFile, F_SETFL, flags);

                if (hFile < 0)
                {
                    std::cout << "failed to open\n";
                }

                //AIO init
                bzero((char *)&my_aiocb, sizeof(struct aiocb));
                my_aiocb.aio_fildes = hFile;
                //my_aiocb.aio_buf = buffer[0];
                my_aiocb.aio_nbytes = SINGLE_BUFFER;
                //my_aiocb.aio_offset = 0;

                my_aiocb.aio_sigevent.sigev_notify = SIGEV_NONE;
                //AIO

                auto pRecordSetting = DeviceRecordSetting::GetInstance();
                if (pRecordSetting->m_Mode == "N")
                    Write(&FileHeader[0], (unsigned int)FileHeader.size(), true);
                else
                    Write1(&FileHeader[0], (unsigned int)FileHeader.size(), 0,0,0,true);

                Running = true;
                m_NT_Running = true;
                EndRunning = true;
				m_IORunner = std::shared_ptr<Runner>(new Runner(this));
				m_IOThread = std::shared_ptr<boost::thread>(new boost::thread(boost::ref(*m_IORunner)));
                second_keep = nt_clock();

                //// set cpu affinity ///////////
                unsigned long mask = 0;
                //auto pRecordSetting = DeviceRecordSetting::GetInstance();

//                if (ch == 0)//port1
//                    mask = pRecordSetting->Port1CPUNumWrite;
//                else//port2
//                    mask = pRecordSetting->Port2CPUNumWrite;

                mask = pRecordSetting->v_CPUAffinityWrite[ch-1];

                int reseult = 0;
                pthread_t threadID = (pthread_t)m_IOThread->native_handle();

                //set
                reseult = pthread_setaffinity_np(threadID, sizeof(mask), (cpu_set_t *)&mask);

                //// set cpu affinity ////////////


                ///// set therad priority /////////
                int retcode;
                int policy;

               // pthread_t threadID = (pthread_t)m_IOThread->native_handle();
                struct sched_param param;

                if ((retcode = pthread_getschedparam(threadID, &policy, &param)) != 0)
                {
                    errno = retcode;
                    perror("pthread_getschedparam");
                    exit(EXIT_FAILURE);
                }

//                std::cout << "INHERITED: ";
//                std::cout << "policy=" << ((policy == SCHED_FIFO) ? "SCHED_FIFO" :
//                    (policy == SCHED_RR) ? "SCHED_RR" :
//                    (policy == SCHED_OTHER) ? "SCHED_OTHER" : "???")
//                    << ", priority=" << param.sched_priority << std::endl;


                policy = SCHED_FIFO;
                int temp = pRecordSetting->ThreadPriorityWrite;
                param.sched_priority = temp;

                if ((retcode = pthread_setschedparam(threadID, policy, &param)) != 0)
                {
                    errno = retcode;
                    perror("pthread_setschedparam");
                    exit(EXIT_FAILURE);
                }

//                if ((retcode = pthread_getschedparam(threadID, &policy, &param)) != 0)
//                {
//                    errno = retcode;
//                    perror("pthread_getschedparam");
//                    exit(EXIT_FAILURE);
//                }

//                std::cout << "INHERITED: ";
//                std::cout << "policy=" << ((policy == SCHED_FIFO) ? "SCHED_FIFO" :
//                    (policy == SCHED_RR) ? "SCHED_RR" :
//                    (policy == SCHED_OTHER) ? "SCHED_OTHER" : "???")
//                    << ", priority=" << param.sched_priority << std::endl;

                ///// set therad priority /////////

			}

            //replay
			else
			{
				m_bHeader = true;
				seg_info = std::shared_ptr<std::ifstream>(new std::ifstream());
				seg_info->open((filename + "/record.seg").c_str(), std::ios::binary);
				if (!seg_info->good())
				{
					error = "Failure : " + filename + "/record.seg";
					return;
				}
                headerlen = HEADER_NT;
                //seg_info->read((char*)&headerlen, sizeof(headerlen));
				replycurrent = 0;
				unsigned int len = 0;
				file_size = readdatasize = filesize(fname);
				while (replybegin > replycurrent + readdatasize)
				{
					long long total = 0;
					readdatasize -= headerlen;
					while (total < readdatasize)
					{
						seg_info->read((char*)&len, sizeof(len));
						if (seg_info->bad()) break;
						replycurrent += len;
						total += len;
					}
					if (seg_info->bad()) break;
					++nFiles;
					fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
					readdatasize = filesize(fname);
				}
				std::wstring name;
				for (int i = 0; i < (int)fname.size(); ++i)
				{
					name.push_back(fname[i]);
				}

                hFile = open(fname.c_str(), O_RDONLY | flag);
                if (hFile < 0)
                {
                    std::cout << "failed to open\n";
                }

                GetError();
				std::string readbuffer;
				Running = true;
				m_IORunner = std::shared_ptr<Runner>(new Runner(this));
				m_IOThread = std::shared_ptr<boost::thread>(new boost::thread(boost::ref(*m_IORunner)));
				Sleep(10);

				while (replybegin > replycurrent)
				{
					ReadNext(readbuffer);
				}
				unsigned long long nbuffer = (unsigned long long)(NUM_BUFFERS * PRE_LOAD_RATE);
				while (n_written - n_current < nbuffer)
				{
					Sleep(5);
				}
			}
		}
	}

	void RecordController::Close()
	{
		if (Running)
		{
            Running = false;

			if (m_IORunner) m_IORunner->Stop();
			while (!m_IORunner->IsDone())
			{
                usleep(1000);
			}
		}
	}

	void RecordController::NextFile()
	{
		if (Running)
		{
            FlushRemain();
            ++nFiles;
			datasize = 0;
            std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
            std::wstring name;
            for (int i = 0; i < (int)fname.size(); ++i)
            {
                name.push_back(fname[i]);
            }
			if (m_bRead && !DoesExist())
			{
				hFile = INVALID_HANDLE_VALUE;
				Running = false;
				readdatasize = 0;
				return;
			}
			if (m_bRead)
			{
				file_size = readdatasize = filesize(fname);
				if (hFile != INVALID_HANDLE_VALUE)
				{

                    close(hFile);
                    GetError();
					hFile = INVALID_HANDLE_VALUE;
				}

                hFile = open(fname.c_str(), O_RDONLY | flag);
                if (hFile < 0)
                {
                    std::cout << "failed to open\n";
                }

                GetError();
				m_bHeader = true;
				if (hFile == INVALID_HANDLE_VALUE)
				{
					Running = false;
				}
			}
			else
			{
                //origin source
                hFile = open(fname.c_str(), O_CREAT | O_APPEND | O_WRONLY | O_TRUNC| flag, mode);
                int flags = fcntl(hFile, F_GETFL, 0);
                fcntl(hFile, F_SETFL, flags);

                if (hFile < 0)
                {
                    std::cout << "failed to open\n";
                }

                my_aiocb.aio_fildes = hFile;
                TagFileOperation(true);


                // NET-49 auto remove file
                auto setting = DeviceRecordSetting::GetInstance();
                if (setting->m_AutoFileRemoveUse == "Y")
                {
                    if (setting->m_StorageUsage[ID-1] > setting->m_AutoFileRemoveRate)
                    {
                        std::string deleteFname = filename + "/" + std::to_string((long long)m_deleteFileIndex) + NetworkDeviceManager::GetInstance()->GetExtension();
                        int x = remove(deleteFname.c_str());
                        m_deleteFileIndex++;
                    }
                }
			}
		}
	}

	void RecordController::Write(char* data, unsigned int size, bool force)
	{
        usleep(ThreadSleepMemcpy);
        if ((Running || force) && size > 0)
        {
            //seg_out.write((char*)&size, sizeof(size));
            seg_out.write((char*)&size, sizeof(size));
            auto idx = n_current % NUM_BUFFERS;

            if (leftsize + size > SINGLE_BUFFER)
            {
                //fill up end of buffer
                unsigned long long tail = SINGLE_BUFFER - leftsize;
                memcpy(&buffer[idx][leftsize], data, tail);
                total_size += tail;
                idx = n_current % NUM_BUFFERS;

                //kjk
//                memcpy(leftbuffer[idx], &data[tail], size - tail);
//                remainBufferSize[idx] = size - tail;

                if (n_current % MAX_NUM_BUFFER_FILE == (MAX_NUM_BUFFER_FILE - 1))
                {
                    memcpy(leftbuffer[idx], &data[tail], size - tail);
                    remainBufferSize[idx] = size - tail;

                    ++n_current;
                    idx = n_current % NUM_BUFFERS;
                    memcpy(buffer[idx], &FileHeader[0], FileHeader.size());
                    total_size +=  FileHeader.size();
                    leftsize = FileHeader.size();
                }
                else
                {
                    //write next buffer
                    ++n_current;
                    idx = n_current % NUM_BUFFERS;
                    memcpy(buffer[idx], &data[tail], size - tail);
                    total_size += size - tail;
                    leftsize = size - tail;
                }
            }
            else
            {
                memcpy(&buffer[idx][leftsize], data, size);
                //memcpy(&buffer[idx][0], data, size);
                total_size += size;
                leftsize += size;
            }

            //leftsize %= SINGLE_BUFFER;

            if (n_current > n_written + NUM_BUFFERS && errorStatus !=1)
            {
                error = "Error : overflowing";
            }

            if (RecordDuration < (unsigned long long)(nt_clock() - record_begin_time))
            {
                Close();
            }
        }
	}

    void RecordController::Write1(char* data, unsigned int size, int thread_leftsize, int seq, int newidx, bool force)
    {
        usleep(ThreadSleepMemcpy);
        if ((Running || force) && size > 0)
        {
            leftsize = thread_leftsize;
            End_leftsize = thread_leftsize + size;
            if (thread_leftsize + size > SINGLE_BUFFER)
            {
                //fill up end of buffer
                unsigned long long tail = SINGLE_BUFFER - thread_leftsize;
                memcpy(&buffer[newidx][thread_leftsize], data, tail);
                total_size += tail;

                if (n_current % MAX_NUM_BUFFER_FILE == (MAX_NUM_BUFFER_FILE - 1))
                {
                    memcpy(leftbuffer[newidx], &data[tail], size - tail);
                    remainBufferSize[newidx] = size - tail;

                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &FileHeader[0], FileHeader.size());
                    total_size +=  FileHeader.size();
                }
                else
                {
                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &data[tail], size - tail);
                    total_size += size - tail;
                    //leftsize = size - tail;
                }

            }
            else
            {
                memcpy(&buffer[newidx][thread_leftsize], data, size);
                total_size += size;
                //leftsize +=size;
            }

            if (n_current > n_written + NUM_BUFFERS && errorStatus !=1)
            {
                error = "Error : overflowing";
            }
        }
    }

    void RecordController::Write2(char* data, unsigned int size, int thread_leftsize, int seq, int newidx, bool force)
    {
        usleep(ThreadSleepMemcpy);
        if ((Running || force) && size > 0)
        {
            leftsize = thread_leftsize;
            End_leftsize = thread_leftsize + size;
            if (thread_leftsize + size > SINGLE_BUFFER)
            {
                //fill up end of buffer
                unsigned long long tail = SINGLE_BUFFER - thread_leftsize;
                memcpy(&buffer[newidx][thread_leftsize], data, tail);
                total_size += tail;

                if (n_current % MAX_NUM_BUFFER_FILE == (MAX_NUM_BUFFER_FILE - 1))
                {
                    memcpy(leftbuffer[newidx], &data[tail], size - tail);
                    remainBufferSize[newidx] = size - tail;

                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &FileHeader[0], FileHeader.size());
                    total_size +=  FileHeader.size();
                }
                else
                {
                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &data[tail], size - tail);
                    total_size += size - tail;
                    //leftsize = size - tail;
                }

            }
            else
            {
                memcpy(&buffer[newidx][thread_leftsize], data, size);
                total_size += size;
                //leftsize +=size;
            }


            if (n_current > n_written + NUM_BUFFERS && errorStatus !=1)
            {
                error = "Error : overflowing";
            }
        }
    }

    void RecordController::Write3(char* data, unsigned int size, int thread_leftsize, int seq, int newidx, bool force)
    {
        usleep(ThreadSleepMemcpy);
        if ((Running || force) && size > 0)
        {
            leftsize = thread_leftsize;
            End_leftsize = thread_leftsize + size;
            if (thread_leftsize + size > SINGLE_BUFFER)
            {
                //fill up end of buffer
                unsigned long long tail = SINGLE_BUFFER - thread_leftsize;
                memcpy(&buffer[newidx][thread_leftsize], data, tail);
                total_size += tail;

                if (n_current % MAX_NUM_BUFFER_FILE == (MAX_NUM_BUFFER_FILE - 1))
                {
                    memcpy(leftbuffer[newidx], &data[tail], size - tail);
                    remainBufferSize[newidx] = size - tail;

                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &FileHeader[0], FileHeader.size());
                    total_size +=  FileHeader.size();
                }
                else
                {
                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &data[tail], size - tail);
                    total_size += size - tail;
                    //leftsize = size - tail;
                }

            }
            else
            {
                memcpy(&buffer[newidx][thread_leftsize], data, size);
                total_size += size;
                //leftsize +=size;
            }

            if (n_current > n_written + NUM_BUFFERS && errorStatus !=1)
            {
                error = "Error : overflowing";
            }
        }
    }

    void RecordController::Write4(char* data, unsigned int size, int thread_leftsize, int seq, int newidx, bool force)
    {
        usleep(ThreadSleepMemcpy);
        if ((Running || force) && size > 0)
        {
            leftsize = thread_leftsize;
            End_leftsize = thread_leftsize + size;
            //seg_out.write((char*)&size, sizeof(size));
            if (thread_leftsize + size > SINGLE_BUFFER)
            {
                //fill up end of buffer
                unsigned long long tail = SINGLE_BUFFER - thread_leftsize;
                memcpy(&buffer[newidx][thread_leftsize], data, tail);
                total_size += tail;

                if (n_current % MAX_NUM_BUFFER_FILE == (MAX_NUM_BUFFER_FILE - 1))
                {
                    memcpy(leftbuffer[newidx], &data[tail], size - tail);
                    remainBufferSize[newidx] = size - tail;

                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &FileHeader[0], FileHeader.size());
                    total_size +=  FileHeader.size();
                }
                else
                {
                    ++n_current;
                    newidx = n_current % NUM_BUFFERS;
                    memcpy(buffer[newidx], &data[tail], size - tail);
                    total_size += size - tail;
                    //leftsize = size - tail;
                }

            }
            else
            {
                memcpy(&buffer[newidx][thread_leftsize], data, size);
                total_size += size;
                //leftsize +=size;
            }

            if (n_current > n_written + NUM_BUFFERS && errorStatus !=1)
            {
                error = "Error : overflowing";
            }
        }
    }


	bool RecordController::ReadNext(std::string& data)
	{
        usleep(10);
		data = "";
        if (replycurrent > replyduration)
        {
            return false;
        }
        while (n_current >= n_written)
		{
            if (!Running)
            {
                return false;
            }
            usleep(1);
		}
		unsigned int size = 0;
        while (size == 0)
        {
            if (seg_info->fail() || seg_info->eof())
            {
                return false;
            }
            seg_info->read((char*)&size, sizeof(size));

            //for debugging
            //fprintf(file_debug, "%d \n", size);
        }

		data.resize(size);
		char * pdata = &data[0];
		auto idx = n_current % NUM_BUFFERS;
		unsigned long long test = remainBufferSize[idx];
		if (remainBufferSize[idx] == 0) return false;
		unsigned long long cpsize = 0;
		while (cpsize < size)
		{
			unsigned long long currentsize = size - cpsize;
            while (n_current >= n_written)
			{
                if (!Running)
                {
                    return false;
                }
                usleep(1);
			}
			idx = n_current % NUM_BUFFERS;
			test = remainBufferSize[idx];
			if (leftsize + currentsize >= test)
			{
				unsigned long long tail = test - leftsize;
                memcpy(pdata + cpsize, &buffer[idx][leftsize], tail);
				cpsize += tail;
				remainBufferSize[idx] = 0;
				++n_current;
				leftsize = 0;
			}
			else
			{
				memcpy(pdata + cpsize, &buffer[idx][leftsize], currentsize);
				leftsize += currentsize;
				cpsize += currentsize;
			}

			leftsize %= test;
		}
		replycurrent += data.size();
		return data.size() != 0;
	}

	void RecordController::Flush()
	{
		if (m_bRead)
		{
            usleep(10);
			if (Running)
			{
				if (NUM_BUFFERS - 1 > (n_written - n_current))
				{
					auto idx = n_written % NUM_BUFFERS;
					long long readsize = (long long)SINGLE_BUFFER;
					if (readsize > readdatasize)
					{
						readsize = (unsigned long)readdatasize;
						if (readdatasize > 0)
						{
                            close(hFile);
							hFile = INVALID_HANDLE_VALUE;
							GetError();
							std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();
							std::ifstream fin(fname.c_str(), std::ios::binary);
							long long loc = file_size - readsize;
							fin.seekg(loc);
                            fin.read(buffer[idx], readsize);
							fin.close();
						}
					}
					else
					{
                        read(hFile, buffer[idx], readsize);
                        GetError();
					}

                    //kjk
                    headerlen = HEADER_NT;
					if (readsize > 0)
					{
						readdatasize -= readsize;
						if (m_bHeader)
						{
                            m_bHeader = false;
                            readsize -= headerlen;
                            std::string temp;
                            temp.resize(readsize);
                            memcpy(&temp[0], buffer[idx] + headerlen, readsize);
                            memcpy(buffer[idx], &temp[0], readsize);
						}
						datasize += readsize;
						remainBufferSize[idx] = readsize;
						++n_written;
					}
					else
					{
                        NextFile();
					}

                    BufferLevel = n_written - n_current;
				}
				else
				{
                    usleep(1);
				}
			}
		}
		else
		{
            usleep(ThreadSleepWrite);
            if (RecordDuration < (unsigned long long)(nt_clock() - record_begin_time))
            {

                m_NT_Running = false;
                return;

                //Not need at RecordController
                //Finishing must be ckecked at NTNetworkDevice

                //Running = false;
                //m_IORunner->Stop();

            }

			if (n_current > n_written)
			{
                int ret =0;
                auto idx = n_written % NUM_BUFFERS;

                if(!DebugMode)
                {
                    #ifdef RedHat
                    //redhat
                    int size = write(hFile, buffer[idx], SINGLE_BUFFER);
                    #else
                    write(hFile, buffer[idx], SINGLE_BUFFER);

//                    //aio
//                    my_aiocb.aio_buf = buffer[idx];
//                    my_aiocb.aio_offset += SINGLE_BUFFER;

//                    ret = aio_write(&my_aiocb);

//                    if (ret != 0)
//                    {
//                        printf("AIO error\n");
//                    }

//                    while(true)
//                    {
//                        ret = aio_error(&my_aiocb);

//                        if(ret == EINPROGRESS)
//                        {
//                            usleep(AIOSleep);
//                        }
//                        else if (ret == ECANCELED)
//                        {
//                            printf("canceled\n");
//                        }
//                        else if (ret == 0)
//                        {
//                            aio_return(&my_aiocb);
//                            break;
//                        }
//                        else if (ret == 9 || ret == 28)
//                        {

//                            errorStatus = 1;
//                            error = "Storage full";
//                            Close();
//                            m_IORunner->Stop();
//                            Running = false;
//                        }
//                        else
//                        {
//                            printf("Error another value\n");
//                        }
//                    }
                    #endif
                }

                GetError();
				datasize += SINGLE_BUFFER;
				TotalData += SINGLE_BUFFER;
				if (n_written % MAX_NUM_BUFFER_FILE == (MAX_NUM_BUFFER_FILE - 1))
				{
                    NextFile();
				}
				++n_written;
                unsigned long long dur = nt_clock() - second_keep;
                if (dur > 200)
                {
                    LastMinuteData = (unsigned long long)((n_written - last_written) * SINGLE_BUFFER / (dur / 1000.0));
                    last_written = n_written;
                    second_keep += dur;
                    //error = std::to_string(n_written);
                }

//                unsigned long long dur2 = nt_clock() - second_keep2;
//                if (dur2 > 1000)
//                {
//                    boost::thread t(&RecordController::m_Test ,this);
//                    t.join();
//                    second_keep2 += dur2;
//                }
			}
		}
	}

    void RecordController::m_Test()
    {
//        QProcess *process = new QProcess;
//        process->start("/bin/df -t ext4");
//        process->waitForFinished();
//        QString res = process->readAll().data();
//        QStringList parts = res.split("\n");
//        int x = 0;
    }

	void RecordController::FlushRemain()
	{
		if (!m_bRead)
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
                close(hFile);
				hFile = INVALID_HANDLE_VALUE;
				GetError();
                if (datasize > 0)
				{
					std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();

                    #ifdef Ubuntu
                    //ubuntu
                    std::fstream fout(fname.c_str(), std::ios::app | std::ios::binary);

                    #else
                    //redhat
                    hFile_remain = open(fname.c_str(), O_APPEND | O_WRONLY, mode);

                    #endif
                    auto num = n_written % NUM_BUFFERS;
					if (n_written % MAX_NUM_BUFFER_FILE != (MAX_NUM_BUFFER_FILE - 1))
					{
                        num = (n_written - 1) % NUM_BUFFERS;
					}

                    #ifdef Ubuntu
                    //ubuntu
                    //hFile_remain = open(fname.c_str(), O_APPEND | O_WRONLY, mode);
                    //int size = write(hFile_remain, leftbuffer[num], remainBufferSize[num]);

                    fout.write(leftbuffer[num], remainBufferSize[num]);
                    fout.close();

                    TotalData += remainBufferSize[num];
                    #else

                    //redhat
                    int size = write(hFile_remain, leftbuffer[num], remainBufferSize[num]);
                    TotalData += size;
                    close(hFile_remain);
                    #endif
				}
                //TagFileOperation(false);
			}
		}
		else
		{
			if (hFile != INVALID_HANDLE_VALUE)
			{
                close(hFile);
				GetError();
				hFile = INVALID_HANDLE_VALUE;
			}
		}
	}

    // 2019-03-07
    // FlushRemainBuffer
    void RecordController::FlushRemainBuffer()
    {
        usleep(1000);

        if (!m_bRead)
        {
            if (hFile != INVALID_HANDLE_VALUE)
            {
                close(hFile);
                hFile = INVALID_HANDLE_VALUE;
                GetError();

                if (leftsize > 0)
                {
                    std::string fname = filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension();

                    #ifdef Ubuntu
                    //ubuntu
                    std::fstream fout(fname.c_str(), std::ios::app | std::ios::binary);
                    #else

                    //redhat
                    hFile_remain = open(fname.c_str(), O_APPEND | O_WRONLY, mode);
                    #endif

                    while (n_current >= n_written)
                    {
                        auto num = n_written % NUM_BUFFERS;

                        //size_t m_size = 0;
                        if (n_current > n_written)
                        {
                            #ifdef Ubuntu
                            //ubuntu
                            fout.write(buffer[num], SINGLE_BUFFER);
                            TotalData += SINGLE_BUFFER;
                            #else
                            //redhat
                            int size = write(hFile_remain, buffer[num], SINGLE_BUFFER);
                            TotalData += size;
                            #endif
                        }
                        else if (n_current == n_written)
                        {
                            #ifdef Ubuntu
                            //ubuntu
                            //fout.write(buffer[num], leftsize);
                            fout.write(buffer[num], End_leftsize);
                            TotalData += End_leftsize;
                            #else
                            //redhat
                            int size = write(hFile_remain, buffer[num], End_leftsize);
                            TotalData += size;
                            break;
                            #endif
                        }
                        n_written++;
                    }
                    #ifdef Ubuntu
                    fout.close();
                    #else
                    close(hFile_remain);
                    #endif

                }
            }
            TagFileOperation(false);
        }

    }

	void RecordController::AddTagToRecord(int tagidx, bool bset)
	{
		if (!m_bRead)
		{
			auto tags = UserTagManager::GetInstance()->Tags;
            std::string temp = tags[tagidx].Name;
			if (tagidx < (int)tags.size())
			{
				tag_out.open((filename + "/session.tag").c_str(), std::ios::app);
				std::string mss = Logger::TimeStamp();
                tag_out << tagidx+101 << ",LOCAL_EVENT" << (bset ? ",SET," : ",RESET,") << timestamp << "," << mss << "." << timestamp % 1000
                    << "," << filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension()
                    << "," << tags[tagidx].Name << "," << tags[tagidx].Description << "\n";
                tag_out.close();
			}
		}
	}

	unsigned long long RecordController::AddMetaTagToRecord(unsigned long long tagidx, bool bset, const std::string& time)
	{
		unsigned long long res = timestamp;
		if (!m_bRead)
		{
			tag_out.open((filename + "/session.tag").c_str(), std::ios::app);
			tag_out << tagidx << ",REMOTE_EVENT" << (bset ? ",SET," : ",RESET,") << timestamp << "," << time
				<< "," << filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension() << "\n";
			tag_out.close();
		}
		return res;
	}

	void RecordController::GetDurationFromTags(bool tag)
	{
		RelayOffset = 0;
		RelayDuration = 0;
		std::ifstream fin((filename + "/session.tag").c_str());
		std::string line;
		std::vector<unsigned long long> times;
		while (getline(fin, line))
		{
			auto fields = GetFields(line, ",");
			if (fields.size() > 3)
			{
                times.push_back(stoull(fields[3]));
			}
		}
		if (times.size() > 1)
		{
			if (tag)
			{
				RelayDuration = times.back();
				RelayOffset = times.front();
				TotalDurationTag = RelayDuration - RelayOffset;
			}
			else
			{
                RelayDuration = TotalDuration = (times.back() - times.front()) / 100;
			}
		}
	}

	bool RecordController::DoesExist()
	{
		std::string file = GetFileName();
		return boost::filesystem::exists(file);
	}

	void RecordController::TagFileOperation(bool begin)
	{
		if (!m_bRead)
		{
			tag_out.open((filename + "/session.tag").c_str(), std::ios::app);
			std::string mss = Logger::TimeStamp();
			tag_out << (begin ? 1 : 2) << ",LOCAL_EVENT,SET," << timestamp << "," << mss << "." << timestamp % 1000
				<< "," << filename + "/" + std::to_string((long long)nFiles) + NetworkDeviceManager::GetInstance()->GetExtension() << "\n";
			tag_out.close();
		}
	}

	void RecordController::SetReplaySessionByTime(unsigned long long b, unsigned long long d)
	{
		nFiles = 1;
		std::string file = GetFileName();
		replydatasize = 0;
		while (boost::filesystem::exists(file))
		{
			replydatasize += filesize(file);
			++nFiles;
			file = GetFileName();
		}
		nFiles = 1;
		long double br = 0;
		long double dr = 0;
		if (TotalDuration != 0)
		{
			br = (long double)b / TotalDuration;
			dr = (long double)(d + b) / TotalDuration;
		}
		replybegin = (long long)(br * replydatasize);
		replyduration = (long long)(dr * replydatasize);
	}

	void RecordController::SetReplaySessionBySize(unsigned long long b, unsigned long long d)
	{
		replybegin = b;
		replyduration = d;
	}

	void RecordController::SetReplaySessionByTag(unsigned long long b, unsigned long long d)
	{
		std::ifstream fin((filename + "/session.tag").c_str());
		std::string line;
        unsigned long long starttag = 0;
		while (getline(fin, line))
		{
			auto fields = GetFields(line, ",");
			if (fields.size() > 3)
            {
                starttag = stoull(fields[3]);
				break;
			}
		}
		nFiles = 1;
		std::string file = GetFileName();
		replydatasize = 0;
		while (boost::filesystem::exists(file))
		{
			replydatasize += filesize(file);
			++nFiles;
			file = GetFileName();
		}
		nFiles = 1;
		long double br = 0;
		long double dr = 0;
		if (TotalDurationTag != 0)
		{
			br = (long double)(b - starttag) / (TotalDurationTag);
            dr = (long double)(d - starttag) / (TotalDurationTag);
            //dr = (long double)(d - b) / (TotalDurationTag);
		}
		replybegin = (long long)(br * replydatasize);
		replyduration = (long long)(dr * replydatasize);
	}

	void RecordController::GetError()
    {
#ifdef WIN32
		errorCode = GetLastError();
#endif
	}

}
